<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>

<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Tagit</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<c:url value="/resources/bootstrap/css/theme-default.css"/>"/>
        
        
        <!-- EOF CSS INCLUDE -->    
        
        
        <script type="text/javascript">

	
    </script>                        
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="index.html">Tagit</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="${sessionScope.user.readImagePath}" />
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="${sessionScope.user.readImagePath}" />
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">${sessionScope.user}</div>
                                
                            </div>
                          
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li class="active">
                        <a href="<c:url value="/user/home"/>"><span class="fa fa-desktop"></span> <span class="xn-text">Home</span></a>                        
                    </li> 
                     <li >
                        <a href="<c:url value="/user/profile"/>"><span class="fa fa-user"></span> <span class="xn-text">Profile</span></a>                        
                    </li> 
                                        
                   
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                
                
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    
                    <!-- START WIDGETS -->                    
                    <div class="row">
                        <div class="col-md-3">
                            
                            <!-- START WIDGET SLIDER -->
                            <div class="widget widget-default widget-carousel">
                                <div id="owl-example" class="owl-carousel owl-theme" style="opacity: 1; display: block;">
                                    <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1326px; left: 0px; display: block; transition: all 400ms ease 0s; transform: translate3d(-221px, 0px, 0px);"><div class="owl-item" style="width: 221px;"><div>                                    
                                        <div class="widget-title">Total Visitors</div>                                                                        
                                        <div class="widget-subtitle">27/08/2014 15:23</div>
                                        <div class="widget-int">3,548</div>
                                    </div></div><div class="owl-item" style="width: 221px;"><div>                                    
                                        <div class="widget-title">Returned</div>
                                        <div class="widget-subtitle">Visitors</div>
                                        <div class="widget-int">1,695</div>
                                    </div></div><div class="owl-item" style="width: 221px;"><div>                                    
                                        <div class="widget-title">New</div>
                                        <div class="widget-subtitle">Visitors</div>
                                        <div class="widget-int">1,977</div>
                                    </div></div></div></div>
                                    
                                    
                                <div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page"><span class=""></span></div><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div></div></div>                            
                                <div class="widget-controls">                                
                                    <a title="Remove Widget" data-placement="top" data-toggle="tooltip" class="widget-control-right widget-remove" href="#"><span class="fa fa-times"></span></a>
                                </div>                             
                            </div>         
                            <!-- END WIDGET SLIDER -->
                            
                        </div>
                        <div class="col-md-3">
                            
                            <!-- START WIDGET MESSAGES -->
                            <div onclick="location.href='pages-messages.html';" class="widget widget-default widget-item-icon">
                                <div class="widget-item-left">
                                    <span class="fa fa-trophy"></span>
                                </div>                             
                                <div class="widget-data">
                                    <div class="widget-int num-count">48</div>
                                    <div class="widget-title">Highest Score</div>
                                    <div class="widget-subtitle">Assisted Game</div>
                                </div>      
                                
                            </div>                            
                            <!-- END WIDGET MESSAGES -->
                            
                        </div>
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
                            <div onclick="location.href='pages-address-book.html';" class="widget widget-default widget-item-icon">
                                <div class="widget-item-left">
                                    <span class="fa fa-trophy"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count">375</div>
                                    <div class="widget-title">Highest Score</div>
                                    <div class="widget-subtitle">Non Assisted Game</div>
                                </div>
                                                          
                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        <div class="col-md-3">
                            
                            <!-- START WIDGET CLOCK -->
                            <div class="widget widget-danger widget-padding-sm">
                                <div class="widget-big-int plugin-clock">01<span>:</span>48</div>                            
                                <div class="widget-subtitle plugin-date">Friday, January 30, 2015</div>
                                                            
                            </div>                        
                            <!-- END WIDGET CLOCK -->
                            
                        </div>
                    </div>
                    <!-- END WIDGETS -->                    
                   
                    
                 
                    
                </div>
                
                
                
                
                <div class="content-frame">                                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-tag"></span> Welcome! to Tagit</h2>
                        </div>                                                    
                                                 
                    </div>
                    <!-- END CONTENT FRAME TOP -->
                    
                    <!-- START CONTENT FRAME RIGHT -->
              <!--       <div class="content-frame-right" style="height: 606px;">
                        
                        <div class="list-group list-group-contacts border-bottom push-down-10">
                            <a class="list-group-item" href="#">                                 
                                <div class="list-group-status status-online"></div>
                                <img alt="Dmitry Ivaniuk" class="pull-left" src="assets/images/users/user.jpg">
                                <span class="contacts-title">Dmitry Ivaniuk</span>
                                <p>Hello my friend, how are...</p>
                            </a>                                
                            <a class="list-group-item" href="#">                                    
                                <div class="list-group-status status-online"></div>
                                <img alt="Nadia Ali" class="pull-left" src="assets/images/users/user3.jpg">
                                <span class="contacts-title">Nadia Ali</span>
                                <p>Wanna se my photos?</p>
                            </a>                                                                
                            <a class="list-group-item active" href="#">         
                                <div class="list-group-status status-online"></div>
                                <img alt="John Doe" class="pull-left" src="assets/images/users/user2.jpg">
                                <div class="contacts-title">John Doe <span class="label label-danger">5</span></div>
                                <p>This project is awesome</p>                                       
                            </a>
                            <a class="list-group-item" href="#">         
                                <div class="list-group-status status-away"></div>
                                <img alt="Brad Pitt" class="pull-left" src="assets/images/users/user4.jpg">
                                <span class="contacts-title">Brad Pitt</span>
                                <p>ok</p>                     
                            </a>
                            <a class="list-group-item" href="#">         
                                <div class="list-group-status status-offline"></div>
                                <img alt="Darth Vader" class="pull-left" src="assets/images/users/no-image.jpg">
                                <span class="contacts-title">Darth Vader</span>
                                <p>We should win this war!!!1</p>
                            </a>
                            <a class="list-group-item" href="#">         
                                <div class="list-group-status status-offline"></div>
                                <img alt="Kim Kardashian" class="pull-left" src="assets/images/users/no-image.jpg">
                                <span class="contacts-title">Kim Kardashian</span>
                                <p>You received a letter from Darth?</p>
                            </a>
                            <a class="list-group-item" href="#">         
                                <div class="list-group-status status-offline"></div>
                                <img alt="Jason Statham" class="pull-left" src="assets/images/users/no-image.jpg">
                                <span class="contacts-title">Jason Statham</span>
                                <p>Lets play chess...</p>
                            </a>                            
                        </div>
                        
                        <div class="block">
                            <h4>Status</h4>
                            <div class="list-group list-group-simple">                                
                                <a class="list-group-item" href="#"><span class="fa fa-circle text-success"></span> Online</a>
                                <a class="list-group-item" href="#"><span class="fa fa-circle text-warning"></span> Away</a>
                                <a class="list-group-item" href="#"><span class="fa fa-circle text-muted"></span> Offline</a>                                
                            </div>
                        </div>
                        
                    </div> -->
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="content-frame-body content-frame-body-left" style="height: 666px;">
                        
                        
                       
                        
                            
                            <!-- START WIDGET MESSAGES -->
                            <div onclick="location.href='<c:url value="/singlePlayer/playGame"/>'" class="widget widget-default widget-item-icon" style="cursor: pointer;">
                                <div class="widget-item-left">
                                    <span class="fa fa-gamepad"></span>
                                </div>                             
                                <div class="widget-data" onclick="location.href='<c:url value="/singlePlayer/playGame"/>'">
                                    <div class="widget-int num-count">Click here</div>
                                    <div class="widget-title">To play</div>
                                    <div class="widget-subtitle">Assisted Game For Image</div>
                                </div> 
                                   
                               
                            </div>    
                            
                               <div onclick="location.href='<c:url value="/singlePlayer/playGameMusic"/>'" class="widget widget-default widget-item-icon" style="cursor: pointer;">
                                <div class="widget-item-left">
                                    <span class="fa fa-gamepad"></span>
                                </div>                             
                               
                                
                                 <div class="widget-data" onclick="location.href='<c:url value="/singlePlayer/playGameMusic"/>'">
                                    <div class="widget-int num-count">Click here</div>
                                    <div class="widget-title">To play</div>
                                    <div class="widget-subtitle">Assisted Game For Music</div>
                                </div>      
                               
                            </div>                         
                            <!-- END WIDGET MESSAGES -->
                            
                       
                       
                      <div onclick="location.href='<c:url value="/singlePlayer/playGameNonAssisted"/>'" class="widget widget-default widget-item-icon" style="cursor: pointer;">
                                <div class="widget-item-left">
                                    <span class="fa fa-gamepad"></span>
                                </div>                             
                                <div class="widget-data" onclick="location.href='<c:url value="/singlePlayer/playGameNonAssisted"/>'">
                                    <div class="widget-int num-count">Click here</div>
                                    <div class="widget-title">To Play</div>
                                    <div class="widget-subtitle">Non Assisted Game For Image</div>
                                </div>      
                                
             
                               
                            </div>  
                            
                            
                              <div onclick="location.href='<c:url value="/singlePlayer/playGameNonAssistedMusic"/>'" class="widget widget-default widget-item-icon" style="cursor: pointer;">
                                <div class="widget-item-left">
                                    <span class="fa fa-gamepad"></span>
                                </div>                             
                                   
                                
                                 <div class="widget-data" onclick="location.href='<c:url value="/singlePlayer/playGameNonAssistedMusic"/>'">
                                    <div class="widget-int num-count">Click here</div>
                                    <div class="widget-title">To Play</div>
                                    <div class="widget-subtitle">Non Assisted Game For Music</div>
                                </div>  
                               
                            </div>  
                    <div onclick="location.href='<c:url value="/singlePlayer/searchAttribute"/>'" class="widget widget-default widget-item-icon" style="cursor: pointer;">
                                <div class="widget-item-left">
                                    <span class="fa fa-gamepad"></span>
                                </div>                             
                                <div class="widget-data" onclick="location.href='<c:url value="/singlePlayer/searchAttribute"/>'">
                                    <div class="widget-int num-count">Click here</div>
                                    <div class="widget-title">To Search</div>
                                    <div class="widget-subtitle">Image</div>
                                </div>      
                               
                            </div>  
                    
                        <div onclick="location.href='<c:url value="/singlePlayer/searchTune"/>'" class="widget widget-default widget-item-icon" style="cursor: pointer;">
                                <div class="widget-item-left">
                                    <span class="fa fa-gamepad"></span>
                                </div>                             
                                <div class="widget-data" onclick="location.href='<c:url value="/singlePlayer/searchTune"/>'">
                                    <div class="widget-int num-count">Click here</div>
                                    <div class="widget-title">To Search</div>
                                    <div class="widget-subtitle">TUNE</div>
                                </div>      
                               
                            </div>
                        
                    </div>
                    <!-- END CONTENT FRAME BODY -->      
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                       
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<c:url value="/logout/user"/>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
       <!--  <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio> -->
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/jquery/jquery.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/jquery/jquery-ui.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap.min.js"/>"></script> 
        <script type="text/javascript" src=""></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<c:url value="/resources/bootstrap/js/plugins/icheck/icheck.min.js"/>'></script>        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/scrolltotop/scrolltopcontrol.js"/>"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/morris/raphael-min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/morris/morris.min.js"/>"></script>       
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/rickshaw/d3.v3.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/rickshaw/rickshaw.min.js"/>"></script>
        <script type='text/javascript' src='<c:url value="/resources/bootstrap/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"/>'></script>
        <script type='text/javascript' src='<c:url value="/resources/bootstrap/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"/>'></script>                
                        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/owl/owl.carousel.min.js"/>"></script>                 
          <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap-datepicker.js"/>"></script>    
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/moment.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/daterangepicker/daterangepicker.js"/>"></script>
        <!-- END THIS PAGE PLUGINS-->        
         
         <!-- START FORM PLUGINS-->  
         
        
                  
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap-file-input.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap-select.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/tagsinput/jquery.tagsinput.min.js"/>"></script>
         <!-- END FORM PLUGINS-->
         
        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/settings.js"/>"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins.js"/>"></script>        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/actions.js"/>"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/demo_dashboard.js"/>"></script>
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>






