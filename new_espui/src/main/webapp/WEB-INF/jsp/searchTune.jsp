<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>

<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Tagit</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<c:url value="/resources/bootstrap/css/theme-default.css"/>"/>
        
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/htmlresource/css/countdown_timer/jquery.countdownTimer.css"/>" />
         <link rel="stylesheet" type="text/css" href="<c:url value="/resources/htmlresource/css/jplayer.blue.monday.min.css"/>" />
        <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/jquery/jquery.min.js"/>"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/jquery/jquery-ui.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap.min.js"/>"></script> 
        <script type="text/javascript" src="<c:url value="/resources/htmlresource/js/countdown_timer/jquery.countdownTimer.js"/>"></script>
         <script src="<c:url value="/resources/htmlresource/js/jquery.jplayer.min.js"/>" type="text/javascript"></script>
        <script type="text/javascript" src=""></script>        
        <!-- END PLUGINS -->

       
        
  <script type="text/javascript">
  // BuildMyString.com generated code. Please enjoy your string responsibly.

  var sb ="<div id=\"jp_container_1\" class=\"jp-audio\" role=\"application\" aria-label=\"media player\" style=\"width:100px;height:100px\">" +
  "	<div class=\"jp-type-single\">" +
  "		<div class=\"jp-gui jp-interface\">" +
  "			<div class=\"jp-controls\">" +
  "				<button class=\"jp-play\" role=\"button\" tabindex=\"0\">play</button>" +
  "				<button class=\"jp-stop\" role=\"button\" tabindex=\"0\">stop</button>" +
  "			</div>" +
  "			<div class=\"jp-progress\">" +
  "				<div class=\"jp-seek-bar\">" +
  "					<div class=\"jp-play-bar\"></div>" +
  "				</div>" +
  "			</div>" +
  "			<div class=\"jp-volume-controls\">" +
  "				<button class=\"jp-mute\" role=\"button\" tabindex=\"0\">mute</button>" +
  "				<button class=\"jp-volume-max\" role=\"button\" tabindex=\"0\">max volume</button>" +
  "				<div class=\"jp-volume-bar\">" +
  "					<div class=\"jp-volume-bar-value\"></div>" +
  "				</div>" +
  "			</div>" +
  "			<div class=\"jp-time-holder\">" +
  "				<div class=\"jp-current-time\" role=\"timer\" aria-label=\"time\">&nbsp;</div>" +
  "				<div class=\"jp-duration\" role=\"timer\" aria-label=\"duration\">&nbsp;</div>" +
  "				<div class=\"jp-toggles\">" +
  "					<button class=\"jp-repeat\" role=\"button\" tabindex=\"0\">repeat</button>" +
  "				</div>" +
  "			</div>" +
  "		</div>" +
  "		<div class=\"jp-details\">" +
  "			<div class=\"jp-title\" aria-label=\"title\">&nbsp;</div>" +
  "		</div>" +
  "		<div class=\"jp-no-solution\">" +
  "			<span>Update Required</span>" +
  "			To play the media you will need to either update your browser to a recent version or update your <a href=\"http://get.adobe.com/flashplayer/\" target=\"_blank\">Flash plugin</a>." +
  "		</div>" +
  "	</div>" +
  "</div>";
  		

  		
  
	 function search(){
			var query = $("#query").val();
			if(query && query.trim()!=""){
		   $.ajax({ 
			type: 'POST',
			url: "<%=request.getContextPath()%>/singlePlayer/searchAttrImage",
	   	    data:{query:query,fileType:'MUSIC'},
	   		dataType: "json",
	   		async:true,cache:false,
	   		success: function (result) {
	   			$("#links").html("");
	   			if(result != null && result != ""){
	   				$.each( result, function(i, obj) {
	   					$("#links").append("<div style=\"margin-top:10px;\"></div><div id=\"jquery_jplayer_1"+i+"\" class=\"jp-jplayer\"></div>"+
                             "<div id=\"jp_container_1"+i+"\" class=\"jp-audio\" role=\"application\" aria-label=\"media player\" style=\"width:470px;height:135px\">" +
                             "	<div class=\"jp-type-single\">" +
                             "		<div class=\"jp-gui jp-interface\">" +
                             "			<div class=\"jp-controls\">" +
                             "				<button class=\"jp-play\" role=\"button\" tabindex=\"0\">play</button>" +
                             "				<button class=\"jp-stop\" role=\"button\" tabindex=\"0\">stop</button>" +
                             "			</div>" +
                             "			<div class=\"jp-progress\">" +
                             "				<div class=\"jp-seek-bar\">" +
                             "					<div class=\"jp-play-bar\"></div>" +
                             "				</div>" +
                             "			</div>" +
                             "			<div class=\"jp-volume-controls\">" +
                             "				<button class=\"jp-mute\" role=\"button\" tabindex=\"0\">mute</button>" +
                             "				<button class=\"jp-volume-max\" role=\"button\" tabindex=\"0\">max volume</button>" +
                             "				<div class=\"jp-volume-bar\">" +
                             "					<div class=\"jp-volume-bar-value\"></div>" +
                             "				</div>" +
                             "			</div>" +
                             "			<div class=\"jp-time-holder\">" +
                             "				<div class=\"jp-current-time\" role=\"timer\" aria-label=\"time\">&nbsp;</div>" +
                             "				<div class=\"jp-duration\" role=\"timer\" aria-label=\"duration\">&nbsp;</div>" +
                             "				<div class=\"jp-toggles\">" +
                             "					<button class=\"jp-repeat\" role=\"button\" tabindex=\"0\">repeat</button>" +
                             "				</div>" +
                             "			</div>" +
                             "		</div>" +
                             "		<div class=\"jp-details\">" +
                             "			<div class=\"jp-title\" aria-label=\"title\">&nbsp;</div>" +
                             "		</div>" +
                             "		<div class=\"jp-no-solution\">" +
                             "			<span>Update Required</span>" +
                             "			To play the media you will need to either update your browser to a recent version or update your <a href=\"http://get.adobe.com/flashplayer/\" target=\"_blank\">Flash plugin</a>." +
                             "		</div>" +
                             "	</div>" +
                             "</div>");
	   					
	   					loadMusic(result[i].readFilePath, result[i].imageName,i);
	   				});
	   				
	   			}else{
	   				$("#links").append("<p>No Tunes found.</p>");
	   			}
	     	},
	   		error: function() {
			  
		  	}
		  });
			}else{
				alert("Please enter search key");
			}
	 }
	 
	 function loadMusic(filePath,title,count){
		 var $player = $("#jquery_jplayer_1"+count);
		
		        $player.jPlayer({
		            ready: function() {
		                $(this).jPlayer("setMedia", {
		                	title: title,
							m4a: filePath
		                });
		            },
		            play: function() { // To avoid multiple jPlayers playing together.
		    			$(this).jPlayer("pauseOthers");
		    		},
		            supplied: "m4a, oga",
					
					wmode: "window",
					cssSelectorAncestor: "#jp_container_1"+count,
					useStateClassSkin: true,
					autoBlur: false,
					smoothPlayBar: true,
					keyEnabled: true,
					remainingDuration: true,
					toggleDuration: true
		        });
		    
	
	 }
	
    </script>           
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->       
        
                     
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="index.html">Tagit</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="${sessionScope.user.readImagePath}" />
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="${sessionScope.user.readImagePath}" />
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">${sessionScope.user}</div>
                                
                            </div>
                          
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li class="active">
                        <a href="<c:url value="/user/home"/>"><span class="fa fa-user"></span> <span class="xn-text">Home</span></a>                        
                    </li> 
                     <li >
                        <a href="<c:url value="/user/profile"/>"><span class="fa fa-desktop"></span> <span class="xn-text">Profile</span></a>                        
                    </li>                     
                  
                    
                 
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                                   </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Search</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap" >
                    <div id="loading" style="margin:200px 0px 0px 474px;display:none;"> <img src="<c:url value="/resources/htmlresource/images/portal/loader.GIF"/>"> loading...   </div>
                    <!-- START WIDGETS -->                    
                  <div class="row" style="height:600px;">

                      <div class="col-md-2">
                      </div>
                      <div class="module_content">
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Search By Attribute</label>
								<input type="text" style="width:92%;" id ="query" name="query" placeholder="Enter multiple tags by comma separated">
							</fieldset><div class="clear"></div>
							<button class="btn btn-primary btn-lg"  onclick="search()" style="margin: 16px 382px 0" id="answr-submit-btn" type="button">Search</button>
					</div>
						
                        <!-- PROFILE WIDGET -->
                        <div  id="main-container">
						
                                   <div id="links" class="gallery" >
                                        </div>
                                 
                       
                            </div>

                        </div>
                        
                        <div class="col-md-2" id="btn-div">
                        
                      </div>
                        
                        <!-- END PROFILE WIDGET -->

                        <!-- SETTINGS WIDGET -->
                          

                      <!-- END SETTINGS WIDGET -->

                    </div>
                    <!-- END WIDGETS -->                    
                   
                    
                 
                    
                </div>
               
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                       
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<c:url value="/logout/user"/>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
       <!--  <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio> -->
        <!-- END PRELOADS -->                  
         <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<c:url value="/resources/bootstrap/js/plugins/icheck/icheck.min.js"/>'></script>        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/scrolltotop/scrolltopcontrol.js"/>"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/morris/raphael-min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/morris/morris.min.js"/>"></script>       
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/rickshaw/d3.v3.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/rickshaw/rickshaw.min.js"/>"></script>
        <script type='text/javascript' src='<c:url value="/resources/bootstrap/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"/>'></script>
        <script type='text/javascript' src='<c:url value="/resources/bootstrap/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"/>'></script>                
                        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/owl/owl.carousel.min.js"/>"></script>                 
          <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap-datepicker.js"/>"></script>    
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/moment.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/daterangepicker/daterangepicker.js"/>"></script>
        <!-- END THIS PAGE PLUGINS-->        
         
         <!-- START FORM PLUGINS-->  
         
        
                  
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap-file-input.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap-select.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/tagsinput/jquery.tagsinput.min.js"/>"></script>
         <!-- END FORM PLUGINS-->
         
        <!-- START TEMPLATE -->
        
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins.js"/>"></script>        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/actions.js"/>"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/demo_dashboard.js"/>"></script>
      
    </body>
</html>






