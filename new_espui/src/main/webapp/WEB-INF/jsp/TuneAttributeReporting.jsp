	
	<%@page import="com.esp.model.type.IMAGECATAGORY"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ page isELIgnored="false" %>
	
	<html>
	
	
	<head>
		<meta charset="utf-8"/>
		<title>Dashboard Admin Panel</title>
		
		<link rel="stylesheet" href="<c:url value="/resources/htmlresource/css/adminpanel/css/layout.css" />" type="text/css" media="screen" />
		<!--[if lt IE 9]>
		<link rel="stylesheet" href="<c:url value="/resources/htmlresource/css/adminpanel/css/ie.css" />" type="text/css" media="screen" />
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="<c:url value="/resources/htmlresource/css/digi.alert.css"/>" rel="stylesheet" type="text/css">
		
		
		<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery-1.7.2.js"/>"></script>
		<script src="<c:url value="/resources/htmlresource/css/adminpanel/js/hideshow.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/resources/htmlresource/css/adminpanel/js/jquery.tablesorter.min.js"/>" type="text/javascript"></script>
		<script type="text/javascript" src="<c:url value="/resources/htmlresource/css/adminpanel/js/jquery.equalHeight.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery.alert.js"/>"></script>
		<script type="text/javascript">
		$(document).ready(function() 
	    	{ 
	      	  $(".tablesorter").tablesorter(); 
	     
	   	 } 
		);
		$(document).ready(function() {
	
		//When page loads...
		$(".tab_content").hide(); //Hide all content
		$("ul.tabs li:first").addClass("active").show(); //Activate first tab
		$(".tab_content:first").show(); //Show first tab content
	
		//On Click Event
		/* $("ul.tabs li").click(function() {
	
			$("ul.tabs li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$(".tab_content").hide(); //Hide all tab content
	
			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			return false;
		}); */
	
	});
		
		
		
		
		
	    </script>
	    <script type="text/javascript">
	    $(function(){
	        $('.column').equalHeight();
	    });
	</script>
	
	</head>
	
	
	<body>
	
		<header id="header">
			<hgroup>
				<h1 class="site_title"><a href="<c:url value="/admin/home"/>">Admin Panel</a></h1>
				<h2 class="section_title">TagIt.com</h2><div class="btn_view_site"><a href="<c:url value="/logout/user"/>">Log Out</a></div>
			</hgroup>
		</header> <!-- end of header bar -->
		
		<section id="secondary_bar">
			<div class="user">
				<p>Hi! ${sessionScope.user.firstName}</p>
				<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
			</div>
			<div class="breadcrumbs_container">
				<article class="breadcrumbs"><a href="#">Admin Panel</a> <div class="breadcrumb_divider"></div> <a class="current">Dashboard</a></article>
			</div>
		</section><!-- end of secondary bar -->
		
		<aside id="sidebar" class="column">
			<form class="quick_search">
				<input type="text" value="Quick Search" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
			</form>
			<hr/>
			<h3>Images</h3>
			<ul class="toggle">
				 <li class="icn_new_article"><a href="<c:url value="/admin/musicHome"/>">Upload Tunes</a></li>
			<li class="icn_new_article"><a href="<c:url value="/admin/home"/>">Upload Images</a></li>
				<li class="icn_edit_article"><a href="<c:url value="/admin/manageImageAttribute"/>">View Images</a></li>
				<li class="icn_edit_article"><a href="<c:url value="/admin/manageTuneAttribute"/>">View Tunes</a></li>
				<li class="icn_categories"><a href="<c:url value="/imageCategory/view"/>">Image Category</a></li>
				<li class="icn_categories"><a href="<c:url value="/attributeCategory/view"/>">Attribute Category</a></li>
				<li class="icn_tags"><a href="#">Tags</a></li>
			</ul>
			<h3>Users</h3>
			<ul class="toggle">
				<li class="icn_view_users"><a href="<c:url value="/manageUser/getUserData"/>">View Users</a></li>
				<li class="icn_profile"><a href="<c:url value="/admin/profile"/>">Your Profile</a></li>
			</ul>
			<!-- <h3>Media</h3>
			<ul class="toggle">
				<li class="icn_folder"><a href="#">File Manager</a></li>
				<li class="icn_photo"><a href="#">Gallery</a></li>
				<li class="icn_audio"><a href="#">Audio</a></li>
				<li class="icn_video"><a href="#">Video</a></li>
			</ul> -->
			<h3>Admin</h3>
			<ul class="toggle">
				<li class="icn_settings"><a href="#">Game Settings</a></li>
				<li class="icn_security"><a href="#">Security</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/reporting"/>">Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/attributeReporting"/>">Attribute Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/tuneReporting"/>">Tune Reporting</a></li>
				<li class="icn_security"><a href="#">Tune Attribute Reporting</a></li>
				<li class="icn_jump_back"><a href="<c:url value="/logout/user"/>">Logout</a></li>
			</ul>
			
			<footer>
				<hr />
				<p><strong>Copyright &copy; 2015 Tagit Admin</strong></p>
				
			</footer>
		</aside><!-- end of sidebar -->
		
		<section id="main" class="column">
			
			<h4 class="alert_info">Welcome to the TagIt Admin panel</h4>
			
	
			
			<article class="module width_full">
				<c:if test="${not empty attributeReportingList}">
				 
			   <c:forEach items="${attributeReportingList}" var="listValue"  varStatus="loop">
  					<c:if test="${loop.last}">
			<header><h3 class="tabs_involved">Uploaded Tunes</h3>
		 <ul class="tabs">
	   			<li><a style="cursor: pointer;" onclick="location.href='<c:url value="${listValue.excelDownloadPath}"/>'"><img src="<c:url value='/resources/images/portal/excel.png'/>"  height="20">Download Reporting Excel</a></li>
	    		
			</ul>
			</header>
	       	</c:if> 
				</c:forEach>
			   
	           </c:if>
			<div class="tab_container">
				<div id="tab1" class="tab_content">
				<table class="tablesorter" cellspacing="0"> 
				<thead> 
					<tr> 
	   					<th></th> 
	    				<th>Tune Name</th> 
	    			    <th>Attribute Name</th>
	    			    <th>Category</th>
	    			     <th>Hit Count</th>
	    				 <th>Ignore Count</th>
	    			 
					</tr> 
				</thead> 
				<tbody id="imageBody"> 
				<c:if test="${not empty attributeReportingList}">
				 
			   <c:forEach items="${attributeReportingList}" var="listValue"  varStatus="loop">
			      <c:if test="${!loop.last}">
					<tr id="tr${listValue.image.id}"> 
	   					 <td>${loop.index+1}.</td> 
	    				<td>${listValue.image.imageName}</td> 
	    					    				
	    					    				<td>
	    					    				
	    					    				 <c:forEach items="${listValue.defaultCorrectCountList}" var="listValue1"  varStatus="loop1">
	    					    				     <span>${listValue1.attribute.attrName}</span><br/>
	    					    				 </c:forEach>
	    					    				
	    					    				 <c:forEach items="${listValue.correctHitCountList}" var="listValue1"  varStatus="loop1">
	    					    				     <span>${listValue1.attribute.attrName}</span><br/>
	    					    				 </c:forEach>
	    					    				  </td>
	    					    				  
	    					    				    <td>
	    					    				
	    					    				   <c:forEach items="${listValue.defaultCorrectCountList}" var="listValue1"  varStatus="loop1">
	    					    				     <span>${listValue1.attrCategory}</span><br/>
	    					    				 </c:forEach>
	    					    				 
	    					    				  <c:forEach items="${listValue.correctHitCountList}" var="listValue1"  varStatus="loop1">
	    					    				     <span>${listValue1.attrCategory}</span><br/>
	    					    				 </c:forEach>
	    					    				 </td>
	    					    				  <td>
	    					    				 
	    					    				   <c:forEach items="${listValue.defaultCorrectCountList}" var="listValue1"  varStatus="loop1">
	    					    				     <span>${listValue1.hitCount}</span><br/>
	    					    				 </c:forEach>
	    					    				  
	    					    				  <c:forEach items="${listValue.correctHitCountList}" var="listValue1"  varStatus="loop1">
	    					    				     <span>${listValue1.hitCount}</span><br/>
	    					    				 </c:forEach>
	    					    				 </td>
	    					    				   <td>
	    					    				   
	    					    				    <c:forEach items="${listValue.defaultCorrectCountList}" var="listValue1"  varStatus="loop1">
	    					    				     <span>${listValue1.ignoreCount}</span><br/>
	    					    				 </c:forEach>
	    					    				  
	    					    				  <c:forEach items="${listValue.correctHitCountList}" var="listValue1"  varStatus="loop1">
	    					    				      <c:if test="${listValue1.ignoreCount>0}">
	    					    				     <span>${listValue1.ignoreCount}</span><br/>
	    					    				      </c:if>
	    					    				      
	    					    				        <c:if test="${listValue1.ignoreCount==0}">
	    					    				     <span>-</span><br/>
	    					    				      </c:if>
	    					    				 </c:forEach>
	    					    				 </td>		
	    				 
					</tr> 
				</c:if>
				</c:forEach>
			   
	           </c:if>
					
					
				</tbody> 
				</table>
				</div><!-- end of #tab1 -->
				
							
			</div><!-- end of .tab_container -->
	
			</article><!-- end of post new article -->
			
			
			<div class="clear"></div>
			
			
			
			
			
		<!-- 	<article class="module width_full">
				<header><h3>Basic Styles</h3></header>
					<div class="module_content">
						<h1>Header 1</h1>
						<h2>Header 2</h2>
						<h3>Header 3</h3>
						<h4>Header 4</h4>
						<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Maecenas faucibus mollis interdum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
	
	<p>Donec id elit non mi porta <a href="#">link text</a> gravida at eget metus. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
	
						<ul>
							<li>Donec ullamcorper nulla non metus auctor fringilla. </li>
							<li>Cras mattis consectetur purus sit amet fermentum.</li>
							<li>Donec ullamcorper nulla non metus auctor fringilla. </li>
							<li>Cras mattis consectetur purus sit amet fermentum.</li>
						</ul>
					</div>
			</article>end of styles article -->
			<div class="spacer"></div>
		</section>
	
	
	</body>
	
	</html>