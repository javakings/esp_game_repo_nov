<%@page import="com.esp.model.type.IMAGECATAGORY"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<form action="<%=request.getContextPath()%>/admin/upload" method="post" enctype="multipart/form-data">
<table>
	<tr>
	<td><input type="text" name="imageName">
	</td>
	</tr>
	<tr>
	<td><select name="imageCatagory">
	<%
		for(IMAGECATAGORY catagory : IMAGECATAGORY.values())
		{
	%>
	<option value="<%=catagory %>"><%=catagory %></option>
	<%		
		}
	%>
	</select>
	</td>
	</tr>
	<tr>
	<td><input type="file" name="imageFile"></td>
	
	</tr>
	
	<tr>
	<td><textarea rows="" cols="" name="tabooAttributes"></textarea></td>
	
	</tr>
	<tr>
	<td><input type="submit"></td>
	</tr>
	
</table>

</form>

</body>
</html>