
<%@page import="com.esp.model.type.IMAGECATAGORY"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<html>


<head>
	<meta charset="utf-8"/>
	<title>Dashboard Admin Panel</title>
	
	<link rel="stylesheet" href="<c:url value="/resources/htmlresource/css/adminpanel/css/layout.css" />" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="<c:url value="/resources/htmlresource/css/adminpanel/css/ie.css" />" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link href="<c:url value="/resources/htmlresource/css/digi.alert.css"/>" rel="stylesheet" type="text/css">
	
	
	<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery-1.7.2.js"/>"></script>
	<script src="<c:url value="/resources/htmlresource/css/adminpanel/js/hideshow.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/htmlresource/css/adminpanel/js/jquery.tablesorter.min.js"/>" type="text/javascript"></script>
	<script type="text/javascript" src="<c:url value="/resources/htmlresource/css/adminpanel/js/jquery.equalHeight.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery.alert.js"/>"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
      	  getProfile('${sessionScope.user.userId}');
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
	
	function deleteImage(id){
		jConfirm('Image will be permanently deleted. Would you like to proceed?', 'Confirmation Dialog', 
		  function(r) {
			if(r==true)
				{
			$.ajax({ 
				    type: 'POST',
		    		url: "<%=request.getContextPath()%>/admin/delete",
		    		data:{imageId:id},
		    		dataType: "json",
		    		async:true,cache:false,
		    		success: function (result) {
		   	 			if(result!=""){
		   	 				if(result.status){
		   						$("tr#tr"+id).fadeOut(500);
		   	 				}else{
		   	 				 	var $delAlert=jQuery.noConflict();
		                  		jConfirm(result.status, 'Confirmation Dialog', 
	       					    function(r) {
	       							if(r==true)
	       								{
	       					   			}
	       						
	       					});
		   	 				}
		            	}
		      		},
		    		error: function() {
				  
			  		}
			  });
				}
		});
	}
	
	function deleteAttribute(attrId,imageId){
		jConfirm('Attribute will be permanently deleted. Would you like to proceed?', 'Confirmation Dialog', 
		  function(r) {
			if(r==true)
				{
			$.ajax({ 
				    type: 'POST',
		    		url: "<%=request.getContextPath()%>/admin/deleteAttr",
		    		data:{imageId:imageId,attrId:attrId},
		    		dataType: "json",
		    		async:true,cache:false,
		    		success: function (result) {
		   	 			if(result!=""){
		   	 				if(result.status){
		   	 				
		   	 				editImage(imageId);
		   	 				}else{
		   	 				 	var $delAlert=jQuery.noConflict();
		                  		jConfirm(result.status, 'Confirmation Dialog', 
	       					    function(r) {
	       							if(r==true)
	       								{
	       					   			}
	       						
	       					});
		   	 				}
		            	}
		      		},
		    		error: function() {
				  
			  		}
			  });
				}
		});
	}

	
	function editProfile(id){
		$("#profilediv").html("<fieldset><label><strong>First Name : </strong><input type='box' value='${sessionScope.user.firstName}' id='fname'></label>"+
				"</fieldset><fieldset>"+
				"<label><strong>Last Name : </strong><input type='box' value='${sessionScope.user.lastName}' id='lname'> </label>"+
				"</fieldset><fieldset>"+
				"<label><strong>City : </strong><input type='box' value='${sessionScope.user.city}' id='city'></label>"+
				"</fieldset><fieldset>"+
				"<label><strong>Email : </strong><input type='box' style=\"width:200px;\" value='${sessionScope.user.email}' id='email'> </label>"+
				"</fieldset><footer><div class=\"submit_link\">"+
				"<input type=\"submit\" value=\"Update\" class=\"alt_btn\" onclick=\"updateProfile('"+id+"')\"></div></footer>");
		
	}
	
	function addAttributeInbox(imageId,i){
		
		 $("#imageBody").append("<tr id='attributeBox"+imageId+"'>"+ 
					 "<td></td>"+ 
	    				
	    				
	    				"<td><input type='box' placeholder=\"Enter multiple tags by comma separated\"  id='attributeBox'> </td>"+ 
	    				"<td><input type='submit'  value='Add' title=\"Add Attribute\" onclick=\"addAttribute('"+imageId+"','"+i+"')\"</td>"+ 
					"</tr> ");
		 
		 var $t=$('.width_3_quarter');
			$t.animate({"scrollTop":$('.width_3_quarter')[0].scrollHeight
				
			},'slow');
	}
	
	function updateProfile(userId){
		var fname=$("#fname").val();
		var lname=$("#lname").val();
		var city=$("#city").val();
		var email=$("#email").val();
		if(fname!='' && lname!='' && city!='' && email!=''){
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/admin/updateProfile",
    		data:{fname:fname,lname:lname,city:city,email:email,id:userId},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			window.location.href='<c:url value='/admin/profile'/>';
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		}
	}
	
	function getProfile(userId){
		
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/admin/getProfile",
    		data:{id:userId},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			$("#profilediv").html("<fieldset><form action=\"<%=request.getContextPath()%>/admin/uploadUserImage\" method=\"post\" enctype=\"multipart/form-data\">"+
   	 			"<div style=\"float:left;margin-left:10px; border: 5px ridge gray;\">"+
   	 			"<img src=\"<%=request.getContextPath()%>"+result.msgObj.userReadFilePath+"\" height=\"100\" width=\"100\"></div>"+
				"<label><input type=\"file\"  placeholder=\"Upload here\"  name=\"imageFile\"  tabindex=\"10\">"+
				"<input type=\"hidden\"  value='"+userId+"'  name=\"userId\"  tabindex=\"10\">"+
				"<input type=\"submit\" value=\"Upload\" class=\"alt_btn\"></label>"+
				"</form></fieldset><fieldset><label><strong>First Name :</strong> "+result.msgObj.firstName+"</label>"+
				"</fieldset><fieldset>"+
				"<label><strong>Last Name :</strong> "+result.msgObj.lastName+"</label>"+
				"</fieldset><fieldset>"+
				"<label><strong>City :</strong> "+result.msgObj.city+"</label>"+
				"</fieldset><fieldset>"+
				"<label><strong>Email :</strong> "+result.msgObj.email+"</label>"+
				"</fieldset><div class=\"clear\"></div>");
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	}
	
	function updateImage(imageId){
		var imageCategory=$("#imagecat").val();
		
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/admin/updateImage",
    		data:{imageCategory:imageCategory,imageId:imageId},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			   window.location.href='<c:url value='/admin/home'/>';
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	}
	
	function getCategory(imageCatagory){
		$.ajax({ 
		    type: 'GET',
    		url: "<%=request.getContextPath()%>/admin/getImageCategory",
    		
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			    
   	 			       for(var s=0;s<result.length;s++){
   	 			    	   $("#imagecat").append("<option value='"+result[s]+"'>"+result[s]+"</option>");
   	 			       } 
   	 			       
   	 			   $("#imagecat").val(imageCatagory);   
   	 			      
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	}
	
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="<c:url value="/admin/home"/>">Admin Panel</a></h1>
			<h2 class="section_title">TagIt.com</h2><div class="btn_view_site"><a href="<c:url value="/logout/user"/>">Log Out</a></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p>Hi! ${sessionScope.user.firstName}</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="#">Admin Panel</a> <div class="breadcrumb_divider"></div> <a class="current">Profile</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		<form class="quick_search">
			<input type="text" value="Quick Search" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>
		<h3>Images</h3>
		<ul class="toggle">
		 <li class="icn_new_article"><a href="<c:url value="/admin/musicHome"/>">Upload Tunes</a></li>
			<li class="icn_new_article"><a href="<c:url value="/admin/home"/>">Upload Images</a></li>
			<li class="icn_edit_article"><a href="<c:url value="/admin/manageImageAttribute"/>">View Images</a></li>
				<li class="icn_edit_article"><a href="<c:url value="/admin/manageTuneAttribute"/>">View Tunes</a></li>
			<li class="icn_categories"><a href="<c:url value="/imageCategory/view"/>">Image Category</a></li>
			<li class="icn_categories"><a href="<c:url value="/attributeCategory/view"/>">Attribute Category</a></li>
			<li class="icn_tags"><a href="#">Tags</a></li>
		</ul>
		<h3>Users</h3>
		<ul class="toggle">
			<li class="icn_view_users"><a href="#">View Users</a></li>
			<li class="icn_profile"><a href="#">Your Profile</a></li>
		</ul>
		<!-- <h3>Media</h3>
		<ul class="toggle">
			<li class="icn_folder"><a href="#">File Manager</a></li>
			<li class="icn_photo"><a href="#">Gallery</a></li>
			<li class="icn_audio"><a href="#">Audio</a></li>
			<li class="icn_video"><a href="#">Video</a></li>
		</ul> -->
		<h3>Admin</h3>
		<ul class="toggle">
			<li class="icn_settings"><a href="#">Game Settings</a></li>
			<li class="icn_security"><a href="#">Security</a></li>
					<li class="icn_security"><a href="<c:url value="/gameSummary/reporting"/>">Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/attributeReporting"/>">Attribute Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/tuneReporting"/>">Tune Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/tuneAttributeReporting"/>">Tune Attribute Reporting</a></li>
			<li class="icn_jump_back"><a href="<c:url value="/logout/user"/>">Logout</a></li>
		</ul>
		
		<footer>
			<hr />
			<p><strong>Copyright &copy; 2015 Tagit Admin</strong></p>
			
		</footer>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		
		<h4 class="alert_info">Welcome to the TagIt Admin panel</h4>
		
		
		
		<article class="module width_full">
		
			
			<footer>
			<div style="float:left;">
			<h3>Profile</h3>
			</div>
			
				<div class="submit_link">
					
					<input type="image" src="<c:url value="/resources/htmlresource/css/adminpanel/images/icn_edit.png"/>" title="Edit Profile" onclick="editProfile('${sessionScope.user.userId}')">
				</div>
			</footer>
				<div class="module_content" id="profilediv">
					
				</div>
			
		</article><!-- end of post new article -->
		
		
		
	
		
		<div class="clear"></div>
		
		
		
		
	
		<div class="spacer"></div>
	</section>


</body>

</html>