
<%@page import="com.esp.model.type.IMAGECATAGORY"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<html>


<head>
	<meta charset="utf-8"/>
	<title>Dashboard Admin Panel</title>
	
	<link rel="stylesheet" href="<c:url value="/resources/htmlresource/css/adminpanel/css/layout.css" />" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="<c:url value="/resources/htmlresource/css/adminpanel/css/ie.css" />" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link href="<c:url value="/resources/htmlresource/css/digi.alert.css"/>" rel="stylesheet" type="text/css">
	
	
	<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery-1.7.2.js"/>"></script>
	<script src="<c:url value="/resources/htmlresource/css/adminpanel/js/hideshow.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/htmlresource/css/adminpanel/js/jquery.tablesorter.min.js"/>" type="text/javascript"></script>
	<script type="text/javascript" src="<c:url value="/resources/htmlresource/css/adminpanel/js/jquery.equalHeight.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery.alert.js"/>"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
	
	
	

	
	function editCategory(id){
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/attributeCategory/edit",
    		data:{catId:id},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			      $("#imageBody").html("<tr id=''>"+ 
    					 "<td></td>"+ 
    	    				"<td><input type='box' style='width: 100%;' value='"+result.categoryName+"' name='imageCatagory' id='attrcat'/></td>"+ 
    	    				"<td>"+result.sysCreationDate+"</td>"+ 
    	    				"<td><div class='submit_link'>"+
    						"<input type='submit' value='Update' class='alt_btn' onclick=\"updateCategory('"+id+"')\">  <a href='<c:url value='/attributeCategory/view'/>'>Cancel</a>"+
    						"</div></td>"+ 
    					"</tr> ");
   	 			 
   	 			      
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	}
	

	
	function addAttribute(){
		var catName=$("#attrCatName").val();
		if(catName!=''){
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/attributeCategory/add",
    		data:{catName:catName},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			window.location.href='<c:url value='/attributeCategory/view'/>';
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		}else{
		    alert("Please enter category name");
		}
	}
	
	function updateCategory(catId){
		var catName=$("#attrcat").val();
		
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/attributeCategory/update",
    		data:{catName:catName,catId:catId},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			   window.location.href='<c:url value='/attributeCategory/view'/>';
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	}
	
	
	
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="<c:url value="/admin/home"/>">Admin Panel</a></h1>
			<h2 class="section_title">TagIt.com</h2><div class="btn_view_site"><a href="<c:url value="/logout/user"/>">Log Out</a></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p>Hi! ${sessionScope.user.firstName}</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="#">Admin Panel</a> <div class="breadcrumb_divider"></div> <a class="current">Attribute Category</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		<form class="quick_search">
			<input type="text" value="Quick Search" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>
		<h3>Images</h3>
		<ul class="toggle">
						
			 <li class="icn_new_article"><a href="<c:url value="/admin/musicHome"/>">Upload Tunes</a></li>
			<li class="icn_new_article"><a href="<c:url value="/admin/home"/>">Upload Images</a></li>
			<li class="icn_edit_article"><a href="<c:url value="/admin/manageImageAttribute"/>">View Images</a></li>
				<li class="icn_edit_article"><a href="<c:url value="/admin/manageTuneAttribute"/>">View Tunes</a></li>
<li class="icn_categories"><a href="<c:url value="/imageCategory/view"/>">Image Category</a></li>
			<li class="icn_categories"><a href="#">Attribute Category</a></li>
			<li class="icn_tags"><a href="#">Tags</a></li>
		</ul>
		<h3>Users</h3>
		<ul class="toggle">
			<li class="icn_view_users"><a href="#">View Users</a></li>
			<li class="icn_profile"><a href="<c:url value="/admin/profile"/>">Your Profile</a></li>
		</ul>
		<!-- <h3>Media</h3>
		<ul class="toggle">
			<li class="icn_folder"><a href="#">File Manager</a></li>
			<li class="icn_photo"><a href="#">Gallery</a></li>
			<li class="icn_audio"><a href="#">Audio</a></li>
			<li class="icn_video"><a href="#">Video</a></li>
		</ul> -->
		<h3>Admin</h3>
		<ul class="toggle">
			<li class="icn_settings"><a href="#">Game Settings</a></li>
			<li class="icn_security"><a href="#">Security</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/reporting"/>">Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/attributeReporting"/>">Attribute Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/tuneReporting"/>">Tune Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/tuneAttributeReporting"/>">Tune Attribute Reporting</a></li>
			<li class="icn_jump_back"><a href="<c:url value="/logout/user"/>">Logout</a></li>
		</ul>
		
		<footer>
			<hr />
			<p><strong>Copyright &copy; 2015 Tagit Admin</strong></p>
			
		</footer>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		
		<h4 class="alert_info">Welcome to the TagIt Admin panel</h4>
		
		
		
		<article class="module width_full">
			<header><h3>Stats</h3></header>
			<div class="module_content">
				<article class="stats_graph">
					<img src="http://chart.apis.google.com/chart?chxr=0,0,3000&chxt=y&chs=520x140&cht=lc&chco=76A4FB,80C65A&chd=s:Tdjpsvyvttmiihgmnrst,OTbdcfhhggcTUTTUadfk&chls=2|2&chma=40,20,20,30" width="520" height="140" alt="" />
				</article>
				
				<article class="stats_overview">
					<div class="overview_today">
						<p class="overview_day">Today</p>
						<p class="overview_count">1,876</p>
						<p class="overview_type">Game Played</p>
						<p class="overview_count">2,103</p>
						<p class="overview_type">Tags Added</p>
					</div>
					<div class="overview_previous">
						<p class="overview_day">Yesterday</p>
						<p class="overview_count">1,646</p>
						<p class="overview_type">Game Played</p>
						<p class="overview_count">2,054</p>
						<p class="overview_type">Tags Added</p>
					</div>
				</article>
				<div class="clear"></div>
			</div>
		</article><!-- end of stats article -->
		
		<article class="module width_full">
		
			<header><h3>Add Category</h3></header>
				<div class="module_content">
						
						<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
							<label>Category Name</label>
							<input type="text" style="width:92%;" id="attrCatName" placeholder="Enter category name">
						</fieldset><div class="clear"></div>
				</div>
			<footer>
				<div class="submit_link">
					<!-- <a href="javascript:addAttribute()">Add</a> -->
					<input type="submit" value="Add" class="alt_btn" onclick="addAttribute()">
					<input type="reset" value="Reset">
				</div>
			</footer>
		
		</article><!-- end of post new article -->
		
		<article class="module width_3_quarter">
		<header><h3 class="tabs_involved">Uploaded Images</h3>
		<!-- <ul class="tabs">
   			<li><a href="#tab1">Posts</a></li>
    		<li><a href="#tab2">Comments</a></li>
		</ul> -->
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th></th> 
    				<th>Category</th> 
    				<th>Created On</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody id="imageBody"> 
			<c:if test="${not empty attrcatlist}">
			 
		   <c:forEach items="${attrcatlist}" var="listValue"  varStatus="loop">
		    
				<tr id="tr${listValue.id}"> 
   					 <td>${loop.index+1}.</td> 
    				 
    				<td>${listValue.categoryName}</td> 
    				<td>${listValue.sysCreationDate}</td> 
    				<td><input type="image" src="<c:url value="/resources/htmlresource/css/adminpanel/images/icn_edit.png"/>" title="Edit" onclick="editCategory('${listValue.id}')"></td> 
				</tr> 
			</c:forEach>
		   
           </c:if>
				
				
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
						
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
		
		<article class="module width_quarter">
			<header><h3>Feedbacks</h3></header>
			<div class="message_list">
				<div class="module_content">
					<div class="message"><p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor.</p>
					<p><strong>John Doe</strong></p></div>
					<div class="message"><p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor.</p>
					<p><strong>John Doe</strong></p></div>
					<div class="message"><p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor.</p>
					<p><strong>John Doe</strong></p></div>
					<div class="message"><p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor.</p>
					<p><strong>John Doe</strong></p></div>
					<div class="message"><p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor.</p>
					<p><strong>John Doe</strong></p></div>
				</div>
			</div>
			<footer>
				<form class="post_message">
					<input type="text" value="Message" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
					<input type="submit" class="btn_post_message" value=""/>
				</form>
			</footer>
		</article><!-- end of messages article -->
		
		<div class="clear"></div>
		
		
		
		
		
	<!-- 	<article class="module width_full">
			<header><h3>Basic Styles</h3></header>
				<div class="module_content">
					<h1>Header 1</h1>
					<h2>Header 2</h2>
					<h3>Header 3</h3>
					<h4>Header 4</h4>
					<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Maecenas faucibus mollis interdum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>

<p>Donec id elit non mi porta <a href="#">link text</a> gravida at eget metus. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>

					<ul>
						<li>Donec ullamcorper nulla non metus auctor fringilla. </li>
						<li>Cras mattis consectetur purus sit amet fermentum.</li>
						<li>Donec ullamcorper nulla non metus auctor fringilla. </li>
						<li>Cras mattis consectetur purus sit amet fermentum.</li>
					</ul>
				</div>
		</article>end of styles article -->
		<div class="spacer"></div>
	</section>


</body>

</html>