<%@page import="com.esp.model.type.GENDER"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/htmlresource/css/style.css" />"  />
<link href="<c:url value="/resources/htmlresource/css/newform/new_form_style.css" />"  type="text/css" rel="stylesheet" />
 <link href="<c:url value="/resources/htmlresource/css/newform/responsive.css" />" type="text/css" rel="stylesheet" />
 <script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery-1.11.1.min.js"/>"></script>
   
</head>

<body>
<div id="main_container">

  <div id="middle_box">
  	
  </div>
 
 
   <div class="pattern_bg_reg">
   <div class="middle_box_content"><p style="font-size: 24px;font-weight: bold;margin-left: 318px;">REGISTRATION</p>
   <p style="font-size: 24px;font-weight: bold;margin-left: 149px;color:#FF0000;margin-top:10px;margin-bottom: 10px;"></p>
   </div>
  	<section id="form_container">
		<form name="hongkiat" id="hongkiat-form" method="post"  action="<%=request.getContextPath()%>/registration/add">
		<div id="wrapping" class="clearfix">
			<section id="aligned">
			
		      <section id="prioritycase">
			  <h3 style="color: gray;">Upload Image:</h3>
			  <input type="file" name="document"  placeholder="Upload here"    tabindex="10">
		
			</section>
			<br /> <br />
			 <textarea name="description" id="description" placeholder="Enter project description...." tabindex="2" class="txtblock"></textarea>
			
			</section>
		</div>
		<br/><br/>
		<section id="buttons">
			<input type="reset" name="reset" id="resetbtn" class="resetbtn" value="Reset">
			<input type="submit" name="submit" id="submitbtn" class="submitbtn" tabindex="11" value="Register!">
			<br style="clear:both;">
		</section>
		</form>
	</section>
  </div>

 
</div>
 

</body>
</html>




