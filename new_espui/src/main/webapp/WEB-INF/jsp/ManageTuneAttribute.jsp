
<%@page import="com.esp.model.type.IMAGECATAGORY"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<html>


<head>
	<meta charset="utf-8"/>
	<title>Dashboard Admin Panel</title>
	
	<link rel="stylesheet" href="<c:url value="/resources/htmlresource/css/adminpanel/css/layout.css" />" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="<c:url value="/resources/htmlresource/css/adminpanel/css/ie.css" />" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link href="<c:url value="/resources/htmlresource/css/digi.alert.css"/>" rel="stylesheet" type="text/css">
	
	
	<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery-1.7.2.js"/>"></script>
	<script src="<c:url value="/resources/htmlresource/css/adminpanel/js/hideshow.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/htmlresource/css/adminpanel/js/jquery.tablesorter.min.js"/>" type="text/javascript"></script>
	<script type="text/javascript" src="<c:url value="/resources/htmlresource/css/adminpanel/js/jquery.equalHeight.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery.alert.js"/>"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
	
	function deleteImage(id){
		jConfirm('Image will be permanently deleted. Would you like to proceed?', 'Confirmation Dialog', 
		  function(r) {
			if(r==true)
				{
			$.ajax({ 
				    type: 'POST',
		    		url: "<%=request.getContextPath()%>/admin/delete",
		    		data:{imageId:id},
		    		dataType: "json",
		    		async:true,cache:false,
		    		success: function (result) {
		   	 			if(result!=""){
		   	 				if(result.status){
		   						$("tr#tr"+id).fadeOut(500);
		   	 				}else{
		   	 				 	var $delAlert=jQuery.noConflict();
		                  		jConfirm(result.status, 'Confirmation Dialog', 
	       					    function(r) {
	       							if(r==true)
	       								{
	       					   			}
	       						
	       					});
		   	 				}
		            	}
		      		},
		    		error: function() {
				  
			  		}
			  });
				}
		});
	}
	
	function deleteAttribute(attrId,imageId){
		jConfirm('Attribute will be permanently deleted. Would you like to proceed?', 'Confirmation Dialog', 
		  function(r) {
			if(r==true)
				{
			$.ajax({ 
				    type: 'POST',
		    		url: "<%=request.getContextPath()%>/admin/deleteAttr",
		    		data:{imageId:imageId,attrId:attrId},
		    		dataType: "json",
		    		async:true,cache:false,
		    		success: function (result) {
		   	 			if(result!=""){
		   	 				if(result.status){
		   	 				
		   	 				editImage(imageId);
		   	 				}else{
		   	 				 	var $delAlert=jQuery.noConflict();
		                  		jConfirm(result.status, 'Confirmation Dialog', 
	       					    function(r) {
	       							if(r==true)
	       								{
	       					   			}
	       						
	       					});
		   	 				}
		            	}
		      		},
		    		error: function() {
				  
			  		}
			  });
				}
		});
	}
	
	function showImageAttr(id){
		$("p#notification").html("");
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/admin/editImageAll",
    		data:{imageId:id},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			      $("#imageBody").html("<tr id=''>"+ 
    					 "<td></td>"+ 
    	    				"<td>"+result.imageName+"</td>"+ 
    	    				"<td>NA</td>"+ 
    	    				"<td>"+result.sysCreationDate+"</td>"+ 
    	    				"<td><div class='submit_link'>"+
    						"<input type='submit' value='Update' class='alt_btn' onclick=\"updateImage('"+id+"')\">  <a href='<c:url value='/admin/manageTuneAttribute'/>'>Cancel</a>"+
    						"</div></td>"+ 
    					"</tr> ");
   	 			  $("#imageBody").append("<tr><td>Attibute Name</td></tr>");
   	 			      for(var i=0;i<result.attributeList.length;i++){
   	 			       $("#imageBody").append("<tr id='attrtd"+result.attributeList[i].id+"'>"+ 
     					 	
     	    				"<td>"+result.attributeList[i].attrValue+"</td>"+ 
     	    				
     					"</tr> ");
   	 			   getAttrCategory(result.attributeList[i].attrCategory.id,result.attributeList[i].id);
   	 			getAttrType(result.attributeList[i].type,result.attributeList[i].id);
   	 			       
   	 			      }
   	 			      
   	 			  getCategory(result.imageCatagory);
   	 			      
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	}

	
	function editImage(id){
		$("p#notification").html("");
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/admin/editImageAll",
    		data:{imageId:id},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			      $("#imageBody").html("<tr id=''>"+ 
    					 "<td></td>"+ 
    					 "<td><input type='box' value='"+result.imageName+"' id='imgName"+id+"'></td>"+  
    	    				"<td>Music</td>"+ 
     	    				
    	    				"<td>"+result.sysCreationDate+"</td>"+ 
    	    				"<td><div class='submit_link'>"+
    						"<input type='submit' value='Update' class='alt_btn' onclick=\"updateImage('"+id+"')\">  <a href='<c:url value='/admin/manageTuneAttribute'/>'>Cancel</a>"+
    						"</div></td>"+ 
    					"</tr> ");
   	 			  $("#imageBody").append("<tr><td>Sr No.</td><td>Name</td><td>Category</td><td>Type</td><td>Actions</td></tr>");
   	 			      for(var i=0;i<result.attributeList.length;i++){
   	 			       $("#imageBody").append("<tr id='attrtd"+result.attributeList[i].id+"'>"+ 
     					 	"<td>Attribute "+eval(i+1)+"</td>"+ 
     	    				"<td><input type='box' id='box"+result.attributeList[i].id+"' value='"+result.attributeList[i].attrValue+"'></td>"+ 
     	    				"<td>"+result.attributeList[i].attrCategory.categoryName+"</td>"+ 
     	    				"<td><input type='hidden' id='attrcat"+result.attributeList[i].id+"' value='"+result.attributeList[i].attrCategory.id+"'></td>"+
     	    				"<td><select style='width: 100%;' name='attributeType' id='attrType"+result.attributeList[i].id+"'></select></td>"+ 
     	    				"<td><input type='image' src='<c:url value='/resources/htmlresource/css/adminpanel/images/icn_trash.png'/>' title=\"Delete Attribute\" onclick=\"deleteAttribute('"+result.attributeList[i].id+"','"+id+"')\" "+ 
     	    				"<td><a href=\"javascript:updateAttribute('"+id+"','"+result.attributeList[i].id+"')\">Update</a></td>"+ 
     					"</tr> ");
   	 			  // getAttrCategory(result.attributeList[i].attrCategory.id,result.attributeList[i].id);
   	 			getAttrType(result.attributeList[i].type,result.attributeList[i].id);
   	 			       
   	 			      }
   	 			      
   	 			  //getCategory(result.imageCatagory);
   	 			      
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	}
	
	function addAttributeInbox(imageId,i){
		
		 $("#imageBody").append("<tr id='attributeBox"+imageId+"'>"+ 
					 "<td></td>"+ 
	    				
	    				
	    				"<td><input type='box' placeholder=\"Enter multiple tags by comma separated\"  id='attributeBox'> </td>"+ 
	    				"<td><input type='submit'  value='Add' title=\"Add Attribute\" onclick=\"addAttribute('"+imageId+"','"+i+"')\"</td>"+ 
					"</tr> ");
		 
		 var $t=$('.width_3_quarter');
			$t.animate({"scrollTop":$('.width_3_quarter')[0].scrollHeight
				
			},'slow');
	}
	
	function updateAttribute(imageId,attrId){
		$("p#notification").html("");
	var attrCategoryId=$("#attrcat"+attrId).val();
	var attrType=$("#attrType"+attrId).val();
	var attrName=$("#box"+attrId).val();
	
		
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/admin/updateImageAttribute",
    		data:{attrCategoryId:attrCategoryId,imageId:imageId,attrId:attrId,attrName:attrName,attrType:attrType},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			   $("p#notification").html("Successfully updated");
            	}
      		},
    		error: function() {
		  
	  		}
	  });
	}
	
	function addAttribute(imageId,j){
		var attribute=$("#attributeBox").val();
		if(attribute!=''){
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/admin/addAttribute",
    		data:{attribute:attribute,imageId:imageId},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			for(var i=0;i<result.objList.length;i++){
   	 			$("#attributeBox"+imageId).remove();
   	 			   $("#imageBody").append("<tr id='attrtd"+result.objList[i].id+"'>"+ 
       					 "<td></td>"+ 
       	    				"<td></td>"+ 
       	    				"<td>Attribute "+eval(i+parseInt(j)+1)+"</td>"+ 
       	    				"<td>"+result.objList[i].attrValue+"</td>"+ 
       	    				"<td><input type='image' src='<c:url value='/resources/htmlresource/css/adminpanel/images/icn_trash.png'/>' title=\"Delete Attribute\" onclick=\"deleteAttribute('"+result.objList[i].id+"','"+imageId+"')\" "+ 
       	    				"<td><a href=\"javascript:addAttributeInbox('"+imageId+"')\">Add more Tags</a></td>"+ 
       					"</tr> ");
   	 			}
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		}
	}
	
	function updateImage(imageId){
		$("p#notification").html("");
	
		var imageName=$("#imgName"+imageId).val();
		$.ajax({ 
		    type: 'POST',
    		url: "<%=request.getContextPath()%>/admin/updateMusic",
    		data:{imageId:imageId,imageName:imageName},
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			$("p#notification").html("Successfully updated");
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	}
	
<%-- 	function getCategory(imageCatagory){
		$.ajax({ 
		    type: 'GET',
    		url: "<%=request.getContextPath()%>/admin/getImageCategory",
    		
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			    
   	 			       for(var s=0;s<result.length;s++){
   	 			    	   $("#imagecat").append("<option value='"+result[s].id+"'>"+result[s].categoryName+"</option>");
   	 			       } 
   	 			       
   	 			   $("#imagecat").val(imageCatagory);   
   	 			      
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	}
	
	function getAttrCategory(attrCategory,attrId){
		$.ajax({ 
		    type: 'GET',
    		url: "<%=request.getContextPath()%>/admin/getAttrCategory",
    		
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			    
   	 			       for(var s=0;s<result.length;s++){
   	 			    	   $("#attrcat"+attrId).append("<option value='"+result[s].id+"'>"+result[s].categoryName+"</option>");
   	 			       } 
   	 			       
   	 			   $("#attrcat"+attrId).val(attrCategory);   
   	 			      
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	} --%>
	
	function getAttrType(attrType,attrId){
		$.ajax({ 
		    type: 'GET',
    		url: "<%=request.getContextPath()%>/admin/getAttributeType",
    		
    		dataType: "json",
    		async:true,cache:false,
    		success: function (result) {
   	 			if(result!=""){
   	 			    
   	 			       for(var s=0;s<result.length;s++){
   	 			    	   $("#attrType"+attrId).append("<option value='"+result[s]+"'>"+result[s]+"</option>");
   	 			       } 
   	 			       
   	 			   $("#attrType"+attrId).val(attrType);   
   	 			      
            	}
      		},
    		error: function() {
		  
	  		}
	  });
		
	}
	
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="<c:url value="/admin/home"/>">Admin Panel</a></h1>
			<h2 class="section_title">TagIt.com</h2><div class="btn_view_site"><a href="<c:url value="/logout/user"/>">Log Out</a></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p>Hi! ${sessionScope.user.firstName}</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="#">Admin Panel</a> <div class="breadcrumb_divider"></div> <a class="current">Manage Image Attribute</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		<form class="quick_search">
			<input type="text" value="Quick Search" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>
		<h3>Images</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="<c:url value="/admin/home"/>">Upload Images</a></li>
			<li class="icn_edit_article"><a href="<c:url value="/admin/manageImageAttribute"/>">View Images</a></li>
				<li class="icn_edit_article"><a href="#">View Tunes</a></li>
			<li class="icn_categories"><a href="<c:url value="/imageCategory/view"/>">Image Category</a></li>
			<li class="icn_categories"><a href="<c:url value="/attributeCategory/view"/>">Attribute Category</a></li>
			<li class="icn_tags"><a href="#">Tags</a></li>
		</ul>
		<h3>Users</h3>
		<ul class="toggle">
			<li class="icn_view_users"><a href="<c:url value="/manageUser/getUserData"/>">View Users</a></li>
			<li class="icn_profile"><a href="<c:url value="/admin/profile"/>">Your Profile</a></li>
		</ul>
		<!-- <h3>Media</h3>
		<ul class="toggle">
			<li class="icn_folder"><a href="#">File Manager</a></li>
			<li class="icn_photo"><a href="#">Gallery</a></li>
			<li class="icn_audio"><a href="#">Audio</a></li>
			<li class="icn_video"><a href="#">Video</a></li>
		</ul> -->
		<h3>Admin</h3>
		<ul class="toggle">
			<li class="icn_settings"><a href="#">Game Settings</a></li>
			<li class="icn_security"><a href="#">Security</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/reporting"/>">Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/attributeReporting"/>">Attribute Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/tuneReporting"/>">Tune Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/tuneAttributeReporting"/>">Tune Attribute Reporting</a></li>
			<li class="icn_jump_back"><a href="<c:url value="/logout/user"/>">Logout</a></li>
		</ul>
		
		<footer>
			<hr />
			<p><strong>Copyright &copy; 2015 Tagit Admin</strong></p>
			
		</footer>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		
		<h4 class="alert_info">Welcome to the TagIt Admin panel</h4>
		
		<p id="notification" style="font-size: 15px;margin-left:40px;color:red;"></p>
		
		<article class="module width_3_quarter" style="width:800px;height:600px;">
		<header><h3 class="tabs_involved">Uploaded Images</h3>
		<!-- <ul class="tabs">
   			<li><a href="#tab1">Posts</a></li>
    		<li><a href="#tab2">Comments</a></li>
		</ul> -->
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th></th> 
    				<th>Tune Name</th> 
    				<th>Category</th> 
    				<th>Created On</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody id="imageBody"> 
			<c:if test="${not empty imagelist}">
			 
		   <c:forEach items="${imagelist}" var="listValue"  varStatus="loop">
		    
				<tr id="tr${listValue.id}"> 
   					 <td>${loop.index+1}.</td> 
    				<td>${listValue.imageName}</td> 
    				<td>NA</td> 
    				<td>${listValue.sysCreationDate}</td> 
    				<td><a href="javascript:showImageAttr('${listValue.id}')">View Attribute</a><input type="image" src="<c:url value="/resources/htmlresource/css/adminpanel/images/icn_edit.png"/>" title="Edit" onclick="editImage('${listValue.id}')"><input type="image" src="<c:url value="/resources/htmlresource/css/adminpanel/images/icn_trash.png"/>" title="Trash" onclick="deleteImage('${listValue.id}')"></td> 
				</tr> 
			</c:forEach>
		   
           </c:if>
				
				
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
						
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
		
		
		
		<div class="clear"></div>
		
		

		<div class="spacer"></div>
	</section>


</body>

</html>