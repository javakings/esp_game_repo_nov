<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>

<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Tagit</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<c:url value="/resources/bootstrap/css/theme-default.css"/>"/>
        
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/htmlresource/css/countdown_timer/jquery.countdownTimer.css"/>" />
           <link rel="stylesheet" type="text/css" href="<c:url value="/resources/htmlresource/css/jplayer.blue.monday.min.css"/>" />

        <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/jquery/jquery.min.js"/>"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/jquery/jquery-ui.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap.min.js"/>"></script> 
        <script type="text/javascript" src="<c:url value="/resources/htmlresource/js/countdown_timer/jquery.countdownTimer.js"/>"></script>
        <script src="<c:url value="/resources/htmlresource/js/jquery.jplayer.min.js"/>" type="text/javascript"></script>
        <script type="text/javascript" src=""></script>        
        <!-- END PLUGINS -->

       
        
        <script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	 
      	 loadImagesForGame(),
    	
      	$("#current_timer").countdowntimer({
			currentTime : true,
	                size : 'lg'
	                
		});
       
   	 } 
	);
	
	$(function(){
		
	});
	
	 var imageNo=0;
	 var imagesToshow=0;
	 var timeTaken;
	 function getImage(){
		 if(imagesToshow>0 && imageNo<imagesToshow){
			 $("#main-container").hide();
			 $("#loading").show();
	    $.ajax({ 
			    type: 'POST',
	    		url: "<%=request.getContextPath()%>/singlePlayer/getImage",
	    		data:{imageNo:imageNo},
	    		dataType: "json",
	    		async:true,cache:false,
	    		success: function (result) {
	    			$("#answers").empty();	
	    			loadMusic(result.readFilePath,result.imageName);
	    		  for(var i=0;i<result.attributeList.length;i++){
	    			  $("#answers").append("<div class=\"form-group\">"+
	    	                  "<label style=\"font-size:24px;text-transform:uppercase;\" class=\"col-md-4 control-label text-left\">"+result.attributeList[i].attrName+"</label>"+
	    	                  "<div class=\"col-md-3\">"+
	    	                                                                 
	    	                  "</div>"+
	    	                  "<div class=\"col-md-5\">"+
	    	                  "<label class=\"switch\">"+
	    	                  "<input type=\"checkbox\" name=\"answer\" onclick=\"enableButton()\" value=\""+result.attributeList[i].id+"\" class=\"switch\">"+
	    	                  "<span></span>"+
	    	                       "</label>"+
	    	                   "</div>"+
	    	                  
	    	                     "</div>");
	    		      }
	    		  
	    		  if(imageNo==imagesToshow-1){
	    			  $("#answers").append("<div class=\"col-md-1\"><button class=\"btn btn-primary btn-lg\" disabled onclick=\"submitAnswers('"+result.imageId+"',true)\" style=\"margin: 3px 275px 0\" id=\"answr-submit-btn\" type=\"button\">Evaluate Game</button></div>");
	    		  }else{
	    		  
	    		      $("#answers").append("<div class=\"col-md-1\" style=\"margin: 3px 275px 0\"><button class=\"btn btn-primary btn-lg\" disabled onclick=\"submitAnswers('"+result.imageId+"',false)\"  id=\"answr-submit-btn\" type=\"button\">Next Image</button></div>");
	    		      $("#answers").append("<div class=\"col-md-1\" style=\"margin:3px -542px 0\" ><button class=\"btn btn-primary btn-lg\"  onclick=\"getImage()\"  id=\"skip-submit-btn\" type=\"button\">Skip</button></div>");
	    		       $("#answers").append("<div class=\"col-md-1\" style=\"margin: 3px -53px 0\"><button class=\"btn btn-primary btn-lg\" disabled onclick=\"submitAnswers('"+result.imageId+"',true)\"  id=\"exit-submit-btn\" type=\"button\">Exit</button></div>");
	    		  }
	    		  
	    		  imageNo++;
	    		  
	    		  setTimeout(
	    				  function() 
	    				  {
	    					  $("#loading").hide();
	    		    		  $("#main-container").show();
	    				  }, 1000);
	    		 
	    		  timeTaken=new Date().getTime();
	    		 
	      		},
	    		error: function() {
			  
		  		}
		  });
		 }else{
		     $("#imagetoshow").remove();
	              $("#head-info").remove();
	              $("#answers").html("<div class=\"error-container\">"+
                     " <div class=\"error-code\">404</div>"+
                     "<div class=\"error-text\">NO IMAGES FOUND</div>"+
                     
                    " <div class=\"error-actions\">"+                                
                      "   <div class=\"row\">"+
                        
                            " <div class=\"col-md-6\">"+
                              "   <button onclick=\" document.location.href = '<c:url value="/user/home"/>';\" class=\"btn btn-primary btn-block btn-lg\">Retun to home page</button>"+
                            " </div>"+
                         "</div>"+                                
                     "</div>"+
                      " </div>");
		 }
			
		}
	 
	 function loadMusic(filePath,title){
		 var $player = $("#jquery_jplayer_1");
		    if ($player.data().jPlayer && $player.data().jPlayer.internal.ready === true) {
		        $player.jPlayer("setMedia", {
		        	title: "",
					m4a: filePath
		        });
		    }
		    else {
		        $player.jPlayer({
		            ready: function() {
		                $(this).jPlayer("setMedia", {
		                	title: "",
							m4a: filePath
		                });
		            },
		            supplied: "m4a, oga",
					
					wmode: "window",
					useStateClassSkin: true,
					autoBlur: false,
					smoothPlayBar: true,
					keyEnabled: true,
					remainingDuration: true,
					toggleDuration: true
		        });
		    }
	
	 }
	 
	 function loadImagesForGame(){
		  imageNo=0;
		  imagesToshow=0;
		    $.ajax({ 
			    type: 'GET',
	    		url: "<%=request.getContextPath()%>/singlePlayer/loadTunesForGame",
	    	 	dataType: "json",
	    		async:true,cache:false,
	    		success: function (result) {
	    		 imagesToshow=result.message;
	    		 if(imagesToshow==0 || imagesToshow==null){
	    			   $("#imagetoshow").remove();
	 	              $("#head-info").remove();
	 	              $("#answers").html("<div class=\"error-container\">"+
	                      " <div class=\"error-code\">404</div>"+
	                      "<div class=\"error-text\">NO IMAGES FOUND</div>"+
	                      
	                     " <div class=\"error-actions\">"+                                
	                       "   <div class=\"row\">"+
	                         
	                             " <div class=\"col-md-12\">"+
	                               "   <button onclick=\" document.location.href = '<c:url value="/user/home"/>';\" class=\"btn btn-primary btn-block btn-lg\">Retun to home page</button>"+
	                             " </div>"+
	                          "</div>"+                                
	                      "</div>"+
	                       " </div>");
	 	             setTimeout(
		    				  function() 
		    				  {
		    					 
		    		    		  $("#main-container").show();
		    				  }, 1000);
	    		 }else{
	    		   getImage();
	    		 }
	      		},
	    		error: function() {
			  
		  		}
		  });
	 }
	 
	 function submitAnswers(imageId,isEvaluateGame){
		 $("#answr-submit-btn").attr('disabled', true);
		 var timeDiff=Math.round((new Date().getTime() - timeTaken)/1000);
		
		 var answers = [];
	     $.each($("input[name='answer']:checked"), function(){            
	    	 answers.push($(this).val());
	     });
		   $.ajax({ 
			type: 'POST',
			headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
		    },

	   		url: "<%=request.getContextPath()%>/singlePlayer/submitAnswers",
	   	    data:JSON.stringify({imageId:imageId,answers:answers,timeTaken:timeDiff}),
	   		dataType: "json",
	   		async:true,cache:false,
	   		success: function (result) {
	   			if(isEvaluateGame){
	   				evaluateGame();
	   			}else{
	   			 getImage();   
	   			}
	     		},
	   		error: function() {
			  
		  		}
		  });
	 }
	 
	
	function evaluateGame(){
		 $("#main-container").hide();
		 $("#loading").show();
		$("#answr-submit-btn").attr('disabled', true);
		   $.ajax({ 
			type: 'POST',
			url: "<%=request.getContextPath()%>/singlePlayer/evaluateGame",
			data:{fileType:'MUSIC'},
	   	    dataType: "json",
	   		async:true,cache:false,
	   		success: function (result) {
	   	                 $("#imagetoshow").remove();
	   	              $("#head-info").html("<h3 class=\"panel-title\"><span class=\"fa fa-list-alt\"></span> Your Score Card</h3>");
	   	              $("#answers").html("<div class=\"error-container\">"+
                              " <div class=\"error-code\"><span class=\"fa fa-trophy\"></span></div>"+
                              "<div class=\"error-text\">"+result.message+"</div>"+
                              
                             " <div class=\"error-actions\">"+                                
                               "   <div class=\"row\">"+
                                 "     <div class=\"col-md-6\">"+
                                   "       <a href = '<c:url value="/singlePlayer/playGameMusic"/>';\" class=\"btn btn-info btn-block btn-lg\">Play Again</a>"+
                                   "   </div>"+
                                     " <div class=\"col-md-6\">"+
                                       "   <a href = '<c:url value="/user/home"/>';\" class=\"btn btn-primary btn-block btn-lg\">Retun to home page</a>"+
                                     " </div>"+
                                  "</div>"+                                
                              "</div>"+
                               " </div>");
	   	           setTimeout(
		    				  function() 
		    				  {
		    					  $("#loading").hide();
		    		    		  $("#main-container").show();
		    				  }, 1000);
	   	          
	     		},
	   		error: function() {
			  
		  		}
		  });
	}
	
	function enableButton(){
		 var answers = [];
	     $.each($("input[name='answer']:checked"), function(){            
	    	 answers.push($(this).val());
	     });
	     
	     if(answers.length>0){
	    	 $("#answr-submit-btn").removeAttr('disabled');
	    	 $("#exit-submit-btn").removeAttr('disabled');
	     }else{
	    	 $("#answr-submit-btn").attr('disabled', true);
	    	 $("#exit-submit-btn").attr('disabled', true);
	     }
	}
	
    </script>           
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->       
        
                     
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="index.html">Tagit</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="${sessionScope.user.readImagePath}" />
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="${sessionScope.user.readImagePath}" />
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">${sessionScope.user}</div>
                                
                            </div>
                          
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li class="active">
                        <a href="<c:url value="/user/home"/>"><span class="fa fa-user"></span> <span class="xn-text">Home</span></a>                        
                    </li> 
                     <li >
                        <a href="<c:url value="/user/profile"/>"><span class="fa fa-desktop"></span> <span class="xn-text">Profile</span></a>                        
                    </li>                     
                  
                    
                 
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- MESSAGES -->
                   <%--  <li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-comments"></span></a>
                        <div class="informer informer-danger">4</div>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-comments"></span> Messages</h3>                                
                                <div class="pull-right">
                                    <span class="label label-danger">4 new</span>
                                </div>
                            </div>
                            <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-online"></div>
                                    <img src="<c:url value="/resources/bootstrap/assets/images/users/user2.jpg"/>" class="pull-left" alt="John Doe"/>
                                    <span class="contacts-title">John Doe</span>
                                    <p>Praesent placerat tellus id augue condimentum</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-away"></div>
                                    <img src="<c:url value="/resources/bootstrap/assets/images/users/user.jpg"/>" class="pull-left" alt="Dmitry Ivaniuk"/>
                                    <span class="contacts-title">Dmitry Ivaniuk</span>
                                    <p>Donec risus sapien, sagittis et magna quis</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-away"></div>
                                    <img src="<c:url value="/resources/bootstrap/assets/images/users/user3.jpg"/>" class="pull-left" alt="Nadia Ali"/>
                                    <span class="contacts-title">Nadia Ali</span>
                                    <p>Mauris vel eros ut nunc rhoncus cursus sed</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-offline"></div>
                                    <img src="<c:url value="/resources/bootstrap/assets/images/users/user6.jpg"/>" class="pull-left" alt="Darth Vader"/>
                                    <span class="contacts-title">Darth Vader</span>
                                    <p>I want my money back!</p>
                                </a>
                            </div>     
                            <div class="panel-footer text-center">
                                <a href="pages-messages.html">Show all messages</a>
                            </div>                            
                        </div>                        
                    </li> --%>
                    <!-- END MESSAGES -->
                    <!-- TASKS -->
                   <!--  <li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-tasks"></span></a>
                        <div class="informer informer-warning">3</div>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-tasks"></span> Tasks</h3>                                
                                <div class="pull-right">
                                    <span class="label label-warning">3 active</span>
                                </div>
                            </div>
                            <div class="panel-body list-group scroll" style="height: 200px;">                                
                                <a class="list-group-item" href="#">
                                    <strong>Phasellus augue arcu, elementum</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">50%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 25 Sep 2014 / 50%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Aenean ac cursus</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">80%</div>
                                    </div>
                                    <small class="text-muted">Dmitry Ivaniuk, 24 Sep 2014 / 80%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Lorem ipsum dolor</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">95%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 23 Sep 2014 / 95%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Cras suscipit ac quam at tincidunt.</strong>
                                    <div class="progress progress-small">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 21 Sep 2014 /</small><small class="text-success"> Done</small>
                                </a>                                
                            </div>     
                            <div class="panel-footer text-center">
                                <a href="pages-tasks.html">Show all tasks</a>
                            </div>                            
                        </div>                        
                    </li> -->
                    <!-- END TASKS -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Single Player Game</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap" >
                    <div id="loading" style="margin:200px 0px 0px 474px;display:none;"> <img src="<c:url value="/resources/htmlresource/images/portal/loader.GIF"/>"> loading...   </div>
                    <!-- START WIDGETS -->                    
                  <div class="row" style="height:600px;">

                      <div class="col-md-2">
                      </div>

                        <!-- PROFILE WIDGET -->
                        <div class="col-md-8" style="display: none;" id="main-container">

                            <div class="panel panel-default" id="imagetoshow">
                                <div class="panel-body profile bg-info" style="height:250px;" >

                                 
                                   <div class="profile-image" id="answer-img-show">
                                     <div id="jquery_jplayer_1" class="jp-jplayer"></div>
<div id="jp_container_1" class="jp-audio" role="application" aria-label="media player" style="width:703px;height:219px">
	<div class="jp-type-single">
		<div class="jp-gui jp-interface">
			<div class="jp-controls">
				<button class="jp-play" role="button" tabindex="0">play</button>
				<button class="jp-stop" role="button" tabindex="0">stop</button>
			</div>
			<div class="jp-progress">
				<div class="jp-seek-bar">
					<div class="jp-play-bar"></div>
				</div>
			</div>
			<div class="jp-volume-controls">
				<button class="jp-mute" role="button" tabindex="0">mute</button>
				<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
				<div class="jp-volume-bar">
					<div class="jp-volume-bar-value"></div>
				</div>
			</div>
			<div class="jp-time-holder">
				<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
				<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
				<div class="jp-toggles">
					<button class="jp-repeat" role="button" tabindex="0">repeat</button>
				</div>
			</div>
		</div>
		<div class="jp-details">
			<div class="jp-title" aria-label="title">&nbsp;</div>
		</div>
		<div class="jp-no-solution">
			<span>Update Required</span>
			To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
		</div>
	</div>
</div>
                                        <%-- <img alt="John Doe" height="223" style="border-radius:0% !important;width:646px;" src="<c:url value="/resources/bootstrap/assets/images/users/avatar.jpg"/>"> --%>
                                    </div>                             
                                                                          
                                                                                                       
                                </div>

                                </div>
                                
                                
                                <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle" id="head-info">
                                    <h3 class="panel-title"><span class="fa fa-check-square-o"></span> Choose Correct Answers</h3>                                
                                                                       
                                </div>
                                <div class="panel-body">
                                <!-- <div id="countdowntimer"><span id="future_date"><span></div> -->
                                    <form class="form-horizontal" id="answers" role="form" action="#">
                                                                      
                                        
                                    </form>
                                </div>
                            </div>                           
                            </div>

                        </div>
                        
                        <div class="col-md-2" id="btn-div">
                        
                      </div>
                        
                        <!-- END PROFILE WIDGET -->

                        <!-- SETTINGS WIDGET -->
                          

                      <!-- END SETTINGS WIDGET -->

                    </div>
                    <!-- END WIDGETS -->                    
                   
                    
                 
                    
                </div>
               
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                       
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<c:url value="/logout/user"/>" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
       <!--  <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio> -->
        <!-- END PRELOADS -->                  
         <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<c:url value="/resources/bootstrap/js/plugins/icheck/icheck.min.js"/>'></script>        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/scrolltotop/scrolltopcontrol.js"/>"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/morris/raphael-min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/morris/morris.min.js"/>"></script>       
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/rickshaw/d3.v3.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/rickshaw/rickshaw.min.js"/>"></script>
        <script type='text/javascript' src='<c:url value="/resources/bootstrap/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"/>'></script>
        <script type='text/javascript' src='<c:url value="/resources/bootstrap/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"/>'></script>                
                        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/owl/owl.carousel.min.js"/>"></script>                 
          <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap-datepicker.js"/>"></script>    
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/moment.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/daterangepicker/daterangepicker.js"/>"></script>
        <!-- END THIS PAGE PLUGINS-->        
         
         <!-- START FORM PLUGINS-->  
         
        
                  
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap-file-input.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/bootstrap/bootstrap-select.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins/tagsinput/jquery.tagsinput.min.js"/>"></script>
         <!-- END FORM PLUGINS-->
         
        <!-- START TEMPLATE -->
        
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/plugins.js"/>"></script>        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/actions.js"/>"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/demo_dashboard.js"/>"></script>
      
    </body>
</html>






