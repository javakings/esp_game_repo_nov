	
	<%@page import="com.esp.model.type.IMAGECATAGORY"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ page isELIgnored="false" %>
	
	<html>
	
	
	<head>
		<meta charset="utf-8"/>
		<title>Dashboard Admin Panel</title>
		
		<link rel="stylesheet" href="<c:url value="/resources/htmlresource/css/adminpanel/css/layout.css" />" type="text/css" media="screen" />
		<!--[if lt IE 9]>
		<link rel="stylesheet" href="<c:url value="/resources/htmlresource/css/adminpanel/css/ie.css" />" type="text/css" media="screen" />
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="<c:url value="/resources/htmlresource/css/digi.alert.css"/>" rel="stylesheet" type="text/css">
		
		
		<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery-1.7.2.js"/>"></script>
		<script src="<c:url value="/resources/htmlresource/css/adminpanel/js/hideshow.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/resources/htmlresource/css/adminpanel/js/jquery.tablesorter.min.js"/>" type="text/javascript"></script>
		<script type="text/javascript" src="<c:url value="/resources/htmlresource/css/adminpanel/js/jquery.equalHeight.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery.alert.js"/>"></script>
		<script type="text/javascript">
		$(document).ready(function() 
	    	{ 
	      	  $(".tablesorter").tablesorter(); 
	   	 } 
		);
		$(document).ready(function() {
	
		//When page loads...
		$(".tab_content").hide(); //Hide all content
		$("ul.tabs li:first").addClass("active").show(); //Activate first tab
		$(".tab_content:first").show(); //Show first tab content
	
		//On Click Event
		$("ul.tabs li").click(function() {
	
			$("ul.tabs li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$(".tab_content").hide(); //Hide all tab content
	
			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			return false;
		});
	
	});
		
		function deleteImage(id){
			jConfirm('Image will be permanently deleted. Would you like to proceed?', 'Confirmation Dialog', 
			  function(r) {
				if(r==true)
					{
				$.ajax({ 
					    type: 'POST',
			    		url: "<%=request.getContextPath()%>/admin/delete",
			    		data:{imageId:id},
			    		dataType: "json",
			    		async:true,cache:false,
			    		success: function (result) {
			   	 			if(result!=""){
			   	 				if(result.status){
			   						$("tr#tr"+id).fadeOut(500);
			   	 				}else{
			   	 				 	var $delAlert=jQuery.noConflict();
			                  		jConfirm(result.status, 'Confirmation Dialog', 
		       					    function(r) {
		       							if(r==true)
		       								{
		       					   			}
		       						
		       					});
			   	 				}
			            	}
			      		},
			    		error: function() {
					  
				  		}
				  });
					}
			});
		}
		
		function deleteAttribute(attrId,imageId){
			jConfirm('Attribute will be permanently deleted. Would you like to proceed?', 'Confirmation Dialog', 
			  function(r) {
				if(r==true)
					{
				$.ajax({ 
					    type: 'POST',
			    		url: "<%=request.getContextPath()%>/admin/deleteAttr",
			    		data:{imageId:imageId,attrId:attrId},
			    		dataType: "json",
			    		async:true,cache:false,
			    		success: function (result) {
			   	 			if(result!=""){
			   	 				if(result.status){
			   	 				
			   	 				editImage(imageId);
			   	 				}else{
			   	 				 	var $delAlert=jQuery.noConflict();
			                  		jConfirm(result.status, 'Confirmation Dialog', 
		       					    function(r) {
		       							if(r==true)
		       								{
		       					   			}
		       						
		       					});
			   	 				}
			            	}
			      		},
			    		error: function() {
					  
				  		}
				  });
					}
			});
		}
	
		
		function editImage(id){
			$.ajax({ 
			    type: 'POST',
	    		url: "<%=request.getContextPath()%>/admin/edit",
	    		data:{imageId:id},
	    		dataType: "json",
	    		async:true,cache:false,
	    		success: function (result) {
	   	 			if(result!=""){
	   	 			      $("#imageBody").html("<tr id='"+id+"'>"+ 
	    					 "<td></td>"+ 
	    	    				"<td><img src='<c:url value='"+result.readFilePath+"'/>' height='50' width='50'></td>"+ 
	    	    				"<td><input type='text' value='"+result.imageName+"'  id='imageName' /></td>"+ 
	    	    				"<td><select style='width: 100%;' name='imageCatagory' id='imagecat'></select></td>"+ 
	    	    				"<td>"+result.sysCreationDate+"</td>"+ 
	    	    				"<td><div class='submit_link'>"+
	    						"<input type='submit' value='Update' class='alt_btn' onclick=\"updateImage('"+id+"')\">  <a href='<c:url value='/admin/home'/>'>Cancel</a>"+
	    						"</div></td>"+ 
	    					"</tr> ");
	   	 			      for(var i=0;i<result.attributeList.length;i++){
	   	 			       $("#imageBody").append("<tr id='attrtd"+result.attributeList[i].id+"'>"+ 
	     					 "<td></td>"+ 
	     	    				"<td></td>"+ 
	     	    				"<td>Attribute "+eval(i+1)+"</td>"+ 
	     	    				"<td>"+result.attributeList[i].attrValue+"</td>"+ 
	     	    				"<td><input type='image' src='<c:url value='/resources/htmlresource/css/adminpanel/images/icn_trash.png'/>' title=\"Delete Attribute\" onclick=\"deleteAttribute('"+result.attributeList[i].id+"','"+id+"')\" "+ 
	     	    				"<td><a href=\"javascript:addAttributeInbox('"+id+"','"+eval(result.attributeList.length)+"')\">Add more Tags</a></td>"+ 
	     					"</tr> ");
	   	 			      }
	   	 			      
	   	 			  getCategory(result.imageCatagory);
	   	 			      
	            	}
	      		},
	    		error: function() {
			  
		  		}
		  });
			
		}
		
		function addAttributeInbox(imageId,i){
			
			 $("#imageBody").append("<tr id='attributeBox"+imageId+"'>"+ 
						 "<td></td>"+ 
		    				
		    				
		    				"<td><input type='box' placeholder=\"Enter multiple tags by comma separated\"  id='attributeBox'> </td>"+ 
		    				"<td><input type='submit'  value='Add' title=\"Add Attribute\" onclick=\"addAttribute('"+imageId+"','"+i+"')\"</td>"+ 
						"</tr> ");
			 
			 var $t=$('.width_3_quarter');
				$t.animate({"scrollTop":$('.width_3_quarter')[0].scrollHeight
					
				},'slow');
		}
		
		function addAttribute(imageId,j){
			var attribute=$("#attributeBox").val();
			if(attribute!=''){
			$.ajax({ 
			    type: 'POST',
	    		url: "<%=request.getContextPath()%>/admin/addAttribute",
	    		data:{attribute:attribute,imageId:imageId},
	    		dataType: "json",
	    		async:true,cache:false,
	    		success: function (result) {
	   	 			if(result!=""){
	   	 			for(var i=0;i<result.objList.length;i++){
	   	 			$("#attributeBox"+imageId).remove();
	   	 			   $("#imageBody").append("<tr id='attrtd"+result.objList[i].id+"'>"+ 
	       					 "<td></td>"+ 
	       	    				"<td></td>"+ 
	       	    				"<td>Attribute "+eval(i+parseInt(j)+1)+"</td>"+ 
	       	    				"<td>"+result.objList[i].attrValue+"</td>"+ 
	       	    				"<td><input type='image' src='<c:url value='/resources/htmlresource/css/adminpanel/images/icn_trash.png'/>' title=\"Delete Attribute\" onclick=\"deleteAttribute('"+result.objList[i].id+"','"+imageId+"')\" "+ 
	       	    				"<td><a href=\"javascript:addAttributeInbox('"+imageId+"')\">Add more Tags</a></td>"+ 
	       					"</tr> ");
	   	 			}
	            	}
	      		},
	    		error: function() {
			  
		  		}
		  });
			}
		}
		
		function updateImage(imageId){
			var imageCategory=$("#imagecat").val();
			var imageName=$("#imageName").val();
			
			$.ajax({ 
			    type: 'POST',
	    		url: "<%=request.getContextPath()%>/admin/updateImage",
	    		data:{imageCategory:imageCategory,imageId:imageId,imageName:imageName},
	    		dataType: "json",
	    		async:true,cache:false,
	    		success: function (result) {
	   	 			if(result!=""){
	   	 			   window.location.href='<c:url value='/admin/home'/>';
	            	}
	      		},
	    		error: function() {
			  
		  		}
		  });
			
		}
		
		function getCategory(imageCatagory){
			$.ajax({ 
			    type: 'GET',
	    		url: "<%=request.getContextPath()%>/admin/getImageCategory",
	    		
	    		dataType: "json",
	    		async:true,cache:false,
	    		success: function (result) {
	   	 			if(result!=""){
	   	 			    
	   	 			       for(var s=0;s<result.length;s++){
	   	 			    	   $("#imagecat").append("<option value='"+result[s].id+"'>"+result[s].categoryName+"</option>");
	   	 			       } 
	   	 			       
	   	 			   $("#imagecat").val(imageCatagory.id);   
	   	 			      
	            	}
	      		},
	    		error: function() {
			  
		  		}
		  });
			
		}
		
	    </script>
	    <script type="text/javascript">
	    $(function(){
	        $('.column').equalHeight();
	    });
	</script>
	
	</head>
	
	
	<body>
	
		<header id="header">
			<hgroup>
				<h1 class="site_title"><a href="<c:url value="/admin/home"/>">Admin Panel</a></h1>
				<h2 class="section_title">TagIt.com</h2><div class="btn_view_site"><a href="<c:url value="/logout/user"/>">Log Out</a></div>
			</hgroup>
		</header> <!-- end of header bar -->
		
		<section id="secondary_bar">
			<div class="user">
				<p>Hi! ${sessionScope.user.firstName}</p>
				<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
			</div>
			<div class="breadcrumbs_container">
				<article class="breadcrumbs"><a href="#">Admin Panel</a> <div class="breadcrumb_divider"></div> <a class="current">Dashboard</a></article>
			</div>
		</section><!-- end of secondary bar -->
		
		<aside id="sidebar" class="column">
			<form class="quick_search">
				<input type="text" value="Quick Search" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
			</form>
			<hr/>
			<h3>Images</h3>
			<ul class="toggle">
			 <li class="icn_new_article"><a href="<c:url value="/admin/musicHome"/>">Upload Tunes</a></li>
				<li class="icn_new_article"><a href="#">Upload Images</a></li>
				<li class="icn_edit_article"><a href="<c:url value="/admin/manageImageAttribute"/>">View Images</a></li>
				<li class="icn_edit_article"><a href="<c:url value="/admin/manageTuneAttribute"/>">View Tunes</a></li>
				<li class="icn_categories"><a href="<c:url value="/imageCategory/view"/>">Image Category</a></li>
				<li class="icn_categories"><a href="<c:url value="/attributeCategory/view"/>">Attribute Category</a></li>
				<li class="icn_tags"><a href="#">Tags</a></li>
			</ul>
			<h3>Users</h3>
			<ul class="toggle">
				<li class="icn_view_users"><a href="<c:url value="/manageUser/getUserData"/>">View Users</a></li>
				<li class="icn_profile"><a href="<c:url value="/admin/profile"/>">Your Profile</a></li>
				<%-- <li class="icn_profile"><a href="<c:url value="/admin/dataCorrect"/>">Don't use it</a></li> --%>
			</ul>
			<!-- <h3>Media</h3>
			<ul class="toggle">
				<li class="icn_folder"><a href="#">File Manager</a></li>
				<li class="icn_photo"><a href="#">Gallery</a></li>
				<li class="icn_audio"><a href="#">Audio</a></li>
				<li class="icn_video"><a href="#">Video</a></li>
			</ul> -->
			<h3>Admin</h3>
			<ul class="toggle">
				<li class="icn_settings"><a href="#">Game Settings</a></li>
				<li class="icn_security"><a href="#">Security</a></li>
						<li class="icn_security"><a href="<c:url value="/gameSummary/reporting"/>">Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/attributeReporting"/>">Attribute Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/tuneReporting"/>">Tune Reporting</a></li>
				<li class="icn_security"><a href="<c:url value="/gameSummary/tuneAttributeReporting"/>">Tune Attribute Reporting</a></li>
				<li class="icn_jump_back"><a href="<c:url value="/logout/user"/>">Logout</a></li>
			</ul>
			
			<footer>
				<hr />
				<p><strong>Copyright &copy; 2015 Tagit Admin</strong></p>
				
			</footer>
		</aside><!-- end of sidebar -->
		
		<section id="main" class="column">
			
			<h4 class="alert_info">Welcome to the TagIt Admin panel</h4>
			
			
			
			
			<article class="module width_full">
			<form action="<%=request.getContextPath()%>/admin/upload" method="post" enctype="multipart/form-data">
				<header><h3>Upload New Image</h3></header>
					<div class="module_content">
							<fieldset>
								<label>Upload</label>
								<input type="file"  placeholder="Upload here"  name="imageFile"  tabindex="10">
							</fieldset>
							
							<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Image Category</label> <select style="width: 92%;" name="imageCatagoryId">
								<c:if test="${not empty imagecatlist}">
								<c:forEach items="${imagecatlist}" var="listValue"  varStatus="loop">
								<option value="${listValue.id}">${listValue.categoryName}</option>
					
	                            </c:forEach>
			   
	                          </c:if>
							</select>
						</fieldset>
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Predefined Tags</label>
								<input type="text" style="width:92%;" name="tabooAttributes" placeholder="Enter multiple tags by comma separated">
							</fieldset><div class="clear"></div>
					</div>
				<footer>
					<div class="submit_link">
						
						<input type="submit" value="Upload" class="alt_btn">
						<input type="reset" value="Reset">
					</div>
				</footer>
				</form>
			</article><!-- end of post new article -->
			
			<article class="module width_3_quarter">
			<header><h3 class="tabs_involved">Uploaded Images</h3>
			<!-- <ul class="tabs">
	   			<li><a href="#tab1">Posts</a></li>
	    		<li><a href="#tab2">Comments</a></li>
			</ul> -->
			</header>
	
			<div class="tab_container">
				<div id="tab1" class="tab_content">
				<table class="tablesorter" cellspacing="0"> 
				<thead> 
					<tr> 
	   					<th></th>
	   					<th>Image</th> 
	    				<th>Image Name</th> 
	    				<th>Category</th> 
	    				<th>Created On</th> 
	    				<th>Actions</th> 
					</tr> 
				</thead> 
				<tbody id="imageBody"> 
				<c:if test="${not empty imagelist}">
				 
			   <c:forEach items="${imagelist}" var="listValue"  varStatus="loop">
			    
					<tr id="tr${listValue.id}"> 
	   					 <td>${loop.index+1}.</td> 
	    				<td><img src="<c:url value="${listValue.readFilePath}"/>" height="50" width="50" alt="${listValue.imageName}"></td> 
	    				<td>${listValue.imageName}</td> 
	    				<td>${listValue.imageCatagory.categoryName}</td> 
	    				<td>${listValue.sysCreationDate}</td> 
	    				<td><input type="image" src="<c:url value="/resources/htmlresource/css/adminpanel/images/icn_edit.png"/>" title="Edit" onclick="editImage('${listValue.id}')"><input type="image" src="<c:url value="/resources/htmlresource/css/adminpanel/images/icn_trash.png"/>" title="Trash" onclick="deleteImage('${listValue.id}')"></td> 
					</tr> 
				</c:forEach>
			   
	           </c:if>
					
					
				</tbody> 
				</table>
				</div><!-- end of #tab1 -->
				
							
			</div><!-- end of .tab_container -->
			
			</article><!-- end of content manager article -->
			
			<article class="module width_quarter">
				<header><h3>Feedbacks</h3></header>
				<div class="message_list">
					<div class="module_content">
						<div class="message"><p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor.</p>
						<p><strong>John Doe</strong></p></div>
						<div class="message"><p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor.</p>
						<p><strong>John Doe</strong></p></div>
						<div class="message"><p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor.</p>
						<p><strong>John Doe</strong></p></div>
						<div class="message"><p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor.</p>
						<p><strong>John Doe</strong></p></div>
						<div class="message"><p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor.</p>
						<p><strong>John Doe</strong></p></div>
					</div>
				</div>
				<footer>
					<form class="post_message">
						<input type="text" value="Message" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
						<input type="submit" class="btn_post_message" value=""/>
					</form>
				</footer>
			</article><!-- end of messages article -->
			
			<div class="clear"></div>
			
			
			
			
			
		<!-- 	<article class="module width_full">
				<header><h3>Basic Styles</h3></header>
					<div class="module_content">
						<h1>Header 1</h1>
						<h2>Header 2</h2>
						<h3>Header 3</h3>
						<h4>Header 4</h4>
						<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Maecenas faucibus mollis interdum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
	
	<p>Donec id elit non mi porta <a href="#">link text</a> gravida at eget metus. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
	
						<ul>
							<li>Donec ullamcorper nulla non metus auctor fringilla. </li>
							<li>Cras mattis consectetur purus sit amet fermentum.</li>
							<li>Donec ullamcorper nulla non metus auctor fringilla. </li>
							<li>Cras mattis consectetur purus sit amet fermentum.</li>
						</ul>
					</div>
			</article>end of styles article -->
			<div class="spacer"></div>
		</section>
	
	
	</body>
	
	</html>