<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
	
<head>
	<title>TagIt.com</title>
		<meta charset="utf-8">
		<link href="<c:url value="/resources/htmlresource/css/login-signup/css/style.css" />" rel='stylesheet' type='text/css' />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript" src="<c:url value="/resources/htmlresource/js/jquery-1.11.1.min.js"/>"></script>
		<!--webfonts-->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600,700' rel='stylesheet' type='text/css'>
		<!--//webfonts-->
		
		<script type="text/javascript">
				function toggle(id){
					if(id=='signin'){
						$("#form_container").hide();
						$("#signin").show();
						
					}else{
						$("#form_container").show();
						$("#signin").hide();
					}
					
				}
			</script>
		
		
</head>
<body>
	
				 <!-----start-main---->
				<div class="login-form">
						<h1><a href="javascript:toggle('signin');"> Sign In </a></h1>
						<h2><a href="javascript:toggle('form_container');">Create Account</a></h2>
				<form id="signin"  method="post" action="<%=request.getContextPath()%>/authenticate/login">
					<li>
						<input type="text" class="text" value="User Name" name="loginId" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'User Name';}" ><a href="#" class=" icon user"></a>
					</li>
					<li>
						<input type="password" value="Password" name="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}"><a href="#" class=" icon lock"></a>
					</li>
					
					 <div class ="forgot">
					  
						<input type="submit" onclick="myFunction()" value="Sign In" > <a href="#" class=" icon arrow"></a> 
					                                                                                                                                                                                                                              </h4>
					</div>
					
					  
							<%-- <p style="color:#fff;">${successObj}</p> --%>                                                                                                                                                                                                                                </h4>
				
					
				</form>
				
			
				
		<form id="form_container" style="display: none;" method="post"  action="<%=request.getContextPath()%>/user/add">
					<li>
						<input type="text" class="text" name="firstName" value="First Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'First Name';}" ><a href="#" class=" icon user"></a>
					</li>
					<li>
						<input type="text" class="text" name="lastName" value="Last Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Last Name';}" ><a href="#" class=" icon user"></a>
					</li>
					<li>
						<input type="text" class="text" name="email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" ><a href="#" class=" icon user"></a>
					</li>
					<li>
						<input type="text" class="text" name="loginId" value="Login id" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Login id';}" ><a href="#" class=" icon user"></a>
					</li>
					<li>
						<input type="password" name="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}"><a href="#" class=" icon lock"></a>
					</li>
					<li>
						<input type="password" name="confpassword" value="Confirm Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Confirm Password';}"><a href="#" class=" icon lock"></a>
					</li>
					
					
					
					 <div class ="forgot">
						
						<input type="submit" onclick="myFunction()" value="Sign Up" > <a href="#" class=" icon arrow"></a>                                                                                                                                                                                                                                 </h4>
					</div>
					
				</form>
		
			</div>
			<!--//End-login-form-->
					<div class="ad728x90" style="text-align:center">
				
		   </div>


		  <!-----start-copyright---->
   					<div class="copy-right">
						<p>@Copyright 2014</p> 
					</div>
				<!-----//end-copyright---->
		 		
</body>
</html>