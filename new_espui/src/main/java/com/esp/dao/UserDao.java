package com.esp.dao;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.esp.model.auth.Authentication;
import com.esp.model.user.User;

public interface UserDao {
	
	  void saveUser(User user, String loginId, String password);
	     
	  List<User> findAllUsers();
	     
	  User deleteUserById(String id);
	  
	  public void uploadImage(String filePath,String readFilePath,String id,HttpServletRequest request);
	  
	  User updateUser(String fname,String lname,String city,String email,String id,HttpServletRequest request);
	  
	  User getUserById(String id);
	  
	  Authentication getUserByLoginId(String loginId);
	  
	  void setOnlineStatus(String userId);
	  void setOfflineStatus(String userId);
	  
	  boolean isOnline(String userId);
	  boolean isBusy(String userId);
	  
	  List<User> getFreePlayers();
	  void setBusy(String userId);

}
