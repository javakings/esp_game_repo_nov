/**
 * 
 */
package com.esp.dao;

import java.util.List;

import com.esp.model.game.Game;
import com.esp.model.game.GameAttributeSummary;
import com.esp.model.game.PlayerAttribute;
import com.esp.model.type.FILETYPE;
import com.esp.model.user.User;

/**
 * @author Amit
 *
 */
public interface GameDao {

	Game saveNewMultiplayerGame(User player1, User player2);
	
	PlayerAttribute savePlayerAttribute(PlayerAttribute playeAttribute);
	
	Game saveNewSinglePlayerGame(Integer score,String userId,List<GameAttributeSummary>gameAttributeSummaryList,FILETYPE fileType);
	
	Game saveNewNonAssistedGame(Integer score,String userId,List<GameAttributeSummary>gameAttributeSummaryList,FILETYPE fileType);
	
	void saveGameAttribute(String gameId, String imageId, GameAttributeSummary attrSummary);
	
	GameAttributeSummary getGameAttributeSummary(String gameId, String imageId);
	List<GameAttributeSummary> getGameAttributeSummaryList(String gameId, String imageId);
	
	List<Game> findAllGames() ;
	
	Game getGame(String gameId);
}
