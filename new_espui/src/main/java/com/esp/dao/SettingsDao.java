/**
 * 
 */
package com.esp.dao;

import java.util.Date;
import java.util.List;

import com.esp.model.gamesettings.Settings;

/**
 * @author jay
 *
 */
public interface SettingsDao {
   public List<Settings>findSettings();
   public void addSettings(Date defaultPlayTime,Integer scorePerAttributeMatched);
}
