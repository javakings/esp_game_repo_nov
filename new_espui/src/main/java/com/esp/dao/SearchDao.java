/**
 * 
 */
package com.esp.dao;

import java.util.List;

import com.esp.model.image.Image;
import com.esp.model.type.FILETYPE;

/**
 * @author Amit
 *
 */

public interface SearchDao {

	public List<Image> searchImage(String query,FILETYPE fileType);
}
