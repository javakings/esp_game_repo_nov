/**
 * 
 */
package com.esp.dao;

import java.util.List;

import com.esp.model.image.AttributeCategory;

/**
 * @author Jay
 *
 */
public interface AttributeCategoryDao {
	  public List<AttributeCategory>findAllCategory();
	  public AttributeCategory findCategoryById(String id);
	  public void addCategory(String name);
	  public void updateCategory(String id,String name);
	  public void deleteCategory(String id,String name);
}
