/**
 * 
 */
package com.esp.dao;

import java.util.List;

import com.esp.model.image.ImageCategory;

/**
 * @author Jay
 *
 */
public interface ImageCategoryDao {
  public List<ImageCategory>findAllCategory();
  public ImageCategory findCategoryById(String id);
  public void addCategory(String name);
  public void updateCategory(String id,String name);
  public void deleteCategory(String id,String name);
}
