/**
 * 
 */
package com.esp.dao;

import java.util.List;

import com.esp.model.image.Attribute;
import com.esp.model.image.AttributeCategory;
import com.esp.model.image.Image;
import com.esp.model.type.ATTRIBUTETYPE;
import com.esp.model.type.FILETYPE;
import com.esp.model.type.IMAGECATAGORY;
import com.esp.model.user.User;

/**
 * @author Amit
 *
 */
public interface ImageDao {
	
	public List<Image> findAllImages(FILETYPE fileType);
	
	public List<Image> getImageForGame(FILETYPE fileType,User user);

	public void uploadImage(String imageName, String catagoryId, String filePath,String readFilePath,List<Attribute>attributeList,FILETYPE fileType);
	
	public void deleteImage(String id);
	
	public Image findImagebyId(String id);
	
	public void updateImage(String id,String catagoryId,String imageName,FILETYPE fileType);
	
	public void updateImageSeenBy(String id,User seenBy);
	
	public void deleteAttribute(String attrId,String imageId);
	
	public List<Attribute> addAttribute(String attribute,String imageId,AttributeCategory cat,User answeredBy);
	
	public void updateImageAttribute(String imageId, String attrId,AttributeCategory attrCat,ATTRIBUTETYPE type,String attributeName);
	
	public void updateImageAttributeAnsweredBy(String attrId,User answeredBy,String imageId);
	
	public void updateImageAttributeIgnoredBy(String attrId,User answeredBy,String imageId);
	
	public void updateImageAttributeWrongAnsweredBy(String attrId,User answeredBy,String imageId);
}
