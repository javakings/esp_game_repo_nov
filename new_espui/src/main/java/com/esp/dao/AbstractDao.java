package com.esp.dao;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.esp.model.type.ClassType;

public abstract class AbstractDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	//private Transaction transaction;
	

	protected Session getSession()
	{
		return sessionFactory.getCurrentSession();
	}
	
	/*protected void closeSession()
	{
		if(transaction != null)
		{
			if(transaction.wasRolledBack())
			{
				transaction.rollback();
			}
			else
			{
				transaction.commit();
			}
		}
		if(session != null)
		{
			session.close();
		}
	}*/

	public void persist(Object entity) 
	{
		getSession().persist(entity);
	}

	public Object update(Object entity) 
	{
		return getSession().merge(entity);
	}

	
	public void delete(Object entity) {
		getSession().delete(entity);
	}
	
	protected <T> List<T> executeNamedQuery(String namedQuery, Map<String, Map<ClassType, Object>> paramMap, Class<T> T)
	{
		List<T> list = null;
		try
		{
		Query query = getSession().getNamedQuery(namedQuery);
		setParameter(query, paramMap);
		list = query.list();
		if(list == null)
		{
			list = new ArrayList<T>();
		}
		}
		finally
		{
			//closeSession();
		}
		return list;
	}
	
	private void setParameter(Query query, Map<String, Map<ClassType, Object>> paramMap)
	{
		Iterator<Map.Entry<String, Map<ClassType, Object>>> iterator = paramMap.entrySet().iterator();
		while(iterator.hasNext())
		{
			Map.Entry<String, Map<ClassType, Object>> entry = iterator.next();
			String key = entry.getKey();
			Map<ClassType, Object> value = entry.getValue();
			Iterator<Map.Entry<ClassType, Object>> paramIterator = value.entrySet().iterator();
			while(paramIterator.hasNext())
			{
				Map.Entry<ClassType, Object> paramEntry = paramIterator.next();
				ClassType paramKey = paramEntry.getKey();
				Object paramValue = paramEntry.getValue();
				switch (paramKey) {
				case Integer:
					query.setInteger(key, (Integer)paramValue);
					break;
				case Double:
					query.setDouble(key, (Double)paramValue);
					break;
				case Float:
					query.setFloat(key, (Float)paramValue);
					break;
				case String:
					query.setString(key, (String)paramValue);
					break;
				default:
					break;
				}
			}
		}
		
	}
}
