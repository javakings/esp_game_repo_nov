package com.esp.dao;

import com.esp.model.user.User;

public interface DbDataCorrectionDao {
	public void correctData(String persistedAttrid,String attrId,String imageId,User answeredBy);
}
