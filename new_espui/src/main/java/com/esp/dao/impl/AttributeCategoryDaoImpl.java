/**
 * 
 */
package com.esp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.esp.dao.AbstractDao;
import com.esp.dao.AttributeCategoryDao;
import com.esp.model.image.AttributeCategory;

/**
 * @author Jay
 *
 */
@Repository("attributeCategoryDaoImpl")
public class AttributeCategoryDaoImpl extends AbstractDao implements AttributeCategoryDao{

	@Override
	public List<AttributeCategory> findAllCategory() {
		Query query = getSession().createQuery("from AttributeCategory");
		List<AttributeCategory> list = query.list();
		return list;
	}

	@Override
	public AttributeCategory findCategoryById(String id) {
		AttributeCategory attrCategory = (AttributeCategory) getSession().get(AttributeCategory.class,id);

		return attrCategory;
	}

	@Override
	public void addCategory(String name) {
		AttributeCategory attrCategory=new AttributeCategory();
		attrCategory.setCategoryName(name);
		persist(attrCategory);
		
	}

	@Override
	public void updateCategory(String id, String name) {
		AttributeCategory attrCategory = (AttributeCategory) getSession().get(AttributeCategory.class,id);
		if(attrCategory!=null){
			attrCategory.setCategoryName(name);
			getSession().update(attrCategory);
		}
		
	}

	@Override
	public void deleteCategory(String id, String name) {
		// TODO Auto-generated method stub
		
	}

}
