package com.esp.dao.impl;

import org.springframework.stereotype.Repository;

import com.esp.dao.AbstractDao;
import com.esp.dao.DbDataCorrectionDao;
import com.esp.model.image.Attribute;
import com.esp.model.image.Image;
import com.esp.model.user.User;

@Repository("dbDataCorrectionDaoImpl")
public class DbDataCorrectionDaoImpl extends AbstractDao implements DbDataCorrectionDao{

	@Override
	public void correctData(String persistedAttrid,String attrId,String imageId,User answeredBy) {
	     Attribute attr=(Attribute)getSession().get(Attribute.class,attrId);
	    
			Image image=findImagebyId(imageId);
			if(image!=null){
				image.getAttributeList().remove(attr);
				getSession().update(image);

			}
			if(attr!=null){
				getSession().delete(attr);
			}
			
			updateImageAttributeAnsweredBy(persistedAttrid,answeredBy,imageId,attr.getAnsweredBy().size());

	}
	
	private Image findImagebyId(String id) {

		Image image = (Image) getSession().get(Image.class,id);

		return image;
	}
	
	private void updateImageAttributeAnsweredBy(String attrId,User answeredBy,String imageId,int count) {
		Attribute attr = (Attribute) getSession().get(Attribute.class,attrId);
		if(attr!=null){
		 	if(answeredBy!=null){
		 		for(int i=0;i<count;i++){
				 attr.getAnsweredBy().add(answeredBy.getId());
		 		}
			}
			getSession().update(attr);
		
	}
		
	}

}
