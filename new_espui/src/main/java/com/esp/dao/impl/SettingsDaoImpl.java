/**
 * 
 */
package com.esp.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.esp.dao.AbstractDao;
import com.esp.dao.SettingsDao;
import com.esp.model.gamesettings.Settings;

/**
 * @author Jay
 *
 */
@Repository("settingsDaoImpl")
public class SettingsDaoImpl extends AbstractDao implements SettingsDao {

	public List<Settings> findSettings() {
		Query query = getSession().createQuery("from Settings"); 
		List<Settings> list = query.list();
		return list;
	}

	public void addSettings(Date defaultPlayTime,Integer scorePerAttributeMatched) {
		Settings settings=new Settings();
		settings.setDefaultPlayTime(defaultPlayTime);
		settings.setScorePerAttributeMatched(scorePerAttributeMatched);
		persist(settings);
	}

}
