/**
 * 
 */
package com.esp.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.esp.dao.AbstractDao;
import com.esp.dao.SearchDao;
import com.esp.model.image.Attribute;
import com.esp.model.image.Image;
import com.esp.model.type.FILETYPE;

/**
 * @author Amit
 *
 */
@Repository
public class SearchDaoImpl extends AbstractDao implements SearchDao{

	@Override
	public List<Image> searchImage(String query,FILETYPE fileType) {
		
		Criteria criteria = getSession().createCriteria(Attribute.class);
		List<String> searchQuery = parseString(query);
		//criteria.add(Restrictions.in("attrName", searchQuery));
		List<Criterion> crList = new ArrayList<Criterion>(); 
		for(String str : searchQuery){
			crList.add(Restrictions.ilike("attrName", "%"+str+"%"));
			
			
		}
		criteria.add(Restrictions.or(crList.toArray(new Criterion[crList.size()])));
		List<Attribute> attrList = criteria.list();
		List<Image> imageList = new ArrayList<Image>();
		Set<String> processedImage = new HashSet<String>();
		for(Attribute attr : attrList){
			if(!processedImage.contains(attr.getImage().getId())){
				if(attr.getImage().getFileType().equals(fileType)){
				imageList.add(attr.getImage());
				}
			}
			processedImage.add(attr.getImage().getId());
		}
		return imageList;
		
	}
	
	private List<String> parseString(String query){
		return Arrays.asList(query.replaceAll("^[,\\s]+", "").split("[,\\s]+"));
	}
	
	public static void main(String[] args) {
		
		
	}

}
