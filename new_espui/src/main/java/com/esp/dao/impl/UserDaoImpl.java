package com.esp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.esp.bean.UserBean;
import com.esp.constant.EspConstant;
import com.esp.dao.AbstractDao;
import com.esp.dao.UserDao;
import com.esp.model.auth.Authentication;
import com.esp.model.image.Image;
import com.esp.model.type.ClassType;
import com.esp.model.user.User;
import com.esp.utility.AuthenticationUtility;


@Repository("userDaoImpl")
public class UserDaoImpl extends AbstractDao implements UserDao {

	public void saveUser(User user, String loginId, String password) {
		Authentication auth = new Authentication();
		auth.setLoginID(loginId);
		auth.setPassword(new AuthenticationUtility().encrypt(password));
		auth.setUser(user);
		persist(auth);
	}

	@SuppressWarnings("unchecked")
	public List<User> findAllUsers() {
		Query query = getSession().createQuery("from User"); 
		List<User> list = query.list();
		return list;
	}

	public User deleteUserById(String id) {
		User user = (User) getSession().load(User.class,id);
		if(user!=null){
			getSession().delete(user);
		}
		return user;
	}

	public User getUserById(String id) {
		User user = (User) getSession().get(User.class,id);
		return user;
	}

	public Authentication getUserByLoginId(String loginId) {
		Map<ClassType, Object> childMap = new HashMap<ClassType, Object>();
		childMap.put(ClassType.String, loginId);
		Map<String, Map<ClassType, Object>> paramMap = new HashMap<String, Map<ClassType,Object>>();
		paramMap.put("loginID", childMap);
		List<Authentication> authList = executeNamedQuery("Authentication.findByLoginId", paramMap, Authentication.class);
		if(authList.size() > 0)
		{
			return authList.get(0);
		}
		return null;
	}

	public User updateUser(String fname, String lname, String city,
			String email ,String id,HttpServletRequest request) {
		User user = (User) getSession().get(User.class,id);
		user.setCity(city);
		user.setEmail(email);
		user.setFirstName(fname);
		user.setLastName(lname);
		getSession().update(user);
		updateSessionObject(user, request);
		return user;
	}

	public void uploadImage(String filePath, String readFilePath,String id,HttpServletRequest request) {

		User user = (User) getSession().get(User.class,id);
		user.setUserImagePath(filePath);
		user.setUserReadFilePath(readFilePath);
		getSession().update(user);
		updateSessionObject(user, request);
	}


	private void updateSessionObject(User user,HttpServletRequest request){
		HttpSession session=request.getSession(false);
		if(session!=null){
			UserBean bean=(UserBean)session.getAttribute(EspConstant.USER);
			if(bean!=null){
				bean.setCity(user.getCity());
				bean.setFirstName(user.getFirstName());
				bean.setEmail(user.getEmail());
				bean.setLastName(user.getLastName());
				bean.setUserId(user.getId());
				bean.setUserType(user.getType());
				bean.setReadImagePath(request.getContextPath()+user.getUserReadFilePath());
				//session.removeAttribute(EspConstant.USER);
				session.setAttribute(EspConstant.USER, bean);
			}
		}
	}

	public void setOnlineStatus(String userId) {
		User user = (User) getSession().get(User.class, userId);
		user.setOnline(true);
		user.setBusy(false);
		persist(user);

	}

	public void setOfflineStatus(String userId) {
		User user = (User) getSession().get(User.class, userId);
		user.setOnline(false);
		user.setBusy(false);
		persist(user);

	}

	public boolean isOnline(String userId) {
		User user = (User) getSession().get(User.class, userId);
		return user.isOnline();
	}

	public boolean isBusy(String userId) {
		User user = (User) getSession().get(User.class, userId);
		return user.isBusy();
	}

	public List<User> getFreePlayers() {
		Query query = getSession().createQuery("from User user where user.isBusy = 1 and user.isOnline= 1");
		List<User> list = query.list();
		return list;
	}

	public void setBusy(String userId) {
		User user = (User) getSession().get(User.class, userId);
		user.setBusy(true);
		persist(user);
		
	}




}
