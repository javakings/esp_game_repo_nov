/**
 * 
 */
package com.esp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.esp.dao.AbstractDao;
import com.esp.dao.GameDao;
import com.esp.model.game.Game;
import com.esp.model.game.GameAttributeSummary;
import com.esp.model.game.PlayerAttribute;
import com.esp.model.image.Image;
import com.esp.model.type.ClassType;
import com.esp.model.type.FILETYPE;
import com.esp.model.type.GAMEMODE;
import com.esp.model.user.User;


/**
 * @author Amit
 *
 */
@Repository("gameDaoImpl")
public class GameDaoImpl extends AbstractDao implements GameDao {

	public Game saveNewMultiplayerGame(User player1, User player2) {
		Game game = new Game();
		game.setPlayer1(player1);
		game.setPlayer2(player2);
		game.setGameMode(GAMEMODE.TWO_PLAYER);
		persist(game);
		return game;
		
	}

	public void saveGameAttribute(String gameId, String imageId, GameAttributeSummary attrSummary) {
		Game game = (Game) getSession().load(Game.class,gameId);
		game.getAttrSummaryList().add(attrSummary);
		persist(game);
	}
	
	
	public List<Game> findAllGames() {
		Query query = getSession().createQuery("from Game"); 
		List<Game> list = query.list();
		return list;
	}

	public GameAttributeSummary getGameAttributeSummary(String gameId, String imageId) {
		
		GameAttributeSummary attrSummary = null;
		Map<ClassType, Object> childMap = new HashMap<ClassType, Object>();
		childMap.put(ClassType.String, imageId);
		Map<String, Map<ClassType, Object>> paramMap = new HashMap<String, Map<ClassType,Object>>();
		paramMap.put("imageId", childMap);
		childMap = new HashMap<ClassType, Object>();
		childMap.put(ClassType.String, gameId);
		paramMap.put("gameId", childMap);

		List<GameAttributeSummary> attrList = executeNamedQuery("gameattrsummary.imageId", paramMap, GameAttributeSummary.class);
		if(attrList != null && attrList.size() > 0)
		{
			attrSummary = attrList.get(0);
		}
		else{
			attrSummary = new GameAttributeSummary();
			attrSummary.setGameId(gameId);
			attrSummary.setImageId(imageId);
		}
		return attrSummary;
	}
	
	

	@Override
	public Game getGame(String gameId) {
		return (Game) getSession().load(Game.class,gameId);
	}

	@Override
	public Game saveNewSinglePlayerGame(Integer score, String userId,List<GameAttributeSummary>gameAttributeSummaryList,FILETYPE fileType) {
		User user=(User) getSession().get(User.class,userId);
		Game game = new Game();
		game.setPlayer1(user);
		game.setScore(score);
		game.setPlayer2(user);
		if(fileType.equals(FILETYPE.IMAGE)){
		game.setGameMode(GAMEMODE.ASSISTED);
		}else{
			game.setGameMode(GAMEMODE.ASSISTED_MUSIC);	
		}
		persist(game);

		for(GameAttributeSummary attr : gameAttributeSummaryList){
			attr.setGameId(game.getId());
		}
		game.setAttrSummaryList(gameAttributeSummaryList);
		update(game);
		return game;
	}

	@Override
	public Game saveNewNonAssistedGame(Integer score, String userId,List<GameAttributeSummary>gameAttributeSummaryList,FILETYPE fileType) {
		User user=(User) getSession().get(User.class,userId);
		Game game = new Game();
		game.setScore(score);
		game.setPlayer1(user);
		game.setPlayer2(user);
		if(fileType.equals(FILETYPE.IMAGE)){
		game.setGameMode(GAMEMODE.NON_ASSISTED);
		}else{
			game.setGameMode(GAMEMODE.NON_ASSISTED_MUSIC);	
		}
		persist(game);
		for(GameAttributeSummary attr : gameAttributeSummaryList){
			attr.setGameId(game.getId());
		}
		game.setAttrSummaryList(gameAttributeSummaryList);
		update(game);
		return game;
	}

	@Override
	public PlayerAttribute savePlayerAttribute(PlayerAttribute playeAttribute) {
		persist(playeAttribute);
		return playeAttribute;
	}

	@Override
	public List<GameAttributeSummary> getGameAttributeSummaryList(String gameId, String imageId) {
	
		Map<ClassType, Object> childMap = new HashMap<ClassType, Object>();
		childMap.put(ClassType.String, imageId);
		Map<String, Map<ClassType, Object>> paramMap = new HashMap<String, Map<ClassType,Object>>();
		paramMap.put("imageId", childMap);
		childMap = new HashMap<ClassType, Object>();
		childMap.put(ClassType.String, gameId);
		paramMap.put("gameId", childMap);

		List<GameAttributeSummary> attrList = executeNamedQuery("gameattrsummary.imageId", paramMap, GameAttributeSummary.class);
	
		return attrList;
	}

	

}
