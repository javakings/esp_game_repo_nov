/**
 * 
 */
package com.esp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.esp.constant.EspConstant;
import com.esp.dao.AbstractDao;
import com.esp.dao.AttributeDao;
import com.esp.model.image.Attribute;
import com.esp.model.type.ATTRIBUTEFORTYPE;

/**
 * @author Jay
 *
 */
@Repository("attributeDaoImpl")
public class AttributeDaoImpl extends AbstractDao implements AttributeDao {

	public List<Attribute> findAllAttributes() {
		Query query = getSession().createQuery("from Attribute");
		List<Attribute> list = query.list();
		return list;
	}
	
	public List<Attribute> findAllAttributesByImageId(String imageId) {
		Query query = getSession().createQuery("from Attribute where image='"+imageId+"'");
		List<Attribute> list = query.list();
		return list;
	}

	public Attribute getAttributeById(String attrId) {
		Attribute attribute = (Attribute) getSession().get(Attribute.class,
				attrId);

		return attribute;
	}

	public List<Attribute> getAttributeBySql(String sql) {
		return null;
	}

	public List<Attribute> getAttributeByCriteria(List<Attribute> attrList,ATTRIBUTEFORTYPE attributeForType) {
		String[] arr = new String[attrList.size()];
		
		for (int i = 0; i < attrList.size(); i++) {
			Attribute attr=attrList.get(i);
			arr[i] = attr.getId();
		}
		Criteria criteria = getSession().createCriteria(Attribute.class);
		criteria.add(Restrictions.not(Restrictions.in("id", arr)));
		criteria.add(Restrictions.eq("attributeForType",attributeForType));
		criteria.setMaxResults(EspConstant.NO_OF_ANSWERS);
		return criteria.list();
	}

}
