/**
 * 
 */
package com.esp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.esp.dao.AbstractDao;
import com.esp.dao.ImageCategoryDao;
import com.esp.model.image.Attribute;
import com.esp.model.image.ImageCategory;

/**
 * @author Jay
 *
 */
@Repository("imageCategoryDaoImpl")
public class ImageCategoryDaoImpl extends AbstractDao implements ImageCategoryDao {

	@Override
	public List<ImageCategory> findAllCategory() {
		Query query = getSession().createQuery("from ImageCategory");
		List<ImageCategory> list = query.list();
		return list;
	}

	@Override
	public ImageCategory findCategoryById(String id) {
		ImageCategory imageCategory = (ImageCategory) getSession().get(ImageCategory.class,
				id);

		return imageCategory;
	}

	@Override
	public void addCategory(String name) {
		ImageCategory imageCategory=new ImageCategory();
		imageCategory.setCategoryName(name);
		persist(imageCategory);
		
	}

	@Override
	public void updateCategory(String id, String name) {
		ImageCategory imageCategory = (ImageCategory) getSession().get(ImageCategory.class,id);
		if(imageCategory!=null){
			imageCategory.setCategoryName(name);
			getSession().update(imageCategory);
		}
	}

	@Override
	public void deleteCategory(String id, String name) {
		// TODO Auto-generated method stub
		
	}

}
