package com.esp.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.esp.dao.AbstractDao;
import com.esp.dao.ImageDao;
import com.esp.model.image.Attribute;
import com.esp.model.image.AttributeCategory;
import com.esp.model.image.Image;
import com.esp.model.image.ImageCategory;
import com.esp.model.type.ATTRIBUTEFORTYPE;
import com.esp.model.type.ATTRIBUTETYPE;
import com.esp.model.type.FILETYPE;
import com.esp.model.user.User;
import com.esp.utility.CommonUtility;

/**
 * 
 * @author Amit
 *
 */
@Repository("imageDaoImpl")
public class ImageDaoImpl extends AbstractDao implements ImageDao{

	public void uploadImage(String imageName, String catagoryId, String filePath,String readFilePath,List<Attribute>attributeList,FILETYPE fileType) {
		
		Image image = new Image();
		image.setImageName(imageName);
		image.setImagePath(filePath);
		image.setReadFilePath(readFilePath);
		image.setFileType(fileType);
		if(fileType.equals(FILETYPE.IMAGE)){
			
			ImageCategory imageCat = (ImageCategory) getSession().load(ImageCategory.class,catagoryId);
			image.setImageCatagory(imageCat);
		}
		
		for(Attribute attr:attributeList){
			attr.setImage(image);
			attr.setType(ATTRIBUTETYPE.DEFAULT);
			if(fileType.equals(FILETYPE.IMAGE)){
				attr.setAttributeForType(ATTRIBUTEFORTYPE.IMAGE);
			}else{
				attr.setAttributeForType(ATTRIBUTEFORTYPE.MUSIC);
			}
		}
		image.setAttributeList(attributeList);
		persist(image);
	}

	public void deleteImage(String id) {

		Image image = (Image) getSession().load(Image.class,id);
		if(image!=null){
			getSession().delete(image);
		}


	}

	public List<Image> findAllImages(FILETYPE fileType) {
		Query query = getSession().createQuery("from Image where fileType='"+fileType+"'"); 
		List<Image> list = query.list();
		return list;
	}



	public Image findImagebyId(String id) {

		Image image = (Image) getSession().get(Image.class,id);

		return image;
	}

	public void deleteAttribute(String attrId,String imageId) {

		Attribute attr=(Attribute)getSession().get(Attribute.class,attrId);
		Image image=findImagebyId(imageId);
		if(image!=null){
			image.getAttributeList().remove(attr);
			getSession().update(image);

		}
		if(attr!=null){
			getSession().delete(attr);
		}



	}

	public List<Attribute> addAttribute(String attribute,String imageId,AttributeCategory cat,User answeredBy)
	{
		String [] attrArray=attribute.split(",");
		List<Attribute>attrList=new ArrayList<Attribute>();
		Image image=findImagebyId(imageId);
		for(String attrVal:attrArray){
			if(attrVal!=null && attrVal.trim().length()>0){
			attrVal=attrVal.trim();
			attrVal=attrVal.toLowerCase();
			List<Attribute>imageAttrList=image.getAttributeList();
			boolean isFound=false;
			Attribute foundAttr=null;
			if(imageAttrList!=null && imageAttrList.size()>0){
				for(Attribute attr:imageAttrList){
					if(attrVal.equalsIgnoreCase(attr.getAttrName().trim())){
						isFound=true;
						foundAttr=attr;
						break;
					}
				}
			}
			
			if(!isFound){
			Attribute attr=new Attribute();
			attr.setAttrName(attrVal);
			attr.setAttrValue(attrVal);
			if(image.getFileType().equals(FILETYPE.IMAGE)){
				attr.setAttributeForType(ATTRIBUTEFORTYPE.IMAGE);
			}else{
				attr.setAttributeForType(ATTRIBUTEFORTYPE.MUSIC);
			}
			if(cat!=null){
				attr.setAttrCategory(cat);
			}else{
				attr.setType(ATTRIBUTETYPE.DEFAULT);
			}
			if(answeredBy!=null){
				attr.getAnsweredBy().add(answeredBy.getId());
			}
			attr.setImage(image);
			persist(attr);
			attrList.add(attr);
			imageAttrList.add(attr);
			}else{
				if(foundAttr!=null){
					if(answeredBy!=null){
						foundAttr.getAnsweredBy().add(answeredBy.getId());
					}
				 getSession().update(foundAttr);
				}
				attrList.add(foundAttr);
			}
		}
		}
	
		getSession().update(image);
		return attrList;
	}

	public void updateImage(String id,String catagoryId,String imageName,FILETYPE fileType) {

		Image image = (Image) getSession().get(Image.class,id);
		if(fileType.equals(FILETYPE.IMAGE)){
		ImageCategory imageCat = (ImageCategory) getSession().load(ImageCategory.class,catagoryId);
		image.setImageCatagory(imageCat);
		}
		if(imageName!=null && imageName.length()>0){
		image.setImageName(imageName);
		}
		
		getSession().update(image);
	}

	@SuppressWarnings("unchecked")
	public List<Image> getImageForGame(FILETYPE fileType,User user) {
		List<Image> singlePlayerImages = new ArrayList<Image>();
		List<Image> imageList = new ArrayList<Image>();
		List<String>imageIdList=new ArrayList<String>();
		int count = ((Long) getSession().createQuery("select count(*) from Image where fileType='" + fileType + "'")
				.uniqueResult()).intValue();
		if (count > 10) {
			Query query = getSession().createQuery("from Image image where image.fileType='" + fileType + "'");
			List<Image> list = query.list();
			if (query.list() != null && query.list().size() > 0) {
				for (Image image : list) {
					if (!image.getSeenBy().contains(user.getId())) {
						imageList.add(image);
						imageIdList.add(image.getId());
					}
					if (imageList.size() == 10) {
						break;
					}
				}
			}

			if (imageList.size() == 10) {
				singlePlayerImages = imageList;
			} else {
				int maxResult = 10-imageList.size();
				List<String> seenBy = new ArrayList<String>();
				seenBy.add(user.getId());
				Criteria crit = getSession().createCriteria(Image.class);
				crit.setMaxResults(maxResult);
				crit.add(Restrictions.eq("fileType", fileType));
			
				if(imageList.size()>0){
					crit.add(Restrictions.not(Restrictions.in("id", imageIdList)));
				}
				singlePlayerImages = crit.list();
				singlePlayerImages.addAll(imageList);
			}
		} else {
			int firstResult = CommonUtility.getRandomNo(count);
			int maxResult = 10;

			if ((count - firstResult) < 10) {
				firstResult = count - maxResult;
				if (firstResult < 0) {
					firstResult = 0;
				}
			}
			List<String> seenBy = new ArrayList<String>();
			seenBy.add(user.getId());
			Criteria crit = getSession().createCriteria(Image.class);
			crit.setFirstResult(firstResult);
			crit.setMaxResults(maxResult);
			crit.add(Restrictions.eq("fileType", fileType));

			singlePlayerImages = crit.list();
		}
	
		return singlePlayerImages;
	}

	@Override
	public void updateImageAttribute(String imageId, String attrId,AttributeCategory attrCat,ATTRIBUTETYPE type,String attributeName) {
		Attribute attr = (Attribute) getSession().get(Attribute.class,attrId);
		Image image = (Image) getSession().get(Image.class,imageId);
		if(attr!=null && image!=null){
			image.getAttributeList().remove(attr);
			attr.setAttrName(attributeName);
			attr.setAttrValue(attributeName);
			attr.setAttrCategory(attrCat);
			attr.setType(type);
			attr.setImage(image);
			getSession().update(attr);
			image.getAttributeList().add(attr);
			getSession().update(image);
		}

	}


	private List<Attribute> getUniqueAttributes(List<Attribute>attrList,List<Attribute>originalAttrList){
		List<Attribute>uniqueList=new ArrayList<Attribute>();
		if(attrList!=null && attrList.size()>0 && attrList!=null && attrList.size()>0){
			for(Attribute attr:attrList){
				if(!originalAttrList.contains(attr)){
					uniqueList.add(attr);
				}
			}
		}

		return uniqueList;
	}

	@Override
	public void updateImageAttributeAnsweredBy(String attrId,User answeredBy,String imageId) {
		Attribute attr = (Attribute) getSession().get(Attribute.class,attrId);
		if(attr!=null){
		 	if(answeredBy!=null){
				attr.getAnsweredBy().add(answeredBy.getId());
			}
			getSession().update(attr);
		
	}
		
	}

	@Override
	public void updateImageAttributeIgnoredBy(String attrId, User answeredBy, String imageId) {
		Attribute attr = (Attribute) getSession().get(Attribute.class,attrId);
		if(attr!=null){
		 	if(answeredBy!=null){
				attr.getIgnoredBy().add(answeredBy.getId());
			}
			getSession().update(attr);
		
	}
		
	}

	@Override
	public void updateImageAttributeWrongAnsweredBy(String attrId, User answeredBy, String imageId) {
		Attribute attr = (Attribute) getSession().get(Attribute.class,attrId);
		if(attr!=null){
		 	if(answeredBy!=null){
				attr.getWrongAnsweredBy().add(answeredBy.getId());
			}
			getSession().update(attr);
		
	}
		
	}

	@Override
	public void updateImageSeenBy(String id, User seenBy) {
		Image img = (Image) getSession().get(Image.class,id);
		if(img!=null){
		 	if(seenBy!=null){
				img.getSeenBy().add(seenBy.getId());
			}
			getSession().update(img);
		
	}
		
	}


}
