/**
 * 
 */
package com.esp.model.game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.esp.model.common.Root;
import com.esp.model.image.Attribute;

/**
 * @author Amit
 *
 */
@Entity
@Table(name = "PLAYER_ATTRIBUTE")
public class PlayerAttribute extends Root implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Column(name = "PLAYER_ID")
	private String playerId;
	
	
	@OneToMany(fetch=FetchType.EAGER)
	private List<Attribute> attribute = new ArrayList<Attribute>();

	/**
	 * @return the playerId
	 */
	public String getPlayerId() {
		return playerId;
	}

	/**
	 * @param playerId the playerId to set
	 */
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public List<Attribute> getAttribute() {
		return attribute;
	}

	public void setAttribute(List<Attribute> attribute) {
		this.attribute = attribute;
	}

	

}
