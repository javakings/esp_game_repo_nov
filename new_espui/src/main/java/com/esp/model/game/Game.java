/**
 * 
 */
package com.esp.model.game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.esp.model.common.Root;
import com.esp.model.type.GAMEMODE;
import com.esp.model.user.User;



/**
 * @author Amit
 *
 */
@Entity
@Table(name = "GAME")
public class Game extends Root implements Serializable{

	@OneToOne(cascade = CascadeType.ALL)
	private User player1;
	
	@OneToOne(cascade = CascadeType.ALL)
	private User player2;
	
	@Column(name = "START_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startTime;
	
	@Column(name = "PLAYED_TIME")
	private Integer playedTime;
	
	@Column(name = "SCORE")
	private Integer score;
	
	@Column(name = "LATEST_ATTR")
	private String latestAttr;
	
	@Column(name="GAME_MODE")
	@Enumerated(EnumType.STRING)
	private GAMEMODE gameMode= GAMEMODE.ASSISTED;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<GameAttributeSummary> attrSummaryList = new ArrayList<GameAttributeSummary>();

	/**
	 * @return the player1
	 */
	public User getPlayer1() {
		return player1;
	}

	/**
	 * @param player1 the player1 to set
	 */
	public void setPlayer1(User player1) {
		this.player1 = player1;
	}

	/**
	 * @return the player2
	 */
	public User getPlayer2() {
		return player2;
	}

	/**
	 * @param player2 the player2 to set
	 */
	public void setPlayer2(User player2) {
		this.player2 = player2;
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the playedTime
	 */
	public Integer getPlayedTime() {
		return playedTime;
	}

	/**
	 * @param playedTime the playedTime to set
	 */
	public void setPlayedTime(Integer playedTime) {
		this.playedTime = playedTime;
	}

	/**
	 * @return the score
	 */
	public Integer getScore() {
		return score;
	}

	/**
	 * @return the gameMode
	 */
	public GAMEMODE getGameMode() {
		return gameMode;
	}

	/**
	 * @param gameMode the gameMode to set
	 */
	public void setGameMode(GAMEMODE gameMode) {
		this.gameMode = gameMode;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(Integer score) {
		this.score = score;
	}

	/**
	 * @return the latestAttr
	 */
	public String getLatestAttr() {
		return latestAttr;
	}

	/**
	 * @param latestAttr the latestAttr to set
	 */
	public void setLatestAttr(String latestAttr) {
		this.latestAttr = latestAttr;
	}

	/**
	 * @return the attrSummaryList
	 */
	public List<GameAttributeSummary> getAttrSummaryList() {
		return attrSummaryList;
	}

	/**
	 * @param attrSummaryList the attrSummaryList to set
	 */
	public void setAttrSummaryList(List<GameAttributeSummary> attrSummaryList) {
		this.attrSummaryList = attrSummaryList;
	}
	
}
