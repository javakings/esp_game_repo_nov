/**
 * 
 */
package com.esp.model.game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.esp.model.common.Root;

/**
 * @author Amit
 *
 */

@Entity
@Table(name = "GAME_ATTRIBUTE_SUMMARY")
@NamedQueries({
	@NamedQuery(name="gameattrsummary.imageId", query = " select attrSum from GameAttributeSummary attrSum where attrSum.imageId = :imageId and attrSum.gameId = :gameId")
})
public class GameAttributeSummary extends Root implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "GAME_ID")
	private String gameId;
	
	@Column(name = "IMAGE_ID")
	private String imageId;
	
	@Column(name = "PLAYED_TIME")
	private Integer playedTime=0;
	
	public Integer getPlayedTime() {
		return playedTime;
	}

	public void setPlayedTime(Integer playedTime) {
		this.playedTime = playedTime;
	}

	@Column(name = "MATCHED_ATTR")
	private String matchedAttribute;
	
	@ElementCollection
	@CollectionTable(name="UNMATCHED_ATTR_TBL")
	private List<String> unMatchedAttr = new ArrayList<String>();

	
	
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="PLAYER_ATTRIBUE_RIGHT_ANSWERS")
	private List<String> playerAttributeForRightAnswers = new ArrayList<String>();
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="PLAYER_ATTRIBUE_WRONG_ANSWERS")
	private List<String> playerAttributeForWrongAnswers = new ArrayList<String>();
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="PLAYER_ATTRIBUE_NO_ANSWERS")
	private List<String> playerAttributeForNoAnswers = new ArrayList<String>();
	
	/**
	 * @return the gameId
	 */
	public String getGameId() {
		return gameId;
	}

	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the imageId
	 */
	public String getImageId() {
		return imageId;
	}

	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	/**
	 * @return the matchedAttribute
	 */
	public String getMatchedAttribute() {
		return matchedAttribute;
	}

	/**
	 * @param matchedAttribute the matchedAttribute to set
	 */
	public void setMatchedAttribute(String matchedAttribute) {
		this.matchedAttribute = matchedAttribute;
	}

	/**
	 * @return the unMatchedAttr
	 */
	public List<String> getUnMatchedAttr() {
		return unMatchedAttr;
	}

	/**
	 * @param unMatchedAttr the unMatchedAttr to set
	 */
	public void setUnMatchedAttr(List<String> unMatchedAttr) {
		this.unMatchedAttr = unMatchedAttr;
	}

	public List<String> getPlayerAttributeForRightAnswers() {
		return playerAttributeForRightAnswers;
	}

	public void setPlayerAttributeForRightAnswers(List<String> playerAttributeForRightAnswers) {
		this.playerAttributeForRightAnswers = playerAttributeForRightAnswers;
	}

	public List<String> getPlayerAttributeForWrongAnswers() {
		return playerAttributeForWrongAnswers;
	}

	public void setPlayerAttributeForWrongAnswers(List<String> playerAttributeForWrongAnswers) {
		this.playerAttributeForWrongAnswers = playerAttributeForWrongAnswers;
	}

	public List<String> getPlayerAttributeForNoAnswers() {
		return playerAttributeForNoAnswers;
	}

	public void setPlayerAttributeForNoAnswers(List<String> playerAttributeForNoAnswers) {
		this.playerAttributeForNoAnswers = playerAttributeForNoAnswers;
	}

	


	

}
