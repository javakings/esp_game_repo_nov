/**
 * 
 */
package com.esp.model.gamesettings;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.esp.model.common.Root;



/**
 * @author Amit
 *
 */
@Entity
@Table(name = "SETTINGS")
public class Settings extends Root implements Serializable{

	@Column(name = "DEFAULT_PLAY_TIME")
	@Temporal(TemporalType.TIME)
	private Date defaultPlayTime;
	
	@Column(name = "SCORE_PER_ATTRIBUTE_MATCHED")
	private Integer scorePerAttributeMatched;
	


	/**
	 * @return the defaultPlayTime
	 */
	public Date getDefaultPlayTime() {
		return defaultPlayTime;
	}

	/**
	 * @param defaultPlayTime the defaultPlayTime to set
	 */
	public void setDefaultPlayTime(Date defaultPlayTime) {
		this.defaultPlayTime = defaultPlayTime;
	}

	public Integer getScorePerAttributeMatched() {
		return scorePerAttributeMatched;
	}

	public void setScorePerAttributeMatched(Integer scorePerAttributeMatched) {
		this.scorePerAttributeMatched = scorePerAttributeMatched;
	}


}
