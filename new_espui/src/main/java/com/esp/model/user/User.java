/**
 * 
 */
package com.esp.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.esp.model.common.Root;
import com.esp.model.type.GENDER;
import com.esp.model.type.USERTYPE;




/**
 * @author Amit
 *
 */
@Entity
@Table(name = "USER")
public class User extends Root implements Serializable{

	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="CITY")
	private String city;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name = "USER_IMAGE_PATH")
	private String userImagePath;
	
	@Column(name = "USER_READ_FILE_PATH")
	private String userReadFilePath;
	
	@Column(name="USER_TYPE")
	@Enumerated(EnumType.STRING)
	private USERTYPE type= USERTYPE.USER;
	
	@Column(name="GENDER")
	@Enumerated(EnumType.STRING)
	private GENDER gender= GENDER.MALE;

	@Column(name = "ISBUSY")
	private boolean isBusy;
	
	@Column(name= "ISONLINE")
	private boolean isOnline;
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the type
	 */
	public USERTYPE getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(USERTYPE type) {
		this.type = type;
	}

	public GENDER getGender() {
		return gender;
	}

	public void setGender(GENDER gender) {
		this.gender = gender;
	}

	public String getUserImagePath() {
		return userImagePath;
	}

	public void setUserImagePath(String userImagePath) {
		this.userImagePath = userImagePath;
	}

	public String getUserReadFilePath() {
		return userReadFilePath;
	}

	public void setUserReadFilePath(String userReadFilePath) {
		this.userReadFilePath = userReadFilePath;
	}

	/**
	 * @return the isBusy
	 */
	public boolean isBusy() {
		return isBusy;
	}

	/**
	 * @param isBusy the isBusy to set
	 */
	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}

	/**
	 * @return the isOnline
	 */
	public boolean isOnline() {
		return isOnline;
	}

	/**
	 * @param isOnline the isOnline to set
	 */
	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}
	
	
	
}
