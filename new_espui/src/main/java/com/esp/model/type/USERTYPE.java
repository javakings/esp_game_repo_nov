/**
 * 
 */
package com.esp.model.type;

/**
 * @author Amit
 *
 */
public enum USERTYPE {

	ADMIN, USER
}
