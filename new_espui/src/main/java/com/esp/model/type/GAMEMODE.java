/**
 * 
 */
package com.esp.model.type;

/**
 * @author gyan
 *
 */
public enum GAMEMODE {
   ASSISTED,TWO_PLAYER,NON_ASSISTED,ASSISTED_MUSIC,NON_ASSISTED_MUSIC
}
