/**
 * 
 */
package com.esp.model.type;

/**
 * @author Jay
 *
 */
public enum ATTRIBUTETYPE {
    RELEVANT,IRRELEVANT,DEFAULT
}
