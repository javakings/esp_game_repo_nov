/**
 * 
 */
package com.esp.model.type;

/**
 * @author Amit
 *
 */
public enum ClassType {

	Integer, Double, String, Float
	
}
