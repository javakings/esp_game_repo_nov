/**
 * 
 */
package com.esp.model.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author Amit
 *
 */
@MappedSuperclass
public abstract class Root implements Serializable{

	/**
	 * 
	 * Represents the Primary ID of the Object in the table.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "PRIMARYID")
	private String id;



	/**
	 * The time stamp when the object was created in database
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATIONDATE", nullable = false, updatable = false)
	private Date sysCreationDate ;

	/**
	 * The time stamp when the object was modified in the database
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LASTMODIFIED", nullable = false)
	private Date sysLastModified ;

	public Root() {
		sysLastModified = sysCreationDate = new Date();
	}
	

	@PrePersist
	protected void onCreate() {
		sysLastModified = sysCreationDate = new Date();
	}

	/**
	 * @return the sysCreationDate
	 */
	public Date getSysCreationDate() {
		return sysCreationDate;
	}

	/**
	 * @param sysCreationDate
	 *            the sysCreationDate to set
	 */
	public void setSysCreationDate(Date sysCreationDate) {
		this.sysCreationDate = sysCreationDate;
	}

	/**
	 * @return the sysLastModified
	 */
	public Date getSysLastModified() {
		return sysLastModified;
	}

	/**
	 * @param sysLastModified
	 *            the sysLastModified to set
	 */
	public void setSysLastModified(Date sysLastModified) {
		this.sysLastModified = sysLastModified;
	}


	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	@PreUpdate
	protected void onUpdate() {
		sysLastModified = new Date();
	}

	@PostUpdate
	protected void postUpdate() {
		/**
		 * Update cache
		 */
		sysLastModified = new Date();
	}

	@PostPersist
	protected void postPersist() {
		/**
		 * Register in cache.
		 */

	}

	@PostRemove
	protected void postRemove() {
		/**
		 * Remove from cache.
		 */

	}

	@PostLoad
	protected void postLoad() {
		/**
		 * Update cache.
		 */

	}

	@Override
	public boolean equals(Object other) {

		boolean matches = false;
		if (other instanceof Root) {
			Root otherObj = (Root) other;
			if (otherObj.getId() != null) {
				matches = this.getId().equals(otherObj.getId());
			}
		}

		return matches;
	}

	@Override
	public int hashCode() {
		int code = super.hashCode();
		if (this.getId() != null) {
			code = this.getId().hashCode();

		}
		return code;
	}

}
