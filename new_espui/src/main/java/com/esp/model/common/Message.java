/**
 * 
 */
package com.esp.model.common;

import java.util.List;

/**
 * @author Amit
 *
 */
public class Message {
	
	private String message;
	
	private List<Object>objList;
	
	private boolean status;
	
	private Object msgObj;
	
	
	public List<Object> getObjList() {
		return objList;
	}

	public void setObjList(List<Object> objList) {
		this.objList = objList;
	}

	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public Object getMsgObj() {
		return msgObj;
	}

	public void setMsgObj(Object msgObj) {
		this.msgObj = msgObj;
	}
	
	

}
