/**
 * 
 */
package com.esp.model.image;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.esp.model.common.Root;

/**
 * @author Jay
 *
 */
@Entity
@Table(name = "ATTRIBUTE_CATEGORY")
public class AttributeCategory extends Root implements Serializable {
   
	@Column(name="CAT_NAME")
	private String categoryName="DEFAULT";

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
