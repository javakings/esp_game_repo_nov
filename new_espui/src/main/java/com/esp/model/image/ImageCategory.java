/**
 * 
 */
package com.esp.model.image;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.esp.model.common.Root;



/**
 * @author Amit
 *
 */
@Entity
@Table(name = "IMAGE_CATAOGORY")
public class ImageCategory extends Root implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "CATAGORY_NAME")
	private String categoryName;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
	
	
}
