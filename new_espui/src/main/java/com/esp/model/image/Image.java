/**
 * 
 */
package com.esp.model.image;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.esp.model.common.Root;
import com.esp.model.type.FILETYPE;
import com.esp.model.type.GAMEMODE;


/**
 * @author Amit
 *
 */
@Entity
@Table(name = "IMAGE")
public class Image extends Root implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@OneToOne
	private ImageCategory imageCatagory;
	
	@Column(name = "IMAGE_NAME")
	private String imageName;
	
	@Column(name = "IMAGE_PATH")
	private String imagePath;
	
	@Column(name = "REAL_NAME")
	private String realName;
	
	@Column(name = "READ_FILE_PATH")
	private String readFilePath;
	
	@Column(name="FILE_TYPE")
	@Enumerated(EnumType.STRING)
	private FILETYPE fileType;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "image")
	@Fetch(value = FetchMode.SUBSELECT)
	@Cascade(CascadeType.ALL)
	@JsonIgnore
	private List<Attribute> attributeList = new ArrayList<Attribute>();
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection
	@CollectionTable(name="IMAGE_SEEN_BY")
	private List<String> seenBy = new ArrayList<String>();
	

	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the realName
	 */
	public String getRealName() {
		return realName;
	}

	/**
	 * @param realName the realName to set
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * @return the attributeList
	 */
	public List<Attribute> getAttributeList() {
		return attributeList;
	}

	/**
	 * @param attributeList the attributeList to set
	 */
	public void setAttributeList(List<Attribute> attributeList) {
		this.attributeList = attributeList;
	}

	public String getReadFilePath() {
		return readFilePath;
	}

	public void setReadFilePath(String readFilePath) {
		this.readFilePath = readFilePath;
	}

	public ImageCategory getImageCatagory() {
		return imageCatagory;
	}

	public void setImageCatagory(ImageCategory imageCatagory) {
		this.imageCatagory = imageCatagory;
	}

	public FILETYPE getFileType() {
		return fileType;
	}

	public void setFileType(FILETYPE fileType) {
		this.fileType = fileType;
	}

	public List<String> getSeenBy() {
		return seenBy;
	}

	public void setSeenBy(List<String> seenBy) {
		this.seenBy = seenBy;
	}
	
}
