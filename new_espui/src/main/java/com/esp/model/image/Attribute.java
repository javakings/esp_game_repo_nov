/**
 * 
 */
package com.esp.model.image;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.esp.model.common.Root;
import com.esp.model.type.ATTRIBUTEFORTYPE;
import com.esp.model.type.ATTRIBUTETYPE;



/**
 * @author Amit
 *
 */
@Entity
@Table(name = "ATTRIBUTE")
public class Attribute extends Root implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */

	@Column(name = "ATTR_NAME")
	private String attrName;
	
	@Column(name = "ATTR_VALUE")
	private String attrValue;
	
	@Column(name="ATTRIBUTE_TYPE")
	@Enumerated(EnumType.STRING)
	private ATTRIBUTETYPE type= ATTRIBUTETYPE.RELEVANT;
	
	
	@Column(name="ATTRIBUTE_FOR_TYPE")
	@Enumerated(EnumType.STRING)
	private ATTRIBUTEFORTYPE attributeForType;
	
	@OneToOne
	private AttributeCategory attrCategory;
	
	@ManyToOne
	private Image image;
	
	@Override
	public boolean equals(Object other) {
		if(other == null)                return false;
	    if(!(other instanceof Attribute) ) return false;

	    Attribute attr = (Attribute) other;
	    return this.attrName.equalsIgnoreCase(attr.attrName);
	}
	
	
	@Override
	public int hashCode() {
		int hash=7;
		hash = 31 * hash + (null == attrName ? 0 : attrName.hashCode());
		return 0;
	}
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection
	@CollectionTable(name="ATTR_IGNOREDBY_TBL")
	private List<String> ignoredBy = new ArrayList<String>();
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection
	@CollectionTable(name="ATTR_ANSWERBY_TBL")
	private List<String> answeredBy = new ArrayList<String>();
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection
	@CollectionTable(name="ATTR_WRONGANSWERBY_TBL")
	private List<String> wrongAnsweredBy = new ArrayList<String>();



	public List<String> getAnsweredBy() {
		return answeredBy;
	}


	public void setAnsweredBy(List<String> answeredBy) {
		this.answeredBy = answeredBy;
	}


	/**
	 * @return the attrName
	 */
	public String getAttrName() {
		return attrName;
	}

	/**
	 * @param attrName the attrName to set
	 */
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	/**
	 * @return the attrValue
	 */
	public String getAttrValue() {
		return attrValue;
	}

	/**
	 * @param attrValue the attrValue to set
	 */
	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

	public AttributeCategory getAttrCategory() {
		return attrCategory;
	}

	public void setAttrCategory(AttributeCategory attrCategory) {
		this.attrCategory = attrCategory;
	}


	public ATTRIBUTETYPE getType() {
		return type;
	}


	public void setType(ATTRIBUTETYPE type) {
		this.type = type;
	}


	/**
	 * @return the image
	 */
	public Image getImage() {
		return image;
	}


	/**
	 * @param image the image to set
	 */
	public void setImage(Image image) {
		this.image = image;
	}


	public List<String> getIgnoredBy() {
		return ignoredBy;
	}


	public void setIgnoredBy(List<String> ignoredBy) {
		this.ignoredBy = ignoredBy;
	}


	public List<String> getWrongAnsweredBy() {
		return wrongAnsweredBy;
	}


	public void setWrongAnsweredBy(List<String> wrongAnsweredBy) {
		this.wrongAnsweredBy = wrongAnsweredBy;
	}


	public ATTRIBUTEFORTYPE getAttributeForType() {
		return attributeForType;
	}


	public void setAttributeForType(ATTRIBUTEFORTYPE attributeForType) {
		this.attributeForType = attributeForType;
	}

}
