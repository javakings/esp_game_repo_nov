/**
 * 
 */
package com.esp.model.auth;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.esp.model.common.Root;
import com.esp.model.user.User;



/**
 * @author Amit
 *
 */
@Entity
@Table(name = "AUTHENTICATION")
@NamedQueries({
		@NamedQuery(name = "Authentication.findByLoginId", query = "select auth from  Authentication auth where auth.loginID = :loginID")
})
public class Authentication extends Root implements Serializable{

	@Column(name = "LOGIN_ID")
	private String loginID;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	private User user;

	/**
	 * @return the loginID
	 */
	public String getLoginID() {
		return loginID;
	}

	/**
	 * @param loginID the loginID to set
	 */
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		
		this.password = password;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
