/**
 * 
 */
package com.esp.requestbean;

import java.util.List;
import java.util.Map;

/**
 * @author Jay
 *
 */
public class SubmitNonAssistedAnswerRequest {
	private String imageId;
	private Map<String,String> answers;
	private String timeTaken;
	
	public String getImageId() {
		return imageId;
	}

	

	public Map<String, String> getAnswers() {
		return answers;
	}



	public void setAnswers(Map<String, String> answers) {
		this.answers = answers;
	}



	public void setImageId(String imageId) {
		this.imageId = imageId;
	}



	public String getTimeTaken() {
		return timeTaken;
	}



	public void setTimeTaken(String timeTaken) {
		this.timeTaken = timeTaken;
	}

	
	

	

}
