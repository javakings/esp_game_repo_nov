/**
 * 
 */
package com.esp.requestbean;

import java.util.List;

/**
 * @author Jay
 *
 */
public class SubmitAnswerRequest {
   private String imageId;
   private List<String> answers;
   private String timeTaken;
   
public String getImageId() {
	return imageId;
}
public void setImageId(String imageId) {
	this.imageId = imageId;
}
public List<String> getAnswers() {
	return answers;
}
public void setAnswers(List<String> answers) {
	this.answers = answers;
}
public String getTimeTaken() {
	return timeTaken;
}
public void setTimeTaken(String timeTaken) {
	this.timeTaken = timeTaken;
}

}
