/**
 * 
 */
package com.esp.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.esp.bean.UserBean;
import com.esp.constant.EspConstant;

/**
 * @author Jay
 *
 */
@WebFilter("/app")
public class AuthenticationFilter implements Filter {

	public void destroy() {
		
		
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		    HttpServletRequest req=(HttpServletRequest)request;
		    HttpServletResponse res=(HttpServletResponse)response;
		    HttpSession session=req.getSession();
		    String uri=req.getRequestURI();
		    UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
		    if(userBean==null && !uri.endsWith("login")){
		    	res.sendRedirect(req.getContextPath()+"/");
		    }else{
		    	chain.doFilter(request, response);
		    }
		    
		   
		
	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
