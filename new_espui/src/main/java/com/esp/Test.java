package com.esp;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.esp.configuration.AppConfig;
import com.esp.model.type.USERTYPE;
import com.esp.model.user.User;
import com.esp.service.UserService;




public class Test {

	
  public static void main(String args[]){
	  
	  
	  User user=new User();
	  user.setFirstName("jay");
	  user.setLastName("kishore");
	  user.setCity("pune");
	  user.setEmail("asas@gmail.com");
	  user.setType(USERTYPE.USER);
	  user.setSysCreationDate(new Date());
	  user.setSysLastModified(new Date());
	  
	  ApplicationContext appContext = 
	    	  new ClassPathXmlApplicationContext("BeanLocations.xml");
	  UserService service=appContext.getBean("UserService",UserService.class);
	 
		service.saveUser(user, "jay", "jay");
  }
}
