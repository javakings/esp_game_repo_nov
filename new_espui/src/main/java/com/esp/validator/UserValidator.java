/**
 * 
 */
package com.esp.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.esp.form.UserForm;
import com.esp.model.user.User;
import com.esp.service.UserService;

/**
 * @author Amit
 *
 */
@Component
public class UserValidator implements Validator{

	@Autowired
	private UserService service;
	
	public boolean supports(Class<?> arg0) {
		return User.class.isAssignableFrom(arg0) || UserForm.class.isAssignableFrom(arg0);
	}

	public void validate(Object arg0, Errors error) {
		if(arg0 instanceof UserForm)
		{
			UserForm userForm = (UserForm)arg0;
			User user = service.getUserByLoginId(userForm.getLoginId());
			if(user != null)
			{
				error.reject("alreadyExists", userForm.getLoginId() +" is unAvailable");
			}
		}
		
	}

}
