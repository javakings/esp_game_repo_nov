package com.esp.comet;

import java.io.Serializable;

import javax.servlet.AsyncContext;



/**
 * @author Aditya
 * 
 */
public class AjaxQueue implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3445348615341706126L;

	private AsyncContext context = null;

	private String registerID = null;

	public AjaxQueue(AsyncContext context, String registerID) {
		super();
		this.context = context;
		this.registerID = registerID;

	}

	public void setFormatter(JSONFormat jsonFormat) {
		// TODO Auto-generated method stub

	}

	public AsyncContext getContext() {
		return context;
	}

	/**
	 * @return the registerID
	 */
	public String getRegisterID() {
		return registerID;
	}

	/**
	 * @param registerID the registerID to set
	 */
	public void setRegisterID(String registerID) {
		this.registerID = registerID;
	}

	/**
	 * @param context the context to set
	 */
	public void setContext(AsyncContext context) {
		this.context = context;
	}


	

}
