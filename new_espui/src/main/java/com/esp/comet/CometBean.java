
package com.esp.comet;

/**
 * @author Amit
 *
 */
public class CometBean {
	
	private String gameId;
	private String requesterId;
	private String accepterId;
	/**
	 * @return the gameId
	 */
	public String getGameId() {
		return gameId;
	}
	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	/**
	 * @return the requesterId
	 */
	public String getRequesterId() {
		return requesterId;
	}
	/**
	 * @param requesterId the requesterId to set
	 */
	public void setRequesterId(String requesterId) {
		this.requesterId = requesterId;
	}
	/**
	 * @return the accepterId
	 */
	public String getAccepterId() {
		return accepterId;
	}
	/**
	 * @param accepterId the accepterId to set
	 */
	public void setAccepterId(String accepterId) {
		this.accepterId = accepterId;
	}
	

}
