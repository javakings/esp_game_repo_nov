
package com.esp.comet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.esp.model.game.Game;
import com.esp.model.image.Image;

/**
 * 
 * @author Amit
 *
 */
@WebListener
public class AjaxPushBootStrapper implements ServletContextListener {

   // Logger logger = Logger.getLogger(AjaxPushBootStrapper.class);	
	AjaxPushSvc apsvc  =null;

	public void contextInitialized(ServletContextEvent sce) {
		/**
		 * Loads the config 
		 */
		
		ConcurrentLinkedQueue<String> playerQueues = new  ConcurrentLinkedQueue<String>();
		sce.getServletContext().setAttribute("playerQueues",playerQueues);
		Map<String, Game> gameMap = new HashMap<String, Game>();
		sce.getServletContext().setAttribute("gameMap",gameMap);
		
		Map<String, List<Image>> gameImageMap = new HashMap<String, List<Image>>();
		sce.getServletContext().setAttribute("gameImageMap",gameImageMap);
		
		ConcurrentLinkedQueue<AjaxQueue> ajaxQueues = new  ConcurrentLinkedQueue<AjaxQueue>();
		sce.getServletContext().setAttribute("ajaxQueues",ajaxQueues);
		
		
		ConcurrentLinkedQueue<CometBean> objectPool = new  ConcurrentLinkedQueue<CometBean>();
		sce.getServletContext().setAttribute("objectPool",objectPool);
		
     	apsvc = new AjaxPushSvc( );
		apsvc.setSc(sce.getServletContext());
		apsvc.start();
	
		
		


	}

	public void contextDestroyed(ServletContextEvent sce) {
	
	
	}
}
