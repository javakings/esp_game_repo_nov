package com.esp.comet;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.AsyncContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


@WebServlet(name = "apeBinder", urlPatterns = { "/ape" }, asyncSupported = true)
public class AjaxQueueAgent extends HttpServlet {

	Logger logger = Logger.getLogger(AjaxQueueAgent.class);
	/**
	 * This class is a temporary arrangement we need to have a Service instead
	 * of a Controller.
	 */
	private static final long serialVersionUID = 6915845304687642652L;
	static AtomicInteger clientcount = new AtomicInteger(0);

	// Register AJAX requests
	public void doGet(HttpServletRequest request, HttpServletResponse response) {

		{

			if (request.isAsyncSupported()) {

				
				String registerID = request.getParameter("registerID");
				if (registerID != null) {

						ConcurrentLinkedQueue<AjaxQueue> ajaxQueues = (ConcurrentLinkedQueue<AjaxQueue>) this
								.getServletContext().getAttribute("ajaxQueues");
						AsyncContext aCtx = request.startAsync();

						
						AjaxQueue ajaxQueue = new AjaxQueue(aCtx, registerID);
						aCtx.addListener(new AjaxPushAsyncContextListener(
								ajaxQueue, this.getServletContext()));

						ajaxQueues.add(ajaxQueue);

				}
			}

		}
	}

	// No op
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

	}

}
