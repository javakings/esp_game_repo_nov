package com.esp.comet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.servlet.AsyncContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;

public class AjaxPushSvc extends Thread {
	protected ServletContext sc = null;
	private static Logger logger = Logger.getLogger(AjaxPushSvc.class);

	public AjaxPushSvc() {
		super();
		currentThread().setName("AjaxPushSvc");
	}

	public ServletContext getSc() {
		return sc;
	}

	public void setSc(ServletContext sc) {
		this.sc = sc;
	}

	@Override
	public void run() {
		if (sc != null) {

			try {
				Executor eventExecutor = Executors.newCachedThreadPool();
				while (true) {
					ConcurrentLinkedQueue<CometBean> objectPool = (ConcurrentLinkedQueue<CometBean>) sc
							.getAttribute("objectPool");
					try {
						final CometBean objectInPool = objectPool.poll();
						if (objectInPool != null) {
							eventExecutor.execute(new Runnable() {
								public void run() {
									ConcurrentLinkedQueue<AjaxQueue> ajaxQueues = (ConcurrentLinkedQueue<AjaxQueue>) sc
											.getAttribute("ajaxQueues");
									for (final AjaxQueue ajaxQueue : ajaxQueues) {
										if (objectInPool.getAccepterId().equals(ajaxQueue.getRegisterID())) {
											final AsyncContext aCtx = ajaxQueue.getContext();
											boolean isCommitted = false;
											try {
												HttpServletResponse response = (HttpServletResponse) aCtx.getResponse();
												isCommitted = response.isCommitted();
												if (!isCommitted) {
													response.setContentType("application/json");
													PrintWriter writer = response.getWriter();
													JSONArray jsonArray = new JSONArray();
													new JSONFormat().formatOutput(jsonArray, objectInPool, sc);
													writer.write(jsonArray.toString());

													response.setStatus(HttpServletResponse.SC_OK);
													response.flushBuffer();
													aCtx.complete();

												}
											} catch (IOException e) {

												logger.error("AjaxPushSvc::Exception", e);
											}

											break;
										}
									}// /
								}
							});
						}
					} catch (Exception ex) {

						// logger.info("AjaxPushSvc::Exception", ex);

					}
				}
			} catch (Exception ex) {
				logger.error("AjaxPushSvc::Exception", ex);

			}
		}

	}

}
