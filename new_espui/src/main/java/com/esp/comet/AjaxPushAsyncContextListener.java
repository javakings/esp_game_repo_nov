package com.esp.comet;

import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletContext;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author Amit
 *
 */
public class AjaxPushAsyncContextListener implements AsyncListener {

	AjaxQueue queue = null ;
	private ServletContext sc;

	public AjaxPushAsyncContextListener(AjaxQueue queue, ServletContext sc) {
		this.queue = queue;
		this.sc = sc;
	}

		
	public void onTimeout(AsyncEvent ae) throws IOException {
	timeOut(ae);

	}

	private void timeOut(AsyncEvent ae) throws IOException {
		AsyncContext aContext = ae.getAsyncContext();
		synchronized (aContext) {
			ServletResponse response = aContext.getResponse();
			if (response instanceof HttpServletResponse) {
				HttpServletResponse htr = (HttpServletResponse) response;
				htr.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT);
				htr.flushBuffer();
                aContext.complete();
			}
	   }
		unRegisterContext(ae);

	}

	private void unRegisterContext(AsyncEvent ae) {
		ConcurrentLinkedQueue<AjaxQueue> ajaxQueues = (ConcurrentLinkedQueue<AjaxQueue>) sc.getAttribute("ajaxQueues");
		ajaxQueues.remove(queue);
	}

	public void onComplete(AsyncEvent arg0) throws IOException {
		unRegisterContext(arg0);		
	}

	public void onError(AsyncEvent arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

	public void onStartAsync(AsyncEvent arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

}
