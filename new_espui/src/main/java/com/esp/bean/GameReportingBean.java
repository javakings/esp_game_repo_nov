/**
 * 
 */
package com.esp.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.esp.model.image.Attribute;
import com.esp.model.image.Image;

/**
 * @author Jay
 *
 */
public class GameReportingBean extends BaseBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8518866703823488609L;
	
	private String assistedGameCount="";
	
	private String nonAssistedGameCount="";
	
	private String assistedGamePlayedByUser="";
	
	private String unassistedGamePlayedByUser="";
	
	private String gameUniquePlayerCountAssisted="";
	
	private String gameUniquePlayerCountNonAssisted="";
	
	private Image image;
	
	private String wrongHitCoun="";
	
	private String averageTimeAttemptAssisted;
	
	private String minTimeAttemptAssisted;
	
	private String maxTimeAttemptAssisted;
	
	private String averageTimeAttemptNonAssisted;
	
	private String minTimeAttemptNonAssisted;
	
	private String maxTimeAttemptNonAssisted;
	
	private String playerNumberForImageAssisted;
	
	private String playerNumberForImageNonAssisted;
	
	private List<AttributeReporting>correctHitCountList;
	
	private List<AttributeReporting>defaultCorrectCountList;
	
	private String excelDownloadPath;

	public String getAssistedGameCount() {
		return assistedGameCount;
	}

	public void setAssistedGameCount(String assistedGameCount) {
		this.assistedGameCount = assistedGameCount;
	}

	public String getNonAssistedGameCount() {
		return nonAssistedGameCount;
	}

	public void setNonAssistedGameCount(String nonAssistedGameCount) {
		this.nonAssistedGameCount = nonAssistedGameCount;
	}

	public String getGameUniquePlayerCountAssisted() {
		return gameUniquePlayerCountAssisted;
	}

	public void setGameUniquePlayerCountAssisted(String gameUniquePlayerCountAssisted) {
		this.gameUniquePlayerCountAssisted = gameUniquePlayerCountAssisted;
	}

	public String getGameUniquePlayerCountNonAssisted() {
		return gameUniquePlayerCountNonAssisted;
	}

	public void setGameUniquePlayerCountNonAssisted(String gameUniquePlayerCountNonAssisted) {
		this.gameUniquePlayerCountNonAssisted = gameUniquePlayerCountNonAssisted;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getWrongHitCoun() {
		return wrongHitCoun;
	}

	public void setWrongHitCoun(String wrongHitCoun) {
		this.wrongHitCoun = wrongHitCoun;
	}

	public String getAverageTimeAttemptAssisted() {
		return averageTimeAttemptAssisted;
	}

	public void setAverageTimeAttemptAssisted(String averageTimeAttemptAssisted) {
		this.averageTimeAttemptAssisted = averageTimeAttemptAssisted;
	}

	public String getMinTimeAttemptAssisted() {
		return minTimeAttemptAssisted;
	}

	public String getAssistedGamePlayedByUser() {
		return assistedGamePlayedByUser;
	}

	public void setAssistedGamePlayedByUser(String assistedGamePlayedByUser) {
		this.assistedGamePlayedByUser = assistedGamePlayedByUser;
	}

	public String getUnassistedGamePlayedByUser() {
		return unassistedGamePlayedByUser;
	}

	public void setUnassistedGamePlayedByUser(String unassistedGamePlayedByUser) {
		this.unassistedGamePlayedByUser = unassistedGamePlayedByUser;
	}

	public void setMinTimeAttemptAssisted(String minTimeAttemptAssisted) {
		this.minTimeAttemptAssisted = minTimeAttemptAssisted;
	}

	public String getMaxTimeAttemptAssisted() {
		return maxTimeAttemptAssisted;
	}

	public void setMaxTimeAttemptAssisted(String maxTimeAttemptAssisted) {
		this.maxTimeAttemptAssisted = maxTimeAttemptAssisted;
	}

	public String getAverageTimeAttemptNonAssisted() {
		return averageTimeAttemptNonAssisted;
	}

	public void setAverageTimeAttemptNonAssisted(String averageTimeAttemptNonAssisted) {
		this.averageTimeAttemptNonAssisted = averageTimeAttemptNonAssisted;
	}

	public String getMinTimeAttemptNonAssisted() {
		return minTimeAttemptNonAssisted;
	}

	public void setMinTimeAttemptNonAssisted(String minTimeAttemptNonAssisted) {
		this.minTimeAttemptNonAssisted = minTimeAttemptNonAssisted;
	}

	public String getMaxTimeAttemptNonAssisted() {
		return maxTimeAttemptNonAssisted;
	}

	public void setMaxTimeAttemptNonAssisted(String maxTimeAttemptNonAssisted) {
		this.maxTimeAttemptNonAssisted = maxTimeAttemptNonAssisted;
	}

	public String getPlayerNumberForImageAssisted() {
		return playerNumberForImageAssisted;
	}

	public String getExcelDownloadPath() {
		return excelDownloadPath;
	}

	public void setExcelDownloadPath(String excelDownloadPath) {
		this.excelDownloadPath = excelDownloadPath;
	}

	public void setPlayerNumberForImageAssisted(String playerNumberForImageAssisted) {
		this.playerNumberForImageAssisted = playerNumberForImageAssisted;
	}

	public String getPlayerNumberForImageNonAssisted() {
		return playerNumberForImageNonAssisted;
	}

	public void setPlayerNumberForImageNonAssisted(String playerNumberForImageNonAssisted) {
		this.playerNumberForImageNonAssisted = playerNumberForImageNonAssisted;
	}

	public List<AttributeReporting> getCorrectHitCountList() {
		return correctHitCountList;
	}

	public void setCorrectHitCountList(List<AttributeReporting> correctHitCountList) {
		this.correctHitCountList = correctHitCountList;
	}

	public List<AttributeReporting> getDefaultCorrectCountList() {
		return defaultCorrectCountList;
	}

	public void setDefaultCorrectCountList(List<AttributeReporting> defaultCorrectCountList) {
		this.defaultCorrectCountList = defaultCorrectCountList;
	}


}
