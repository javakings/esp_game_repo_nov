/**
 * 
 */
package com.esp.bean;

import java.io.Serializable;

import com.esp.model.type.STATUS;

/**
 * @author jay
 *
 */
public class BaseBean implements Serializable {
	
   private STATUS status=STATUS.SUCCESS;
   private String failureMessage;
/**
 * @return the status
 */
public STATUS getStatus() {
	return status;
}
/**
 * @param status the status to set
 */
public void setStatus(STATUS status) {
	this.status = status;
}
/**
 * @return the failureMessage
 */
public String getFailureMessage() {
	return failureMessage;
}
/**
 * @param failureMessage the failureMessage to set
 */
public void setFailureMessage(String failureMessage) {
	this.failureMessage = failureMessage;
}
}
