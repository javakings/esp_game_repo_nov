/**
 * 
 */
package com.esp.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.esp.model.image.Attribute;
import com.esp.model.image.AttributeCategory;
import com.esp.model.image.ImageCategory;
import com.esp.model.type.IMAGECATAGORY;

/**
 * @author Jay
 *
 */
public class ImageBean extends BaseBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ImageCategory imageCatagory;
	
	private String imageName;
	
	private String imagePath;
	
	private String realName;
	
	private String readFilePath;
	
	private String imageId;
	
	private String imagesToShowNo;
	
	private Set<Attribute>attributeList;
	
	private List<AttributeCategory>attrCategoryList;
	
	private String sysCreationDate;
	
	

	public ImageCategory getImageCatagory() {
		return imageCatagory;
	}

	public void setImageCatagory(ImageCategory imageCatagory) {
		this.imageCatagory = imageCatagory;
	}

	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the realName
	 */
	public String getRealName() {
		return realName;
	}

	/**
	 * @param realName the realName to set
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * @return the readFilePath
	 */
	public String getReadFilePath() {
		return readFilePath;
	}

	/**
	 * @param readFilePath the readFilePath to set
	 */
	public void setReadFilePath(String readFilePath) {
		this.readFilePath = readFilePath;
	}

	/**
	 * @return the imageId
	 */
	public String getImageId() {
		return imageId;
	}

	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	/**
	 * @return the imagesToShowNo
	 */
	public String getImagesToShowNo() {
		return imagesToShowNo;
	}

	/**
	 * @param imagesToShowNo the imagesToShowNo to set
	 */
	public void setImagesToShowNo(String imagesToShowNo) {
		this.imagesToShowNo = imagesToShowNo;
	}

	public Set<Attribute> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(Set<Attribute> attributList) {
		this.attributeList = attributList;
	}

	public List<AttributeCategory> getAttrCategoryList() {
		return attrCategoryList;
	}

	public void setAttrCategoryList(List<AttributeCategory> attrCategoryList) {
		this.attrCategoryList = attrCategoryList;
	}

	/**
	 * @return the sysCreationDate
	 */
	public String getSysCreationDate() {
		return sysCreationDate;
	}

	/**
	 * @param sysCreationDate the sysCreationDate to set
	 */
	public void setSysCreationDate(String sysCreationDate) {
		this.sysCreationDate = sysCreationDate;
	}
}
