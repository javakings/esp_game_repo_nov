/**
 * 
 */
package com.esp.bean;

import java.io.Serializable;

import com.esp.model.user.User;

/**
 * @author Jay
 *
 */
public class UserInfoBean extends BaseBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 972121457120636393L;
	
	private User user;
	private String assistedGamePlayed="";
	private String nonAssistedGamePlayed="";
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getAssistedGamePlayed() {
		return assistedGamePlayed;
	}
	public void setAssistedGamePlayed(String assistedGamePlayed) {
		this.assistedGamePlayed = assistedGamePlayed;
	}
	public String getNonAssistedGamePlayed() {
		return nonAssistedGamePlayed;
	}
	public void setNonAssistedGamePlayed(String nonAssistedGamePlayed) {
		this.nonAssistedGamePlayed = nonAssistedGamePlayed;
	}

}
