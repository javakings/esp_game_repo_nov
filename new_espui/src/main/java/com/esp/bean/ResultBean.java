/**
 * 
 */
package com.esp.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author Amit
 *
 */
public class ResultBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String imageId;
	
	private String imagePath;
	
	private List<String> matchedAttribute;
	
	private int score;

	/**
	 * @return the imageId
	 */
	public String getImageId() {
		return imageId;
	}

	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the matchedAttribute
	 */
	public List<String> getMatchedAttribute() {
		return matchedAttribute;
	}

	/**
	 * @param matchedAttribute the matchedAttribute to set
	 */
	public void setMatchedAttribute(List<String> matchedAttribute) {
		this.matchedAttribute = matchedAttribute;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @return the serialversionuid
	 */

	
}
