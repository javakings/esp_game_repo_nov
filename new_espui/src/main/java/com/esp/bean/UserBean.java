package com.esp.bean;

import java.io.Serializable;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.esp.dao.UserDao;
import com.esp.model.type.USERTYPE;

@Repository("userBean")
public class UserBean extends BaseBean implements Serializable, HttpSessionBindingListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5300501440093157910L;

	private String userId;
	
	private String loginID;
	
	private String firstName;
	
	private String lastName;
	
	private USERTYPE userType;
	
	private String email;
	
	private String city;
	
	private String readImagePath;

	@Autowired
	private UserDao userDao;
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the loginID
	 */
	public String getLoginID() {
		return loginID;
	}

	/**
	 * @param loginID the loginID to set
	 */
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public USERTYPE getUserType() {
		return userType;
	}

	public void setUserType(USERTYPE userType) {
		this.userType = userType;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReadImagePath() {
		return readImagePath;
	}

	public void setReadImagePath(String readImagePath) {
		this.readImagePath = readImagePath;
	}

	public void valueBound(HttpSessionBindingEvent arg0) {
		//userDao.setOnlineStatus(this.userId);
	}

	public void valueUnbound(HttpSessionBindingEvent arg0) {
		//userDao.setOfflineStatus(this.userId);
	}
	
      @Override
    public String toString() {
    	// TODO Auto-generated method stub
    	return this.firstName+" "+this.lastName;
    }
	
}
