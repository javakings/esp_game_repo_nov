/**
 * 
 */
package com.esp.bean;

import java.io.Serializable;

import com.esp.model.image.Attribute;

/**
 * @author Jay
 *
 */
public class AttributeReporting extends BaseBean implements Serializable  {
    /**
	 * 
	 */
	private static final long serialVersionUID = 640510687469179829L;

	private Attribute attribute;
    
    private String hitCount;
    
    private String ignoreCount;
    
    private String attrCategory;

	public Attribute getAttribute() {
		return attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public String getHitCount() {
		return hitCount;
	}

	public void setHitCount(String hitCount) {
		this.hitCount = hitCount;
	}


	public String getIgnoreCount() {
		return ignoreCount;
	}

	public void setIgnoreCount(String ignoreCount) {
		this.ignoreCount = ignoreCount;
	}

	public String getAttrCategory() {
		return attrCategory;
	}

	public void setAttrCategory(String attrCategory) {
		this.attrCategory = attrCategory;
	}
}
