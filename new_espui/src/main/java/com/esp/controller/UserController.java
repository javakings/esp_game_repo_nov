/**
 * 
 */
package com.esp.controller;

import java.io.File;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.esp.constant.EspConstant;
import com.esp.form.ImageForm;
import com.esp.form.UserForm;
import com.esp.model.common.Message;
import com.esp.model.user.User;
import com.esp.service.ImageService;
import com.esp.service.UserService;

/**
 * @author Jay
 *
 */
@Controller
@RequestMapping("/user")
public class UserController {

	
	@Autowired
	private UserService userService ;
	
	@Autowired
	private ImageService service;
	
	@RequestMapping(value="/register",method = RequestMethod.GET)
    public ModelAndView getRegistration(ModelAndView model) {
    	 model.setViewName("Registration");
        return model;
    }
	
	
	    @RequestMapping(value="/home",method = RequestMethod.GET)
	    public ModelAndView getHome(ModelAndView model) {
	    	 model.setViewName("home");
	        return model;
	    }
	    
	    @RequestMapping(value = "/profile",method = RequestMethod.GET)
		public String displayProfile(Model model)
		{
			return "userProfile";
		}
	    

		@RequestMapping(value="/uploadUserImage", method = RequestMethod.POST)
		public ModelAndView uploadUserImage(ModelAndView model, ImageForm form, HttpServletRequest request )
		{
			model.setViewName("redirect:profile");
			MultipartFile file = form.getImageFile();
			
			if (null!=file && !file.isEmpty()) {
				try {
					String originalFileName =  new Date().getTime() + file.getOriginalFilename();
					String readFilePath = "/"+ EspConstant.DEFAULT_PATH  + originalFileName;
					String filePath = request.getServletContext().getRealPath(EspConstant.DEFAULT_PATH) +File.separator+ originalFileName; 
					boolean isSuccess =	service.upload(file, filePath);
					if(isSuccess)
					{
						userService.uploadImage(filePath, readFilePath, form.getUserId(),request);
						
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else 
			{
			
			}
			return model;
		}
	    
	
	@RequestMapping(method = RequestMethod.POST,value="/add")
	public ModelAndView registerUser( @ModelAttribute UserForm userForm,ModelAndView model)
	{
		try
		{   
			model.setViewName("index");
			
			User user = new User();
			user.setCity(userForm.getCity());
			user.setEmail(userForm.getEmail());
			user.setFirstName(userForm.getFirstName());
			user.setLastName(userForm.getLastName());
			userService.saveUser(user, userForm.getLoginId(), userForm.getPassword());
			model.addObject("successObj", "You have been Succefully Registered.Kindly login");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			model.addObject("successObj", "Something went wrong.Please try after some time");
		}
		
		return model;
	}
	
	
	@RequestMapping(value="/updateProfile", method = RequestMethod.POST)
	public @ResponseBody Message updateProfile(@RequestParam("fname")String fname,@RequestParam("lname")String lname,@RequestParam("city")String city,@RequestParam("email")String email,@RequestParam("id")String id,HttpServletRequest request)
	{
		Message msg=new Message();
		User user=userService.updateUser(fname, lname, city, email, id,request);
		msg.setMsgObj(user);
		msg.setStatus(true);
		return msg;
	}
	
	@RequestMapping(value="/getProfile", method = RequestMethod.POST)
	public @ResponseBody Message getProfile(@RequestParam("id")String id)
	{
		Message msg=new Message();
		User user=userService.getUserById(id);
		msg.setMsgObj(user);
		msg.setStatus(true);
		return msg;
	}
	
	@RequestMapping(value = "/{loginId}", method =RequestMethod.GET)
	public @ResponseBody Message checkAvailability(@PathVariable String loginId)
	{
		
		//UserService service = ctx.getBean(UserService.class);
		User user = userService.getUserByLoginId(loginId);
		Message msg = new Message();
		if(user != null)
		{
			msg.setStatus(false);
			msg.setMessage(loginId +" is unavailable");
		}
		else
		{
			msg.setStatus(true);
			msg.setMessage(loginId +" available");	
		}
		return msg;
	}
	 

}
