package com.esp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.esp.bean.UserBean;
import com.esp.constant.EspConstant;
import com.esp.model.auth.Authentication;
import com.esp.model.type.USERTYPE;
import com.esp.model.user.User;
import com.esp.service.AttributeCategoryService;
import com.esp.service.ImageCategoryService;
import com.esp.service.UserService;

@Controller
@RequestMapping("/authenticate")
public class AuthenticationController {

	@Autowired
	private UserService service;
	
	@Autowired
	private ImageCategoryService imageCatService;
	
	@Autowired
	private AttributeCategoryService attrCatService;

	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView  authenticate(ModelAndView view, HttpServletRequest request, 
			@RequestParam("loginId") String loginId, @RequestParam("password") String password)
	{
		try{
		Authentication auth = service.authenticate(loginId, password);
		if(null != auth)
		{
			User user = auth.getUser();
			HttpSession session = request.getSession();
			UserBean userBean = new UserBean();
			userBean.setCity(user.getCity());
			userBean.setFirstName(user.getFirstName());
			userBean.setEmail(user.getEmail());
			userBean.setLastName(user.getLastName());
			userBean.setLoginID(auth.getLoginID());
			userBean.setUserId(user.getId());
			userBean.setUserType(user.getType());
			userBean.setReadImagePath(request.getContextPath()+user.getUserReadFilePath());
			session.setAttribute(EspConstant.USER, userBean);
			if(imageCatService.findAllCategory()==null || imageCatService.findAllCategory().size()==0){
				imageCatService.addCategory("FLOWER");
			
			}
			
			if(attrCatService.findAllCategory()==null || attrCatService.findAllCategory().size()==0){
				attrCatService.addCategory("COLOR");
				attrCatService.addCategory(EspConstant.TUNE);
			}
			
			if(user.getType().equals(USERTYPE.ADMIN)){
			view.setViewName("redirect:/admin/home");
			}else{
				view.setViewName("redirect:/user/home");
			}
		}
		else
		{
			view.getModel().put("error", "Invalid LoginId or Password");
			view.setViewName("redirect:/"); 
		}
		
		}catch(Exception e){
			view.getModel().put("error", "Invalid LoginId or Password");
			view.setViewName("redirect:/"); 
		}
		return view;
	}

}
