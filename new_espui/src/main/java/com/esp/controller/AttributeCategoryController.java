/**
 * 
 */
package com.esp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.esp.model.common.Message;
import com.esp.model.image.AttributeCategory;
import com.esp.model.image.ImageCategory;
import com.esp.service.AttributeCategoryService;

/**
 * @author Jay
 *
 */
@Controller
@RequestMapping(value="/attributeCategory")
public class AttributeCategoryController {
	
	@Autowired
	private AttributeCategoryService service;
	
	@RequestMapping(value = "/view",method = RequestMethod.GET)
	public String displayUploadImageForm(Model model)
	{
		List<AttributeCategory>attrCategoryList=service.findAllCategory();
		model.addAttribute("attrcatlist", attrCategoryList);
		return "AttributeCategory";
	}
 
 
 @RequestMapping(value="/edit", method = RequestMethod.POST)
	public @ResponseBody AttributeCategory editAttribute(@RequestParam("catId")String catId)
	{
		return service.findCategoryById(catId);
		 
	}
 
 @RequestMapping(value="/update", method = RequestMethod.POST)
	public @ResponseBody Message updateCategory(@RequestParam("catId")String catId,@RequestParam("catName")String catName)
	{
	    Message msg=new Message();
		service.updateCategory(catId, catName);
		msg.setStatus(true);
		return msg;
		 
	}

	@RequestMapping(value="/add", method = RequestMethod.POST)
	public @ResponseBody Message addAttribute(@RequestParam("catName")String catName)
	{
		
		Message msg=new Message();
		service.addCategory(catName);
		msg.setStatus(true);
		return msg;
	}

}
