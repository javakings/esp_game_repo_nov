/**
 * 
 */
package com.esp.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.FailedLoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.esp.bean.AttributeReporting;
import com.esp.bean.GameReportingBean;
import com.esp.bean.UserBean;
import com.esp.constant.EspConstant;
import com.esp.model.game.Game;
import com.esp.model.game.GameAttributeSummary;
import com.esp.model.image.Attribute;
import com.esp.model.image.Image;
import com.esp.model.type.ATTRIBUTETYPE;
import com.esp.model.type.FILETYPE;
import com.esp.model.type.GAMEMODE;
import com.esp.model.user.User;
import com.esp.service.AttributeCategoryService;
import com.esp.service.AttributeService;
import com.esp.service.GameService;
import com.esp.service.ImageService;
import com.esp.service.SettingsService;
import com.esp.service.UserService;
import com.esp.utility.ExcelUtility;

/**
 * @author Jay
 *
 */
@Controller
@RequestMapping(value = "/gameSummary")
public class GameReportingController {
	@Autowired
	private ImageService imageService;

	@Autowired
	private AttributeService attributeService;

	@Autowired
	private UserService userService;

	@Autowired
	private SettingsService settingsService;

	@Autowired
	private GameService gameService;

	@Autowired
	private AttributeCategoryService attrCatService;

	@RequestMapping(value = "/reporting", method = RequestMethod.GET)
	public String getReportingAll(Model model, HttpServletRequest request) {
		model.addAttribute("reportingList", getReporting(request,FILETYPE.IMAGE));
		return "Reporting";
	}
	
	@RequestMapping(value = "/attributeReporting", method = RequestMethod.GET)
	public String getAttributeReportingAll(Model model,HttpServletRequest request) {
		model.addAttribute("attributeReportingList", getAttributeReporting(request,FILETYPE.IMAGE));
		return "AttributeReporting";
	}

	@RequestMapping(value = "/tuneAttributeReporting", method = RequestMethod.GET)
	public String getMusicAttributeReportingAll(Model model,HttpServletRequest request) {
		model.addAttribute("attributeReportingList", getAttributeReporting(request,FILETYPE.MUSIC));
		return "TuneAttributeReporting";
	}
	
	@RequestMapping(value = "/tuneReporting", method = RequestMethod.GET)
	public String getMusicReportingAll(Model model, HttpServletRequest request) {
		model.addAttribute("reportingList", getReporting(request,FILETYPE.MUSIC));
		return "TuneReporting";
	}

	

	private List<GameReportingBean> getAttributeReporting(HttpServletRequest request,FILETYPE fileType) {
		List<GameReportingBean> gameReportingBeanList = new ArrayList<GameReportingBean>();
		List<Image> imageList = imageService.findAllImages(fileType);
		if (imageList != null && imageList.size() > 0) {
			Map<Integer, List<String>> data = new HashMap<Integer, List<String>>();
			if(fileType!=null && fileType.equals(FILETYPE.IMAGE)){
			data.put(
					1,
					Arrays.asList(new String[] { "Image", "Attribute", "Category",
							"Hit Count", "Ignore Count"}));
			}else{
				data.put(
						1,
						Arrays.asList(new String[] { "Tune", "Attribute", "Category",
								"Hit Count", "Ignore Count"}));	
			}
			int count = 2;
			for (Image image : imageList) {
				GameReportingBean bean = new GameReportingBean();
				bean.setImage(image);
				List<String> dataForExcel = new ArrayList<String>();
				dataForExcel.add(image.getImageName());
				List<AttributeReporting> correctHitList = new ArrayList<AttributeReporting>();
				List<AttributeReporting> defaultCorrectHitList = new ArrayList<AttributeReporting>();
				List<Attribute> attrList = image.getAttributeList();
				if (attrList != null && attrList.size() > 0) {
					for (Attribute attr : attrList) {
						if(!attr.getType().equals(ATTRIBUTETYPE.IRRELEVANT)){
						AttributeReporting attrReporting = new AttributeReporting();
						attrReporting.setAttribute(attr);
						if (attr.getAttrCategory() != null) {
							attrReporting.setAttrCategory(attr.getAttrCategory().getCategoryName());
						} else {
							attrReporting.setAttrCategory("DEFAULT");
						}
						attrReporting.setHitCount("" + attr.getAnsweredBy().size());
						attrReporting.setIgnoreCount("" + attr.getIgnoredBy().size());
						if (attr.getAttrCategory() == null && attr.getType() == ATTRIBUTETYPE.DEFAULT) {
							defaultCorrectHitList.add(attrReporting);
						} else {
							correctHitList.add(attrReporting);
						}
						}
					}
				}
				bean.setCorrectHitCountList(correctHitList);
				bean.setDefaultCorrectCountList(defaultCorrectHitList);
			
				for(AttributeReporting reporting:defaultCorrectHitList){
					String attrData="";
					attrData+=reporting.getAttribute().getAttrName()+"@";
					attrData+=reporting.getAttrCategory()+"@";
					attrData+=reporting.getHitCount()+"@";
					attrData+=reporting.getIgnoreCount()+"@";
					dataForExcel.add(attrData);
				}
				
				for(AttributeReporting reporting:correctHitList){
					String attrData="";
					attrData+=reporting.getAttribute().getAttrName()+"@";
					attrData+=reporting.getAttrCategory()+"@";
					attrData+=reporting.getHitCount()+"@";
					attrData+="-"+"@";
					dataForExcel.add(attrData);
				}
				
				data.put(count, dataForExcel);
				gameReportingBeanList.add(bean);
				count++;
			}
			GameReportingBean bean = new GameReportingBean();
			String readFilePath = ExcelUtility.writeToExcelAttributeReporting(data, request);
			bean.setExcelDownloadPath(readFilePath);
			gameReportingBeanList.add(bean);
		}
		
	
		return gameReportingBeanList;

	}

	private List<GameReportingBean> getReporting(HttpServletRequest request,FILETYPE fileType) {
		HttpSession session = request.getSession(false);

		List<GameReportingBean> gameReportingBeanList = new ArrayList<GameReportingBean>();
		if (session != null) {
			UserBean userBean = (UserBean) session.getAttribute(EspConstant.USER);
			if (userBean != null) {
				Map<Integer, List<String>> data = new HashMap<Integer, List<String>>();
				if(fileType!=null && fileType.equals(FILETYPE.IMAGE)){
				data.put(1,Arrays.asList(new String[] { "Image Name", "Wrong Hit Count", "Play Count Assisted",
								"Play Count Unassisted", "Time Taken Assisted"," "," ","Time Taken Unassisted" }));
				}else{
					data.put(1,Arrays.asList(new String[] { "Tune Name", "Wrong Hit Count", "Play Count Assisted",
							"Play Count Unassisted", "Time Taken Assisted"," "," ","Time Taken Unassisted" }));
				}
				data.put(
						2,
						Arrays.asList(new String[] { " ", " ", " ", " ", "Max","Min","Average",
								"Max","Min","Average" }));
				User user = userService.getUserById(userBean.getUserId());
				List<Game> gameList = gameService.findAllGames();
				if (gameList != null && gameList.size() > 0) {
					int asssistedGameCountPlayed = 0;
					int unAsssistedGameCountPlayed = 0;
					int count = 3;
					List<Image> imageList = imageService.findAllImages(fileType);
					if (imageList != null && imageList.size() > 0) {
						for (Image image : imageList) {
							GameReportingBean bean = new GameReportingBean();
							List<String> dataForExcel = new ArrayList<String>();
							dataForExcel.add(image.getImageName());
							bean.setImage(image);
							int wrongAnswersCount = 0;
							int playersCountImageAssisted = 0;
							int playersCountImageNonAssisted = 0;
							int assistedAvgTime = 0;
							List<Integer> assistedTimeList = new ArrayList<Integer>();
							List<Integer> nonAssistedTimeList = new ArrayList<Integer>();

							int nonAssistedAvgTime = 0;
							for (Game game : gameList) {

								List<GameAttributeSummary> assistedAttrSummarylist = gameService
										.getGameAttributeSummaryList(game.getId(), image.getId());
								if(fileType.equals(FILETYPE.IMAGE)){
								if (game.getGameMode().equals(GAMEMODE.ASSISTED)) {
									if (game.getPlayer1().getId().equals(user.getId())) {
										asssistedGameCountPlayed++;
									}
									if (assistedAttrSummarylist != null && assistedAttrSummarylist.size() > 0) {
										playersCountImageAssisted += assistedAttrSummarylist.size();
										for (GameAttributeSummary summary : assistedAttrSummarylist) {
											wrongAnswersCount += summary.getPlayerAttributeForWrongAnswers().size();
											if (summary.getPlayedTime() != null) {
												assistedAvgTime += summary.getPlayedTime();
												assistedTimeList.add(summary.getPlayedTime());
											}

										}

									}
								} else if(game.getGameMode().equals(GAMEMODE.NON_ASSISTED)) {
									if (game.getPlayer1().getId().equals(user.getId())) {
										unAsssistedGameCountPlayed++;
									}
									List<GameAttributeSummary> nonAssistedAttrSummarylist = gameService
											.getGameAttributeSummaryList(game.getId(), image.getId());
									if (nonAssistedAttrSummarylist != null && nonAssistedAttrSummarylist.size() > 0) {
										playersCountImageNonAssisted += nonAssistedAttrSummarylist.size();
										for (GameAttributeSummary summary : nonAssistedAttrSummarylist) {
											if (summary.getPlayedTime() != null) {
												nonAssistedAvgTime += summary.getPlayedTime();
												nonAssistedTimeList.add(summary.getPlayedTime());
											}

										}
									}
								}
								}else{
									if (game.getGameMode().equals(GAMEMODE.ASSISTED_MUSIC)) {
										if (game.getPlayer1().getId().equals(user.getId())) {
											asssistedGameCountPlayed++;
										}
										if (assistedAttrSummarylist != null && assistedAttrSummarylist.size() > 0) {
											playersCountImageAssisted += assistedAttrSummarylist.size();
											for (GameAttributeSummary summary : assistedAttrSummarylist) {
												wrongAnswersCount += summary.getPlayerAttributeForWrongAnswers().size();
												if (summary.getPlayedTime() != null) {
													assistedAvgTime += summary.getPlayedTime();
													assistedTimeList.add(summary.getPlayedTime());
												}

											}

										}
									} else if(game.getGameMode().equals(GAMEMODE.NON_ASSISTED_MUSIC)) {
										if (game.getPlayer1().getId().equals(user.getId())) {
											unAsssistedGameCountPlayed++;
										}
										List<GameAttributeSummary> nonAssistedAttrSummarylist = gameService
												.getGameAttributeSummaryList(game.getId(), image.getId());
										if (nonAssistedAttrSummarylist != null && nonAssistedAttrSummarylist.size() > 0) {
											playersCountImageNonAssisted += nonAssistedAttrSummarylist.size();
											for (GameAttributeSummary summary : nonAssistedAttrSummarylist) {
												if (summary.getPlayedTime() != null) {
													nonAssistedAvgTime += summary.getPlayedTime();
													nonAssistedTimeList.add(summary.getPlayedTime());
												}

											}
										}
									}
								}

							}

							bean.setWrongHitCoun("" + wrongAnswersCount);
							bean.setPlayerNumberForImageAssisted("" + playersCountImageAssisted);
							bean.setPlayerNumberForImageNonAssisted("" + playersCountImageNonAssisted);
							dataForExcel.add("" + wrongAnswersCount);
							dataForExcel.add("" + playersCountImageAssisted);
							dataForExcel.add("" + playersCountImageNonAssisted);
							if (assistedTimeList.size() > 0) {
								bean.setMinTimeAttemptAssisted("" + Collections.min(assistedTimeList));
								bean.setMaxTimeAttemptAssisted("" + Collections.max(assistedTimeList));
								bean.setAverageTimeAttemptAssisted("" + assistedAvgTime / assistedTimeList.size());
								dataForExcel.add("" +Collections.min(assistedTimeList));
								dataForExcel.add(""+Collections.max(assistedTimeList));
								dataForExcel.add(""+ (assistedAvgTime/ assistedTimeList.size()));

							} else {
								dataForExcel.add("--");
								dataForExcel.add("NA");
								dataForExcel.add("--");
							}
							if (nonAssistedTimeList.size() > 0) {
								bean.setMinTimeAttemptNonAssisted("" + Collections.min(nonAssistedTimeList));
								bean.setMaxTimeAttemptNonAssisted("" + Collections.max(nonAssistedTimeList));
								bean.setAverageTimeAttemptNonAssisted("" + nonAssistedAvgTime
										/ nonAssistedTimeList.size());
								dataForExcel.add("" + Collections.min(nonAssistedTimeList));
								dataForExcel.add("" + Collections.max(nonAssistedTimeList));
								dataForExcel.add("" + (nonAssistedAvgTime/nonAssistedTimeList.size()));
							} else {
								dataForExcel.add("--");
								dataForExcel.add("NA");
								dataForExcel.add("--");
							}
							data.put(count, dataForExcel);
							count++;
							gameReportingBeanList.add(bean);
						}

					}

					GameReportingBean bean = new GameReportingBean();
					int asGamecnt = 0;
					int nonAsGamecnt = 0;
					for (Game game : gameList) {
						if(fileType.equals(FILETYPE.IMAGE)){
						if (game.getGameMode() == GAMEMODE.ASSISTED) {
							asGamecnt++;
						} else if(game.getGameMode() == GAMEMODE.NON_ASSISTED) {
							nonAsGamecnt++;
						}
						}else{
							if (game.getGameMode() == GAMEMODE.ASSISTED_MUSIC) {
								asGamecnt++;
							} else if(game.getGameMode() == GAMEMODE.NON_ASSISTED_MUSIC) {
								nonAsGamecnt++;
							}
						}
					}
					bean.setAssistedGameCount(String.valueOf(asGamecnt));
					bean.setNonAssistedGameCount(String.valueOf(nonAsGamecnt));
					bean.setAssistedGamePlayedByUser("" + asssistedGameCountPlayed);

					bean.setUnassistedGamePlayedByUser("" + unAsssistedGameCountPlayed);
					String readFilePath = ExcelUtility.writeToExcel(data, request);
					bean.setExcelDownloadPath(readFilePath);
					gameReportingBeanList.add(bean);
				}
			}
		}

		return gameReportingBeanList;

	}

	private List<Attribute> getDefaultAttributeList(List<Attribute> attrList) {
		List<Attribute> defaultAttrList = new ArrayList<Attribute>();
		if (attrList != null && attrList.size() > 0) {
			for (Attribute attr : attrList) {
				if (attr.getType().equals(ATTRIBUTETYPE.DEFAULT)) {
					defaultAttrList.add(attr);
				}
			}
		}
		return defaultAttrList;
	}

	private List<Attribute> getUniqueAttributeList(List<Attribute> attrList) {
		List<Attribute> uniqueAttrList = new ArrayList<Attribute>();
		if (attrList != null && attrList.size() > 0) {
			for (Attribute attr : attrList) {
				boolean isFound = false;
				for (Attribute attr1 : uniqueAttrList) {
					if (attr.getAttrName().equalsIgnoreCase(attr1.getAttrName())) {
						isFound = true;
						break;
					}
				}
				if (!isFound) {
					uniqueAttrList.add(attr);
				}

			}
		}
		return uniqueAttrList;
	}
}
