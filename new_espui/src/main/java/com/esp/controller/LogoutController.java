package com.esp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.esp.constant.EspConstant;

@Controller
@RequestMapping(value = "/logout")
public class LogoutController {

	  @RequestMapping(value="/user",method = RequestMethod.GET)
	    public ModelAndView playGame(ModelAndView model,HttpServletRequest request) {
		  HttpSession session = request.getSession(false);
			
			if (session != null) {
				session.removeAttribute(EspConstant.USER);
				session.invalidate();
			}
		    model.setViewName("redirect:/");
	        return model;
	    }
}
