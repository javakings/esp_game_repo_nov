package com.esp.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.esp.bean.ImageBean;
import com.esp.bean.UserBean;
import com.esp.constant.EspConstant;
import com.esp.model.common.Message;
import com.esp.model.game.GameAttributeSummary;
import com.esp.model.gamesettings.Settings;
import com.esp.model.image.Attribute;
import com.esp.model.image.AttributeCategory;
import com.esp.model.image.Image;
import com.esp.model.type.ATTRIBUTEFORTYPE;
import com.esp.model.type.ATTRIBUTETYPE;
import com.esp.model.type.FILETYPE;
import com.esp.model.type.STATUS;
import com.esp.model.user.User;
import com.esp.requestbean.SubmitAnswerRequest;
import com.esp.requestbean.SubmitNonAssistedAnswerRequest;
import com.esp.service.AttributeCategoryService;
import com.esp.service.AttributeService;
import com.esp.service.GameService;
import com.esp.service.ImageService;
import com.esp.service.SettingsService;
import com.esp.service.UserService;
import com.esp.utility.CommonUtility;

@Controller
@RequestMapping(value = "/singlePlayer")
public class SinglePlayerController {

	@Autowired
	private ImageService imageService;

	@Autowired
	private AttributeService attributeService;

	@Autowired
	private UserService userService;

	@Autowired
	private SettingsService settingsService;

	@Autowired
	private GameService gameService;

	@Autowired
	private AttributeCategoryService attrCatService;

	@RequestMapping(value="/playGame",method = RequestMethod.GET)
	public ModelAndView playGame(ModelAndView model) {
		model.setViewName("SinglePlayer");
		return model;
	}

	@RequestMapping(value="/playGameNonAssisted",method = RequestMethod.GET)
	public ModelAndView playGameNonAssisted(ModelAndView model) {
		model.setViewName("NonAssistedGame");
		return model;
	}
	
	@RequestMapping(value="/playGameMusic",method = RequestMethod.GET)
	public ModelAndView playGameMusic(ModelAndView model) {
		model.setViewName("SinglePlayerMusic");
		return model;
	}

	@RequestMapping(value="/playGameNonAssistedMusic",method = RequestMethod.GET)
	public ModelAndView playGameNonAssistedMusic(ModelAndView model) {
		model.setViewName("NonAssistedGameMusic");
		return model;
	}
	@RequestMapping(value="/searchAttribute",method = RequestMethod.GET)
	public ModelAndView searchAttribute(ModelAndView model) {
		model.setViewName("search");
		return model;
	}
	@RequestMapping(value="/searchTune",method = RequestMethod.GET)
	public ModelAndView searchTune(ModelAndView model) {
		model.setViewName("searchTune");
		return model;
	}

	@RequestMapping(value = "/loadImagesForGame", method = RequestMethod.GET)
	public @ResponseBody Message loadImagesForGame(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		Message msg=new Message();
		if (session != null) {
			UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
			if(userBean!=null){
				User user=userService.getUserById(userBean.getUserId());
				
				List<Image> imageList  = imageService.getImageForGame(FILETYPE.IMAGE,user);
				session.setAttribute(EspConstant.SINGLE_PLAYER_IMAGES,
						imageList);
				
				List<Settings>settingsList=settingsService.findSettings();
				if(settingsList==null || settingsList.size()==0){
					settingsService.addSettings(new Date(), 10);
				}
				Map<String, List<String>>imageAnswerMap=(Map<String, List<String>>)session.getAttribute(EspConstant.IMAGE_ANSWERS);
				if(imageAnswerMap!=null && imageAnswerMap.size()>0){
					session.removeAttribute(EspConstant.IMAGE_ANSWERS);
				}
				
                 Map<String, String>imageTimeMap=(Map<String, String>)session.getAttribute(EspConstant.IMAGE_TIME_ASSISTED);
				
				if(imageTimeMap!=null && imageTimeMap.size()>0){
					session.removeAttribute(EspConstant.IMAGE_TIME_ASSISTED);
				}
				
				Map<String, List<String>>imageAnswerMapNonAssisted=(Map<String, List<String>>)session.getAttribute(EspConstant.NON_ASSISTED_IMAGES_ANSWERS);
				if(imageAnswerMapNonAssisted!=null && imageAnswerMapNonAssisted.size()>0){
					session.removeAttribute(EspConstant.NON_ASSISTED_IMAGES_ANSWERS);
				}
				
                 Map<String, String>imageTimeMapNonAssisted=(Map<String, String>)session.getAttribute(EspConstant.IMAGE_TIME_NON_ASSISTED);
				
				if(imageTimeMapNonAssisted!=null && imageTimeMapNonAssisted.size()>0){
					session.removeAttribute(EspConstant.IMAGE_TIME_NON_ASSISTED);
				}
				
				Map<String, List<Attribute>>imageOptionMap=(Map<String, List<Attribute>>)session.getAttribute(EspConstant.IMAGE_ANSWERS_OPTIONS);
				if(imageOptionMap!=null && imageOptionMap.size()>0){
					//session.removeAttribute(EspConstant.IMAGE_ANSWERS_OPTIONS);
				}
				msg.setMessage(String.valueOf(imageList.size()));

			}    

		}

		return msg;

	}
	
	
	@RequestMapping(value = "/loadTunesForGame", method = RequestMethod.GET)
	public @ResponseBody Message loadTunesForGame(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		Message msg=new Message();
		if (session != null) {
			UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
			if(userBean!=null){
				User user=userService.getUserById(userBean.getUserId());
				List<Image> imageList  = imageService.getImageForGame(FILETYPE.MUSIC,user);
				session.setAttribute(EspConstant.SINGLE_PLAYER_IMAGES,
						imageList);
				
				List<Settings>settingsList=settingsService.findSettings();
				if(settingsList==null || settingsList.size()==0){
					settingsService.addSettings(new Date(), 10);
				}
				
				Map<String, List<String>>imageAnswerMap=(Map<String, List<String>>)session.getAttribute(EspConstant.IMAGE_ANSWERS);
				if(imageAnswerMap!=null && imageAnswerMap.size()>0){
					session.removeAttribute(EspConstant.IMAGE_ANSWERS);
				}
				
                 Map<String, String>imageTimeMap=(Map<String, String>)session.getAttribute(EspConstant.IMAGE_TIME_ASSISTED);
				
				if(imageTimeMap!=null && imageTimeMap.size()>0){
					session.removeAttribute(EspConstant.IMAGE_TIME_ASSISTED);
				}
				
				
				Map<String, List<String>>imageAnswerMapNonAssisted=(Map<String, List<String>>)session.getAttribute(EspConstant.NON_ASSISTED_IMAGES_ANSWERS);
				if(imageAnswerMapNonAssisted!=null && imageAnswerMapNonAssisted.size()>0){
					session.removeAttribute(EspConstant.NON_ASSISTED_IMAGES_ANSWERS);
				}
				
                 Map<String, String>imageTimeMapNonAssisted=(Map<String, String>)session.getAttribute(EspConstant.IMAGE_TIME_NON_ASSISTED);
				
				if(imageTimeMapNonAssisted!=null && imageTimeMapNonAssisted.size()>0){
					session.removeAttribute(EspConstant.IMAGE_TIME_NON_ASSISTED);
				}
				Map<String, List<Attribute>>imageOptionMap=(Map<String, List<Attribute>>)session.getAttribute(EspConstant.IMAGE_ANSWERS_OPTIONS);
				if(imageOptionMap!=null && imageOptionMap.size()>0){
					session.removeAttribute(EspConstant.IMAGE_ANSWERS_OPTIONS);
				}
				msg.setMessage(String.valueOf(imageList.size()));

			}    

		}

		return msg;

	}


	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/submitAnswers", method = RequestMethod.POST,consumes="application/json")
	public @ResponseBody Message submitAnswers(HttpServletRequest request,@RequestBody SubmitAnswerRequest answerReq) {
		HttpSession session = request.getSession(false);

		Message msg=new Message();
		if (session != null) {
			UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
			if(userBean!=null){
				User user=userService.getUserById(userBean.getUserId());
				Map<String, List<String>>imageAnswerMap=(Map<String, List<String>>)session.getAttribute(EspConstant.IMAGE_ANSWERS);
				if(imageAnswerMap==null){
					imageAnswerMap=new HashMap<String, List<String>>();
				}
				imageAnswerMap.put(answerReq.getImageId(), answerReq.getAnswers());
				imageService.updateImageSeenBy(answerReq.getImageId(), user);
				session.setAttribute(EspConstant.IMAGE_ANSWERS, imageAnswerMap);
				Map<String, String>imageTimeMap=(Map<String, String>)session.getAttribute(EspConstant.IMAGE_TIME_ASSISTED);
				
				if(imageTimeMap==null){
					imageTimeMap=new HashMap<String, String>();
				}
				imageTimeMap.put(answerReq.getImageId(),answerReq.getTimeTaken());
				session.setAttribute(EspConstant.IMAGE_TIME_ASSISTED, imageTimeMap);
			}
		}
		return msg;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/submitNonAssistedAnswers", method = RequestMethod.POST,consumes="application/json")
	public @ResponseBody Message submitNonAssistedAnswers(HttpServletRequest request,@RequestBody SubmitNonAssistedAnswerRequest answerReq) {
		HttpSession session = request.getSession(false);

		Message msg=new Message();
		if (session != null) {
			UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
			List<Attribute>finalList=new ArrayList<Attribute>();
			if(userBean!=null){
				User user=userService.getUserById(userBean.getUserId());
				if(answerReq!=null){
					Map<String, String>reqMap=answerReq.getAnswers();
					for(Entry<String, String>entry:reqMap.entrySet()){
						AttributeCategory attrCat=attrCatService.findCategoryById(entry.getKey());
						if(attrCat!=null){
							List<Attribute>attrList=imageService.addAttribute(entry.getValue(), answerReq.getImageId(),attrCat,user);
							finalList.addAll(attrList);
						}
					}

					Map<String, List<Attribute>>imageAnswerMap=(Map<String, List<Attribute>>)session.getAttribute(EspConstant.NON_ASSISTED_IMAGES_ANSWERS);
					if(imageAnswerMap==null){
						imageAnswerMap=new HashMap<String, List<Attribute>>();
					}
					imageAnswerMap.put(answerReq.getImageId(), finalList);
					imageService.updateImageSeenBy(answerReq.getImageId(), user);
					session.setAttribute(EspConstant.NON_ASSISTED_IMAGES_ANSWERS, imageAnswerMap);
					
					Map<String, String>imageTimeMap=(Map<String, String>)session.getAttribute(EspConstant.IMAGE_TIME_NON_ASSISTED);
					
					if(imageTimeMap==null){
						imageTimeMap=new HashMap<String, String>();
					}
					imageTimeMap.put(answerReq.getImageId(),answerReq.getTimeTaken());
					session.setAttribute(EspConstant.IMAGE_TIME_NON_ASSISTED, imageTimeMap);
				}
			}
		}
		return msg;

	}



	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getImage", method = RequestMethod.POST)
	public @ResponseBody ImageBean getImageForGame(HttpServletRequest request,
			@RequestParam("imageNo") String imageNo) {
		HttpSession session = request.getSession(false);
		int imageNum = Integer.parseInt(imageNo);
		List<Image> imageList = null;
		Image image = null;
		User  user=null;
		ImageBean imagebean=new ImageBean();
		if (session != null) {
			UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
			if(userBean!=null){
				user=userService.getUserById(userBean.getUserId());

				imageList = (List<Image>) session
						.getAttribute(EspConstant.SINGLE_PLAYER_IMAGES);
				if (imageList != null) {
					if (imageNum == 0) {
						image = imageList.get(0);
					} else {
						if (imageNum < imageList.size()) {
							image = imageList.get(imageNum);

						} else {
							session.removeAttribute(EspConstant.SINGLE_PLAYER_IMAGES);

						}
					}
					
					

					if(image!=null){
						imagebean.setImageId(image.getId());
						imagebean.setImageName(image.getImageName());
						imagebean.setImagesToShowNo(String.valueOf(imageList.size()));
						imagebean.setReadFilePath(request.getContextPath()+"/"+image.getReadFilePath());
						if(user!=null){
							List<Attribute>defaultList=getDefaultAttributeList(image.getAttributeList());
							imagebean.setAttributeList(getAnswers(defaultList, user,image.getFileType()));
						}
						Map<String, List<Attribute>>imageOptionMap=(Map<String, List<Attribute>>)session.getAttribute(EspConstant.IMAGE_ANSWERS_OPTIONS);
						if(imageOptionMap==null){
							imageOptionMap=new HashMap<String, List<Attribute>>();
						}
						
						imageOptionMap.put(image.getId(), new ArrayList<Attribute>(imagebean.getAttributeList()));
						session.setAttribute(EspConstant.IMAGE_ANSWERS_OPTIONS, imageOptionMap);

					}else{
						imagebean.setStatus(STATUS.FAILURE);

					}
				}
			}
		}



		return imagebean;

	}


	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getImageNonAssisted", method = RequestMethod.POST)
	public @ResponseBody ImageBean getImageForNonAssistedGame(HttpServletRequest request,
			@RequestParam("imageNo") String imageNo) {
		HttpSession session = request.getSession(false);
		int imageNum = Integer.parseInt(imageNo);
		List<Image> imageList = null;
		Image image = null;
		User  user=null;
		ImageBean imagebean=new ImageBean();
		if (session != null) {
			UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
			if(userBean!=null){
				user=userService.getUserById(userBean.getUserId());

				imageList = (List<Image>) session
						.getAttribute(EspConstant.SINGLE_PLAYER_IMAGES);
				if (imageList != null) {
					if (imageNum == 0) {
						image = imageList.get(0);
					} else {
						if (imageNum < imageList.size()) {
							image = imageList.get(imageNum);

						} else {
							session.removeAttribute(EspConstant.SINGLE_PLAYER_IMAGES);

						}
					}

					if(image!=null){
						imagebean.setImageId(image.getId());
						imagebean.setImageName(image.getImageName());
						imagebean.setImagesToShowNo(String.valueOf(imageList.size()));
						imagebean.setReadFilePath(request.getContextPath()+"/"+image.getReadFilePath());
						if(user!=null){
							List<AttributeCategory>attCatList= attrCatService.findAllCategory();
							List<AttributeCategory>finalAttrCatList=new ArrayList<AttributeCategory>();
							if(image.getFileType().equals(FILETYPE.IMAGE)){
								 for(AttributeCategory attr:attCatList){
									if(!attr.getCategoryName().equals(EspConstant.TUNE)){
										finalAttrCatList.add(attr);
									}
								}
							}else{
								 for(AttributeCategory attr:attCatList){
										if(attr.getCategoryName().equals(EspConstant.TUNE)){
											finalAttrCatList.add(attr);
											break;
										}
									}
							}
							
							imagebean.setAttrCategoryList(finalAttrCatList);
							
						}

					}else{
						imagebean.setStatus(STATUS.FAILURE);

					}
				}
			}
		}



		return imagebean;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/evaluateGame", method = RequestMethod.POST)
	public @ResponseBody Message evaluateGame(HttpServletRequest request,@RequestParam("fileType") String fileType) {
		HttpSession session = request.getSession(false);
		Message message=new Message();
		if (session != null) {
			UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
			if(userBean!=null){
				List<Settings>settingsList=settingsService.findSettings();
				int correctAnswers=0;
				if(settingsList!=null && settingsList.size()>0){
					Settings settings=settingsList.get(0);

					if(userBean!=null){
						User user=userService.getUserById(userBean.getUserId());
						List<GameAttributeSummary>gameAttributeSummaryList=new ArrayList<GameAttributeSummary>();
						if(user!=null){
							Map<String, String>imageTimeMap=(Map<String, String>)session.getAttribute(EspConstant.IMAGE_TIME_ASSISTED);
							Map<String, List<String>>imageAnswerMap=(Map<String, List<String>>)session.getAttribute(EspConstant.IMAGE_ANSWERS);
							Map<String, List<Attribute>>imageOptionsMap=(Map<String, List<Attribute>>)session.getAttribute(EspConstant.IMAGE_ANSWERS_OPTIONS);
							if(imageAnswerMap!=null && imageAnswerMap.size()>0){
								Set<Entry<String, List<String>>>entrySet=imageAnswerMap.entrySet();
								for(Entry<String,  List<String>> entry:entrySet){
									Image image=imageService.findImageById(entry.getKey());
									
									GameAttributeSummary attrSummary=new GameAttributeSummary();
									attrSummary.setImageId(image.getId());
									if(imageTimeMap!=null && imageTimeMap.size()>0){
										attrSummary.setPlayedTime(Integer.valueOf(imageTimeMap.get(entry.getKey())));
									}
									List<Attribute>originalAnswersList=getDefaultAttributeList(image.getAttributeList());
									List<Attribute>correctOptionsList=new ArrayList<Attribute>();
									if(imageOptionsMap!=null && imageOptionsMap.size()>0){
										Set<Entry<String, List<Attribute>>>entrySet1=imageOptionsMap.entrySet();
										for(Entry<String,  List<Attribute>> entry1:entrySet1){
											if(image.getId().equals(entry1.getKey())){
												List<Attribute>optionsList=entry1.getValue();
											    for(Attribute attr:optionsList){
											    	if(originalAnswersList.contains(attr)){
											    		correctOptionsList.add(attr);
											    	}
											    }
											}
										}
										
									}
									List<String>userAnswersList=entry.getValue();
									for(String attrId:userAnswersList){
										Attribute attr=attributeService.getAttributeById(attrId);
										if(attr!=null){
										if(originalAnswersList.contains(attr)){
												attrSummary.getPlayerAttributeForRightAnswers().add(attrId);
												imageService.updateImageAttributeAnsweredBy(attrId, user, entry.getKey());
												correctAnswers++;
											}else{
												imageService.updateImageAttributeWrongAnsweredBy(attrId, user, entry.getKey());
												attrSummary.getPlayerAttributeForWrongAnswers().add(attrId);
											}
										}
									}
									
									for(Attribute optionAttr:correctOptionsList){
										if(!userAnswersList.contains(optionAttr.getId())){
											attrSummary.getPlayerAttributeForNoAnswers().add(optionAttr.getId());
											imageService.updateImageAttributeIgnoredBy(optionAttr.getId(), user, entry.getKey());
										}
									}
									
									gameAttributeSummaryList.add(attrSummary);

								}
								gameService.saveNewSinglePlayerGame(settings.getScorePerAttributeMatched()*correctAnswers, user.getId(),gameAttributeSummaryList,FILETYPE.valueOf(fileType));

							}
						}

						message.setMessage("Hi! "+user.getFirstName()+". You have scored "+String.valueOf(settings.getScorePerAttributeMatched()*correctAnswers)+" points.");
					}

				}
			}
		}


		return message;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/evaluateNonAssistedGame", method = RequestMethod.POST)
	public @ResponseBody Message evaluateNonAssistedGame(HttpServletRequest request,@RequestParam("fileType") String fileType) {
		HttpSession session = request.getSession(false);
		Message message=new Message();
		if (session != null) {
			UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
			if(userBean!=null){
				List<Settings>settingsList=settingsService.findSettings();
				int correctAnswers=0;
				if(settingsList!=null && settingsList.size()>0){
					Settings settings=settingsList.get(0);

					if(userBean!=null){
						User user=userService.getUserById(userBean.getUserId());
						if(user!=null){
							Map<String, List<Attribute>>imageAnswerMap=(Map<String, List<Attribute>>)session.getAttribute(EspConstant.NON_ASSISTED_IMAGES_ANSWERS);
							Map<String, String>imageTimeMap=(Map<String, String>)session.getAttribute(EspConstant.IMAGE_TIME_NON_ASSISTED);
							List<GameAttributeSummary>gameAttributeSummaryList=new ArrayList<GameAttributeSummary>();
							if(imageAnswerMap!=null && imageAnswerMap.size()>0){
								Set<Entry<String, List<Attribute>>>entrySet=imageAnswerMap.entrySet();
								for(Entry<String,  List<Attribute>> entry:entrySet){
									GameAttributeSummary attrSummary=new GameAttributeSummary();
									attrSummary.setImageId(entry.getKey());
									if(imageTimeMap!=null && imageTimeMap.size()>0){
									attrSummary.setPlayedTime(Integer.valueOf(imageTimeMap.get(entry.getKey())));
									}
									for(Attribute attr : entry.getValue()){
										attrSummary.getPlayerAttributeForRightAnswers().add(attr.getId());	
									}
									correctAnswers += entry.getValue().size();
									gameAttributeSummaryList.add(attrSummary);
								}
								gameService.saveNewNonAssistedGame(settings.getScorePerAttributeMatched()*correctAnswers, user.getId(),gameAttributeSummaryList,FILETYPE.valueOf(fileType));

							}
						}

						message.setMessage("Hi! "+user.getFirstName()+". You have scored "+String.valueOf(settings.getScorePerAttributeMatched()*correctAnswers)+" points.");
					}

				}
			}
		}


		return message;

	}

	@RequestMapping(value = "/searchAttrImage", method = RequestMethod.POST)
	public @ResponseBody List<ImageBean> searchImage(HttpServletRequest request, @RequestParam("query") String query,@RequestParam("fileType") String fileType) {
		HttpSession session = request.getSession(false);
		List<ImageBean> imageBeanList = new ArrayList<ImageBean>();
		if (session != null) {
			UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
			if(userBean!=null){
				List<Image> imageList = gameService.searchImage(query,FILETYPE.valueOf(fileType));
				for(Image image : imageList){
					ImageBean bean = new ImageBean();
					bean.setReadFilePath(request.getContextPath()+image.getReadFilePath());
					bean.setImageName(image.getImageName());
					imageBeanList.add(bean);
				}
			}
		}
		return imageBeanList;

	}

	
	private Set<Attribute> getAnswers(List<Attribute>attrList,User user,FILETYPE fileType){
		Set<Attribute> finalAnswerList = new HashSet<Attribute>();
		List<Attribute> answersList = new ArrayList<Attribute>();
		if(attrList != null && attrList.size() > 0){
			for(Attribute attr:attrList){
				//String id=attr.getId();
				if(!attr.getType().equals(ATTRIBUTETYPE.IRRELEVANT)){
				if(attr.getAnsweredBy().size()==0 || !(attr.getAnsweredBy().contains(user))){
					answersList.add(attr);
				}else if(new Date().compareTo(attr.getSysLastModified())>0){
					answersList.add(attr);
				}else{
					answersList.add(attr);
				}
				}
			}
		}
		
		List<Attribute>wrongAnswerList = attributeService.getAttributeByCriteria(answersList,ATTRIBUTEFORTYPE.valueOf(fileType.toString()));
		

		int maxResult = EspConstant.NO_OF_ANSWERS;
		if(answersList.size() < maxResult){
			maxResult = answersList.size();
		}
		int processedCount = CommonUtility.getRandomNo(maxResult);
		if( processedCount <= 0){
			processedCount = 1;
		}
		List<Integer> numberList = new ArrayList<Integer>();
		for(int i = 0; i < processedCount; i++){
			int attr = CommonUtility.getRandomNo(answersList.size());
			while(numberList.contains(attr)){
				attr = CommonUtility.getRandomNo(answersList.size());
			}

			numberList.add(attr);
			finalAnswerList.add(answersList.get(attr));
		}

		maxResult = EspConstant.NO_OF_ANSWERS - processedCount;
		if(wrongAnswerList.size() < maxResult){
			maxResult = wrongAnswerList.size();
		}
		numberList.clear();
		for(int i = 0; i < maxResult; i++){
			int attr = CommonUtility.getRandomNo(wrongAnswerList.size());
			while(numberList.contains(attr)){
				attr = CommonUtility.getRandomNo(wrongAnswerList.size());
			}

			numberList.add(attr);
			finalAnswerList.add(wrongAnswerList.get(attr));
		}
		return finalAnswerList;
	}
	
	private List<Attribute> getDefaultAttributeList(List<Attribute>attrList){
		List<Attribute>defaultAttrList=new ArrayList<Attribute>();
		if(attrList!=null && attrList.size()>0){
		for(Attribute attr:attrList){
			if(attr.getType().equals(ATTRIBUTETYPE.DEFAULT)){
				defaultAttrList.add(attr);
			}
		}
		}
		return defaultAttrList;
	}
}
