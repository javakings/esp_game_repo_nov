/**
 * 
 */
package com.esp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.esp.bean.UserInfoBean;
import com.esp.model.game.Game;
import com.esp.model.game.GameAttributeSummary;
import com.esp.model.type.GAMEMODE;
import com.esp.model.user.User;
import com.esp.service.AttributeCategoryService;
import com.esp.service.AttributeService;
import com.esp.service.GameService;
import com.esp.service.ImageService;
import com.esp.service.SettingsService;
import com.esp.service.UserService;

/**
 * @author Jay
 *
 */
@Controller
@RequestMapping(value = "/manageUser")
public class ManageUserController {
	@Autowired
	private ImageService imageService;

	@Autowired
	private AttributeService attributeService;

	@Autowired
	private UserService userService;

	@Autowired
	private SettingsService settingsService;

	@Autowired
	private GameService gameService;

	@Autowired
	private AttributeCategoryService attrCatService;

	@RequestMapping(value = "/getUserData", method = RequestMethod.GET)
	public String getUserData(Model model, HttpServletRequest request) {
		model.addAttribute("userList", getUserInfo(request));
		return "ManageUser";
	}

	private List<UserInfoBean> getUserInfo(HttpServletRequest request) {
		List<UserInfoBean> userInfoList = new ArrayList<UserInfoBean>();
		List<User> userList = userService.findAllUsers();
		List<Game> gameList = gameService.findAllGames();

		if (userList != null && userList.size() > 0) {
			for (User user : userList) {
				UserInfoBean userInfo = new UserInfoBean();
				userInfo.setUser(user);
				int asssistedGameCountPlayed = 0;
				int unAsssistedGameCountPlayed = 0;
				if (gameList != null && gameList.size() > 0) {
					for (Game game : gameList) {
						if (game.getGameMode().equals(GAMEMODE.ASSISTED)) {
							if (game.getPlayer1().getId().equals(user.getId())) {
								asssistedGameCountPlayed++;
							}

						} else {
							if (game.getPlayer1().getId().equals(user.getId())) {
								unAsssistedGameCountPlayed++;
							}

						}

					}
				}
				userInfo.setAssistedGamePlayed("" + asssistedGameCountPlayed);
				userInfo.setNonAssistedGamePlayed("" + unAsssistedGameCountPlayed);
				userInfoList.add(userInfo);
			}
		}

		return userInfoList;
	}
}
