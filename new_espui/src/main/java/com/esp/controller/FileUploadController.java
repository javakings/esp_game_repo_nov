/**
 * 
 */
package com.esp.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.esp.bean.ImageBean;
import com.esp.constant.EspConstant;
import com.esp.dao.AttributeDao;
import com.esp.form.ImageForm;
import com.esp.model.common.Message;
import com.esp.model.image.Attribute;
import com.esp.model.image.AttributeCategory;
import com.esp.model.image.Image;
import com.esp.model.image.ImageCategory;
import com.esp.model.type.ATTRIBUTETYPE;
import com.esp.model.type.FILETYPE;
import com.esp.model.user.User;
import com.esp.service.AttributeCategoryService;
import com.esp.service.AttributeService;
import com.esp.service.DbDataCorrectionService;
import com.esp.service.ImageCategoryService;
import com.esp.service.ImageService;
import com.esp.service.UserService;
import com.esp.utility.ToListConverterUtility;

/**
 * @author Amit
 *
 */
@Controller
@RequestMapping(value="/admin")
public class FileUploadController {
	
	@Autowired
	private ImageService service;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ImageCategoryService imageCatService;
	
	@Autowired
	private AttributeCategoryService attrCatService;
	
	@Autowired
	private DbDataCorrectionService correctionService;
	
	@Autowired
	private AttributeService attrService;
	
	@RequestMapping(value = "/home",method = RequestMethod.GET)
	public String displayUploadImageForm(Model model)
	{
		List<Image>imageList=service.findAllImages(FILETYPE.IMAGE);
		List<ImageCategory>imageCategoryList=imageCatService.findAllCategory();
		//model.setViewName("AdminHome");
		model.addAttribute("imagelist", imageList);
		model.addAttribute("imagecatlist", imageCategoryList);
		return "AdminHome";
	}
	
	@RequestMapping(value = "/musicHome",method = RequestMethod.GET)
	public String displayUploadMusicForm(Model model)
	{
		List<Image>imageList=service.findAllImages(FILETYPE.MUSIC);
		model.addAttribute("imagelist", imageList);
		return "MusicHome";
	}
	
	@RequestMapping(value = "/dataCorrect",method = RequestMethod.GET)
	public String displayDataCorrect(Model model)
	{
		List<Image>imageList=service.findAllImages(FILETYPE.IMAGE);
		if(imageList!=null && imageList.size()>0){
		for(Image image:imageList){
			List<Attribute>attrList=attrService.findAllAttributesByImageId(image.getId());
			int count = attrList.size();

		    for (int i = 0; i < count; i++) 
		    {
		        for (int j = i + 1; j < count; j++) 
		        {
		            if (attrList.get(i).getAttrName().trim().equalsIgnoreCase(attrList.get(j).getAttrName().trim()))
		            {
		            	
		            	//correctionService.correctData(attrList.get(i).getId(), attrList.get(j).getId(), image.getId(),userService.getUserById("ff80808151476798015147c8c6400122"));
		            	attrList.remove(j--);
		                count--;
		            	
		            }
		        }
		    }
		}
		}
		return "DataCorrect";
	}
	

	@RequestMapping(value = "/profile",method = RequestMethod.GET)
	public String displayProfile(Model model)
	{
		return "adminProfile";
	}
	
	@RequestMapping(value = "/manageImageAttribute",method = RequestMethod.GET)
	public String displayManageImageAttribute(Model model)
	{
		List<Image>imageList=service.findAllImages(FILETYPE.IMAGE);
		List<ImageCategory>imageCategoryList=imageCatService.findAllCategory();
		//model.setViewName("AdminHome");
		List<ImageCategory>finalList=new ArrayList<ImageCategory>();
		model.addAttribute("imagelist", imageList);
	
		model.addAttribute("imagecatlist", imageCategoryList);
		return "ManageImgeAttribute";
	}
	
	@RequestMapping(value = "/manageTuneAttribute",method = RequestMethod.GET)
	public String displayManageTuneAttribute(Model model)
	{
		List<Image>imageList=service.findAllImages(FILETYPE.MUSIC);
		List<ImageCategory>imageCategoryList=imageCatService.findAllCategory();
		//model.setViewName("AdminHome");
		List<ImageCategory>finalList=new ArrayList<ImageCategory>();
		model.addAttribute("imagelist", imageList);
	
		model.addAttribute("imagecatlist", imageCategoryList);
		return "ManageTuneAttribute";
	}
	

	@RequestMapping(value="/upload", method = RequestMethod.POST)
	public ModelAndView uploadImage(ModelAndView model, ImageForm form, HttpServletRequest request )
	{
		model.setViewName("redirect:home");
		MultipartFile file = form.getImageFile();
		
		if (null!=file && !file.isEmpty()) {
			try {
				String originalFileName =  new Date().getTime() + file.getOriginalFilename();
				String readFilePath = "/"+ EspConstant.DEFAULT_PATH  + originalFileName;
				String trimmedFileName="test";
				if (file.getOriginalFilename().indexOf(".") > 0){
					trimmedFileName=file.getOriginalFilename().substring(0, file.getOriginalFilename().lastIndexOf("."));
				}
				String filePath = request.getServletContext().getRealPath(EspConstant.DEFAULT_PATH) +File.separator+ originalFileName; 
				boolean isSuccess =	service.upload(file, filePath);
				if(isSuccess)
				{
					service.uploadImage(trimmedFileName, form.getImageCatagoryId(), filePath ,readFilePath,new ToListConverterUtility().convertToList(form.getTabooAttributes()),FILETYPE.IMAGE );
					List<Image>imageList=service.findAllImages(FILETYPE.IMAGE);
					model.addObject("imagelist", imageList);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else 
		{
		
		}
		return model;
	}
	
	
	@RequestMapping(value="/uploadMusicFile", method = RequestMethod.POST)
	public ModelAndView uploadMusic(ModelAndView model, ImageForm form, HttpServletRequest request )
	{
		model.setViewName("redirect:musicHome");
		MultipartFile file = form.getImageFile();
		
		if (null!=file && !file.isEmpty()) {
			try {
				String originalFileName =  new Date().getTime() + file.getOriginalFilename();
				String readFilePath = "/"+ EspConstant.DEFAULT_PATH  + originalFileName;
				String trimmedFileName="test";
				if (file.getOriginalFilename().indexOf(".") > 0){
					trimmedFileName=file.getOriginalFilename().substring(0, file.getOriginalFilename().lastIndexOf("."));
				}
				String filePath = request.getServletContext().getRealPath(EspConstant.DEFAULT_PATH) +File.separator+ originalFileName; 
				boolean isSuccess =	service.upload(file, filePath);
				if(isSuccess)
				{
					service.uploadImage(trimmedFileName, form.getImageCatagoryId(), filePath ,readFilePath,new ToListConverterUtility().convertToList(form.getTabooAttributes()),FILETYPE.MUSIC );
					List<Image>imageList=service.findAllImages(FILETYPE.MUSIC);
					model.addObject("imagelist", imageList);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else 
		{
		
		}
		return model;
	}
	
	
	@RequestMapping(value="/edit", method = RequestMethod.POST)
	public @ResponseBody ImageBean editImage(HttpServletRequest request, @RequestParam("imageId")String imageId)
	{
		Image image= service.findImageById(imageId);
		ImageBean imageBean = new ImageBean();
		imageBean.setReadFilePath(request.getContextPath() + image.getReadFilePath());
		imageBean.setImageName(image.getImageName());
		imageBean.setImageCatagory(image.getImageCatagory());
		imageBean.setSysCreationDate(new SimpleDateFormat("dd/MMM/YYYY").format(image.getSysCreationDate()));
		Set<Attribute>defaultCategoryList=new HashSet<Attribute>();
		List<Attribute>attrList=image.getAttributeList();
		if(attrList!=null && attrList.size()>0){
			for(Attribute attr:attrList){
				if(attr.getType()==ATTRIBUTETYPE.DEFAULT){
					defaultCategoryList.add(attr);
				}
			}
		}
		
		imageBean.setAttributeList(defaultCategoryList);
		
		return imageBean;
		 
	}
	
	
	@RequestMapping(value="/editImageAll", method = RequestMethod.POST)
	public @ResponseBody ImageBean editImageAllAttriute(HttpServletRequest request,@RequestParam("imageId")String imageId)
	{
		Image image= service.findImageById(imageId);
		ImageBean imageBean = new ImageBean();
		imageBean.setReadFilePath(request.getContextPath() + image.getReadFilePath());
		imageBean.setImageName(image.getImageName());
		imageBean.setSysCreationDate(new SimpleDateFormat("dd/MMM/YYYY").format(image.getSysCreationDate()));
		imageBean.setImageCatagory(image.getImageCatagory());
		List<Attribute>defaultCategoryList=new ArrayList<Attribute>();
		List<Attribute>attrList=image.getAttributeList();
		if(attrList!=null && attrList.size()>0){
			for(Attribute attr:attrList){
				if(attr.getType()!=ATTRIBUTETYPE.DEFAULT){
					defaultCategoryList.add(attr);
				}
			}
		}
		
		imageBean.setAttributeList(new HashSet<Attribute>(defaultCategoryList));
		
		return imageBean;
		 
	}

	@RequestMapping(value="/delete", method = RequestMethod.POST)
	public @ResponseBody Message deleteImage(@RequestParam("imageId")String imageId)
	{
		
		Message msg=new Message();
		service.deleteImage(imageId);
		msg.setStatus(true);
		return msg;
	}
	
	@RequestMapping(value="/getImageCategory", method = RequestMethod.GET)
	public @ResponseBody List<ImageCategory> getImageCategory()
	{
		return imageCatService.findAllCategory();
	}
	
	@RequestMapping(value="/getAttrCategory", method = RequestMethod.GET)
	public @ResponseBody List<AttributeCategory> getAttributeCategory()
	{
		List<AttributeCategory>finalAttrCatList=new ArrayList<AttributeCategory>();
		for(AttributeCategory attr:attrCatService.findAllCategory()){
		if(!attr.getCategoryName().equals(EspConstant.TUNE)){
			finalAttrCatList.add(attr);
		}
	}
		return finalAttrCatList ;
	}
	
	@RequestMapping(value="/getAttributeType", method = RequestMethod.GET)
	public @ResponseBody ATTRIBUTETYPE[] getAttributeType()
	{
		return ATTRIBUTETYPE.values();
	}
	
	@RequestMapping(value="/addAttribute", method = RequestMethod.POST)
	public @ResponseBody Message addAttribute(@RequestParam("attribute")String attribute,@RequestParam("imageId")String imageId)
	{
		Message msg=new Message();
		List<Attribute>attrList=service.addAttribute(attribute, imageId,null,null);
		msg.setObjList(new ArrayList<Object>());
		for(Attribute attr:attrList){		
		msg.getObjList().add(attr);
		}
		msg.setStatus(true);
		return msg;
	}
	
	
	@RequestMapping(value="/updateImage", method = RequestMethod.POST)
	public @ResponseBody Message updateImage(@RequestParam("imageCategory")String imageCategoryId,@RequestParam("imageId")String imageId,@RequestParam("imageName")String imageName)
	{
		Message msg=new Message();
		service.updateImage(imageId, imageCategoryId,imageName,FILETYPE.IMAGE);
		msg.setStatus(true);
		return msg;
	}
	
	@RequestMapping(value="/updateMusic", method = RequestMethod.POST)
	public @ResponseBody Message updateMusic(@RequestParam("imageId")String imageId,@RequestParam("imageName")String imageName)
	{
		Message msg=new Message();
		service.updateImage(imageId, null,imageName,FILETYPE.MUSIC);
		msg.setStatus(true);
		return msg;
	}
	
	
	@RequestMapping(value="/updateImageAttribute", method = RequestMethod.POST)
	public @ResponseBody Message updateImageAttribute(@RequestParam("attrCategoryId")String attrCategoryId,@RequestParam("imageId")String imageId,@RequestParam("attrId")String attrId,@RequestParam("attrName")String attrName,@RequestParam("attrType")String attrType)
	{
		Message msg=new Message();
		msg.setStatus(false);
		AttributeCategory attrCat=attrCatService.findCategoryById(attrCategoryId);
		if(attrCat!=null && attrType!=null && attrId!=null && imageId!=null){
			service.updateImageAttribute(imageId, attrId, attrCat, ATTRIBUTETYPE.valueOf(attrType), attrName);
		}
	
		
		return msg;
	}
	

	@RequestMapping(value="/updateProfile", method = RequestMethod.POST)
	public @ResponseBody Message updateProfile(@RequestParam("fname")String fname,@RequestParam("lname")String lname,@RequestParam("city")String city,@RequestParam("email")String email,@RequestParam("id")String id,HttpServletRequest request)
	{
		Message msg=new Message();
		User user=userService.updateUser(fname, lname, city, email, id,request);
		msg.setMsgObj(user);
		msg.setStatus(true);
		return msg;
	}
	
	@RequestMapping(value="/getProfile", method = RequestMethod.POST)
	public @ResponseBody Message getProfile(@RequestParam("id")String id)
	{
		Message msg=new Message();
		User user=userService.getUserById(id);
		msg.setMsgObj(user);
		msg.setStatus(true);
		return msg;
	}
	
	@RequestMapping(value="/deleteAttr", method = RequestMethod.POST)
	public @ResponseBody Message deleteAttribute(@RequestParam("attrId")String attrId,@RequestParam("imageId")String imageId)
	{
		Message msg=new Message();
		service.deleteAttribute(attrId, imageId);
		msg.setStatus(true);
		
		return msg;
	}
	
	
	
	@RequestMapping(value="/uploadUserImage", method = RequestMethod.POST)
	public ModelAndView uploadUserImage(ModelAndView model, ImageForm form, HttpServletRequest request )
	{
		model.setViewName("redirect:profile");
		MultipartFile file = form.getImageFile();
		
		if (null!=file && !file.isEmpty()) {
			try {
				String originalFileName =  new Date().getTime() + file.getOriginalFilename();
				String readFilePath = "/"+ EspConstant.DEFAULT_PATH  + originalFileName;
				String filePath = request.getServletContext().getRealPath(EspConstant.DEFAULT_PATH) +File.separator+ originalFileName; 
				boolean isSuccess =	service.upload(file, filePath);
				if(isSuccess)
				{
					userService.uploadImage(filePath, readFilePath, form.getUserId(),request);
					
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else 
		{
		
		}
		return model;
	}
}
