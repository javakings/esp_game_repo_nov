/**
 * 
 */
package com.esp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.esp.bean.ImageBean;
import com.esp.bean.ResultBean;
import com.esp.bean.UserBean;
import com.esp.constant.EspConstant;
import com.esp.model.common.Message;
import com.esp.model.image.Image;
import com.esp.model.type.STATUS;
import com.esp.service.AttributeService;
import com.esp.service.GameService;
import com.esp.service.SettingsService;
import com.esp.service.UserService;

/**
 * @author Amit
 *
 */
@Controller
@RequestMapping(value="multiPlayer")
public class MultiPlayerController {

	@Autowired
	private GameService gameService;

	@Autowired
	private AttributeService attributeService;

	@Autowired
	private UserService userService;

	@Autowired
	private SettingsService settingsService;

	@RequestMapping(value="/MultiplayerGame",method = RequestMethod.GET)
	public ModelAndView playGame(ModelAndView model) {
		model.setViewName("MultiPlayer");
		return model;
	}

	@RequestMapping(value = "/playGame", method = RequestMethod.GET)
	public @ResponseBody Message playGame(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		Message msg=new Message();
		if (session != null) {
			UserBean userBean=(UserBean)session.getAttribute(EspConstant.USER);
			if(userBean!=null){
				gameService.addPlayer(userBean.getUserId(), request.getServletContext());
			}    
		}
		return msg;

	}
	@RequestMapping(value = "/getImage", method = RequestMethod.POST)
	public @ResponseBody ImageBean getImageForGame(HttpServletRequest request,
			@RequestParam("imageCount") String imageCount, @RequestParam("gameId")String gameId) {
		HttpSession session = request.getSession(false);
		int imageNum = Integer.parseInt(imageCount);
		Image image = null;
		ImageBean imagebean=new ImageBean();
		if (session != null) {
			ServletContext sc = request.getServletContext();
			image = gameService.getImage(gameId, sc,imageNum );
			
			if(image!=null){
				List<Image> imageList = gameService.getImageList(gameId, sc);
				imagebean.setImageId(image.getId());
				imagebean.setImagesToShowNo(imageList.size()+"");
				imagebean.setReadFilePath(image.getReadFilePath());
			}else{
				imagebean.setStatus(STATUS.FAILURE);

			}
		}
		return imagebean;

	}
	
	@RequestMapping(value = "/submitAttribute", method = RequestMethod.POST)
	public @ResponseBody ImageBean submitAttribute(HttpServletRequest request, @RequestParam("gameId") String gameId,
			@RequestParam("playerId") String playerId, @RequestParam("imageId") String imageId, 
			@RequestParam("attribute") String attribute, @RequestParam("imageCount") String imageCount){
		HttpSession session = request.getSession(false);
		ImageBean imageBean = new ImageBean();
		if(session != null){
			gameService.submitAttribute(gameId, playerId, attribute, imageId);
			int imageNum = Integer.parseInt(imageCount);
			ServletContext sc = request.getServletContext();
			List<Image> imageList = gameService.getImageList(gameId, sc);
			if(imageList != null && imageList.size() > (imageNum+1)){
				Image image = gameService.getImage(gameId, sc,imageNum+1 );
				imageBean.setImageId(image.getId());
				imageBean.setImagesToShowNo(imageList.size()+"");
				imageBean.setReadFilePath(image.getReadFilePath());
			}
		}
		return imageBean;
	}
	
	@RequestMapping(value = "/evaluate", method = RequestMethod.POST)
	public List<ResultBean> showResult(HttpServletRequest request, @RequestParam("gameId") String gameId){
		HttpSession session = request.getSession(false);
		List<ResultBean> resultList = new ArrayList<ResultBean>();
		if(session != null){
			resultList = gameService.evaluateGame(gameId);
		}
		return resultList;
	}
	
}
