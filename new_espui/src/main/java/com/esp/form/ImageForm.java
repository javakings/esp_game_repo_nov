package com.esp.form;

import org.springframework.web.multipart.MultipartFile;

import com.esp.model.type.FILETYPE;
import com.esp.model.type.IMAGECATAGORY;

public class ImageForm {
	
	private String imageName;
	
	private String userId;
	
	private FILETYPE fileType;
	
	private String imageId;
	
	private String imageCatagoryId;
	
	private MultipartFile imageFile;
	
	private String tabooAttributes;

	public String getTabooAttributes() {
		return tabooAttributes;
	}

	public void setTabooAttributes(String tabooAttributes) {
		this.tabooAttributes = tabooAttributes;
	}

	public String getImageCatagoryId() {
		return imageCatagoryId;
	}

	public void setImageCatagoryId(String imageCatagoryId) {
		this.imageCatagoryId = imageCatagoryId;
	}

	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}


	/**
	 * @return the imageFile
	 */
	public MultipartFile getImageFile() {
		return imageFile;
	}

	/**
	 * @param imageFile the imageFile to set
	 */
	public void setImageFile(MultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public FILETYPE getFileType() {
		return fileType;
	}

	public void setFileType(FILETYPE fileType) {
		this.fileType = fileType;
	}


	

}
