/**
 * 
 */
package com.esp.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.esp.dao.ImageDao;
import com.esp.model.image.Attribute;
import com.esp.model.image.AttributeCategory;
import com.esp.model.image.Image;
import com.esp.model.type.ATTRIBUTETYPE;
import com.esp.model.type.FILETYPE;
import com.esp.model.user.User;
import com.esp.service.ImageService;

/**
 * @author Amit
 *
 */
@Service
@Transactional
public class ImageServiceImpl implements ImageService{

	@Autowired
	private ImageDao imageDao;

	public void uploadImage(String imageName, String catagoryId, String filePath,String readFilePath,List<Attribute>attributeList,FILETYPE fileType) {
		imageDao.uploadImage(imageName, catagoryId, filePath ,readFilePath,attributeList,fileType);

	}

	public boolean upload(MultipartFile file, String filePath) {
		
		boolean isSuccess = false;
		try
		{
			byte[] bytes = file.getBytes();
			BufferedOutputStream stream =
					new BufferedOutputStream(new FileOutputStream(new File(filePath)));
			stream.write(bytes);
			stream.close();
			isSuccess = true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return isSuccess;
	}

	public List<Image> findAllImages(FILETYPE fileType) {
		return imageDao.findAllImages(fileType);
	}

	public void deleteImage(String id) {
		imageDao.deleteImage(id);
		
	}

	public Image findImageById(String id) {
		return imageDao.findImagebyId(id);
	}

	public void updateImage(String id, String catagoryId,String imageName,FILETYPE fileType) {
		imageDao.updateImage(id, catagoryId,imageName,fileType);
		
	}

	public void deleteAttribute(String attrId, String imageId) {
		imageDao.deleteAttribute(attrId, imageId);
		
	}

	public List<Attribute> addAttribute(String attribute, String imageId,AttributeCategory cat,User answeredBy) {
		return imageDao.addAttribute(attribute, imageId,cat,answeredBy);
		
	}

	public List<Image> getImageForGame(FILETYPE fileType,User user) {
		return imageDao.getImageForGame(fileType,user) ;
	}

	@Override
	public void updateImageAttribute(String imageId, String attrId,AttributeCategory attrCat,ATTRIBUTETYPE type,String attributeName) {
		imageDao.updateImageAttribute(imageId, attrId, attrCat, type, attributeName);
		
	}

	@Override
	public void updateImageAttributeAnsweredBy(String attrId, User answeredBy, String imageId) {
		imageDao.updateImageAttributeAnsweredBy(attrId, answeredBy, imageId);
		
	}

	@Override
	public void updateImageAttributeIgnoredBy(String attrId, User answeredBy, String imageId) {
		imageDao.updateImageAttributeIgnoredBy(attrId, answeredBy, imageId);
		
	}

	@Override
	public void updateImageAttributeWrongAnsweredBy(String attrId, User answeredBy, String imageId) {
		imageDao.updateImageAttributeWrongAnsweredBy(attrId, answeredBy, imageId);
		
	}

	@Override
	public void updateImageSeenBy(String id, User seenBy) {
		imageDao.updateImageSeenBy(id, seenBy);
		
	}

}
