package com.esp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.esp.dao.DbDataCorrectionDao;
import com.esp.model.user.User;
import com.esp.service.DbDataCorrectionService;


@Service
@Transactional
public class DbDataCorrectionServiceImpl implements DbDataCorrectionService {

	@Autowired
	DbDataCorrectionDao correctionDao;
	@Override
	public void correctData(String persistedAttrid, String attrId, String imageId,User answeredBy) {
		correctionDao.correctData(persistedAttrid, attrId, imageId,answeredBy);
		
	}

}
