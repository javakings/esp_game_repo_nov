/**
 * 
 */
package com.esp.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.esp.dao.SettingsDao;
import com.esp.model.gamesettings.Settings;
import com.esp.service.SettingsService;

/**
 * @author gyan
 *
 */
@Service
@Transactional
public class SettingsServiceImpl implements SettingsService{
	
	@Autowired
	private SettingsDao settingsDao;

	public List<Settings> findSettings() {
		return settingsDao.findSettings();
	}

	public void addSettings(Date defaultPlayTime,
			Integer scorePerAttributeMatched) {
		settingsDao.addSettings(defaultPlayTime, scorePerAttributeMatched);
		
	}

}
