/**
 * 
 */
package com.esp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.esp.dao.AttributeDao;
import com.esp.model.image.Attribute;
import com.esp.model.type.ATTRIBUTEFORTYPE;
import com.esp.service.AttributeService;

/**
 * @author Jay
 *
 */
@Service
@Transactional
public class AttributeServiceImpl implements AttributeService{
	
	@Autowired 
	private AttributeDao attributeDao;

	public List<Attribute> findAllAttributes() {
		
		return attributeDao.findAllAttributes();
	}

	public Attribute getAttributeById(String attrId) {
		
		return attributeDao.getAttributeById(attrId);
	}

	public List<Attribute> getAttributeBySql(String sql) {
		return attributeDao.getAttributeBySql(sql);
	}

	public List<Attribute> getAttributeByCriteria(List<Attribute> attrList,ATTRIBUTEFORTYPE attributeForType) {
		return attributeDao.getAttributeByCriteria(attrList,attributeForType);
	}
	
	public List<Attribute> findAllAttributesByImageId(String imageId) {
		return attributeDao.findAllAttributesByImageId(imageId);
	}

	
}
