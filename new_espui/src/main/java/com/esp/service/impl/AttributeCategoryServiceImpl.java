/**
 * 
 */
package com.esp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.esp.dao.AttributeCategoryDao;
import com.esp.model.image.AttributeCategory;
import com.esp.service.AttributeCategoryService;

/**
 * @author Jay
 *
 */

@Service
@Transactional
public class AttributeCategoryServiceImpl implements AttributeCategoryService{
    
	@Autowired
	private AttributeCategoryDao attrCatDao;
	
	@Override
	public List<AttributeCategory> findAllCategory() {
		
		return attrCatDao.findAllCategory();
	}

	@Override
	public AttributeCategory findCategoryById(String id) {
		
		return attrCatDao.findCategoryById(id);
	}

	@Override
	public void addCategory(String name) {
		attrCatDao.addCategory(name);
		
	}

	@Override
	public void updateCategory(String id, String name) {
		attrCatDao.updateCategory(id,name);
		
	}

	@Override
	public void deleteCategory(String id, String name) {
		// TODO Auto-generated method stub
		
	}

}
