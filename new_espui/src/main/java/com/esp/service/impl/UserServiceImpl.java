package com.esp.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.esp.dao.UserDao;
import com.esp.model.auth.Authentication;
import com.esp.model.user.User;
import com.esp.service.UserService;
import com.esp.utility.AuthenticationUtility;
@Service
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao dao;
	  
	public void saveUser(User user, String loginId, String password) {
		// TODO Auto-generated method stub
		dao.saveUser(user, loginId, password);
	}

	public List<User> findAllUsers() {
		return dao.findAllUsers();
	}

	public User deleteUserById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public User getUserById(String id) {
		return dao.getUserById(id);
	}

	public Authentication authenticate(String loginId, String password) {
		Authentication auth = dao.getUserByLoginId(loginId);
		if(auth != null)
		{
			String encryptedPassword = new AuthenticationUtility().encrypt(password);
			if(auth.getPassword().equals(encryptedPassword))
			{
				return auth;
			}
		}
		return null;
	}

	public User getUserByLoginId(String loginId) {
		Authentication auth = dao.getUserByLoginId(loginId);
		if(auth != null)
		{
			return auth.getUser();
		}
		return null;
	}

	public User updateUser(String fname, String lname, String city,
			String email, String id,HttpServletRequest request) {
		return dao.updateUser(fname, lname, city, email, id,request);
	}

	public void uploadImage(String filePath, String readFilePath, String id,HttpServletRequest request) {
		dao.uploadImage(filePath, readFilePath, id,request);
		
	}

}
