package com.esp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.esp.dao.ImageCategoryDao;
import com.esp.model.image.ImageCategory;
import com.esp.service.ImageCategoryService;

@Service
@Transactional
public class ImageCategoryServiceImpl implements ImageCategoryService {

	@Autowired
	private ImageCategoryDao imageCatDao;
	
	@Override
	public List<ImageCategory> findAllCategory() {
		return imageCatDao.findAllCategory();
	}

	@Override
	public ImageCategory findCategoryById(String id) {
		return imageCatDao.findCategoryById(id);
	}

	@Override
	public void addCategory(String name) {
		imageCatDao.addCategory(name);
		
	}

	@Override
	public void updateCategory(String id, String name) {
		imageCatDao.updateCategory(id, name);
		
	}

	@Override
	public void deleteCategory(String id, String name) {
		// TODO Auto-generated method stub
		
	}

}
