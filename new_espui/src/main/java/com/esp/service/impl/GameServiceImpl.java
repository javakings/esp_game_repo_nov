/**
 * 
 */
package com.esp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.esp.bean.ResultBean;
import com.esp.dao.GameDao;
import com.esp.dao.ImageDao;
import com.esp.dao.SearchDao;
import com.esp.dao.UserDao;
import com.esp.model.game.Game;
import com.esp.model.game.GameAttributeSummary;
import com.esp.model.game.PlayerAttribute;
import com.esp.model.image.Image;
import com.esp.model.type.FILETYPE;
import com.esp.service.GameService;

/**
 * @author Amit
 *
 */
@Service
@Transactional
public class GameServiceImpl implements GameService {

	@Autowired
	private GameDao gameDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private ImageDao imageDao;
	
	@Autowired
	private SearchDao searchDao;

	@Override
	public void submitAttribute(String gameId, String userId, String attribute, String imageId) {
		GameAttributeSummary attrSummary = gameDao.getGameAttributeSummary(gameId, imageId);
		PlayerAttribute playerAttribute = new PlayerAttribute();
		playerAttribute.setPlayerId(userId);
		//playerAttribute.setAttribute(ToListConverterUtility.convertToList(attribute, ","));
		//attrSummary.getPlayerAttribute().add(playerAttribute);
		gameDao.saveGameAttribute(gameId, imageId, attrSummary);
		
	}

	
	public List<String> compareAttribute(String gameId, String imageId) {
		/*GameAttributeSummary attrSummary = gameDao.getGameAttributeSummary(gameId, imageId);
		List<PlayerAttribute>playerList = attrSummary.getPlayerAttribute();
		List<String> allAttr = new ArrayList<String>(); 
		List<String> commonAttr = new ArrayList<String>();
		Set<String> temp = new HashSet<String>();

		for(PlayerAttribute player : playerList){
			//allAttr.addAll(player.getAttribute());
		}
		for(String attr: allAttr)
		{
			if(!temp.add(attr))
			{
				commonAttr.add(attr);
			}
		}*/

		return null;
	}

	public boolean isCompare(String gameId, String imageId) {
		/*GameAttributeSummary attrSummary = gameDao.getGameAttributeSummary(gameId, imageId);
		if(attrSummary.getPlayerAttribute().size() > 0)
		{
			return true;
		}*/
		return false;
	}

	@Override
	public Game choosePlayer(String playerId, ServletContext sc) {
		Map<String, Game> gameMap = (Map<String, Game>) sc.getAttribute("gameMap");
		Game game = gameMap.get(playerId); 
		if(game != null){
			return game; 
		}
		synchronized (this) {
			ConcurrentLinkedQueue<String> playerQueues = (ConcurrentLinkedQueue<String>)sc.getAttribute("playerQueues");
			for(String playerQueue : playerQueues){
				if(!playerQueue.equals(playerId)){
					playerQueues.remove(playerQueue);
					game = gameDao.saveNewMultiplayerGame(userDao.getUserById(playerId), userDao.getUserById(playerQueue));
					gameMap.put(playerQueue, game);
				}
				break;
			}
		}

		return game;
	}



	@Override
	public void addPlayer(String playerId, ServletContext sc) {
		ConcurrentLinkedQueue<String> playerQueues = (ConcurrentLinkedQueue<String>)sc.getAttribute("playerQueues");
		playerQueues.add(playerId);

	}

	@Override
	public void setImage(String gameId, ServletContext sc) {
		Map<String, List<Image>> gameImageMap = (Map<String, List<Image>>)sc.getAttribute("gameImageMap");
		if(gameImageMap.get(gameId) == null){
			List<Image> imageList = imageDao.getImageForGame(null,null);		
			gameImageMap.put(gameId, imageList);
		}

	}
	
	public List<Image> getImageList(String gameId, ServletContext sc){
		Map<String, List<Image>> gameImageMap = (Map<String, List<Image>>)sc.getAttribute("gameImageMap");
		List<Image> imageList = gameImageMap.get(gameId);
		if(gameImageMap.get(gameId) == null){
			imageList = imageDao.getImageForGame(null,null);		
			gameImageMap.put(gameId, imageList);
		}
		return imageList; 
	}

	@Override
	public Image getImage(String gameId, ServletContext sc, int counter) {
		Map<String, List<Image>> gameImageMap = (Map<String, List<Image>>)sc.getAttribute("gameImageMap");
		Image image = null;
		List<Image> imageList = gameImageMap.get(gameId);
		if(imageList == null){
			setImage(gameId, sc);
			imageList = gameImageMap.get(gameId);
		}
		if(imageList != null && imageList.size() >= counter){
			image = imageList.get(counter);
		}
		
		return image;
	}


	@Override
	public List<ResultBean> evaluateGame(String gameId) {
		List<ResultBean> resultList = new ArrayList<ResultBean>();
		Game game = gameDao.getGame(gameId);
		List<GameAttributeSummary> gameAttrSummary = game.getAttrSummaryList();
		/*for(GameAttributeSummary attrSummary : gameAttrSummary){
			List<PlayerAttribute> playerAttrList = attrSummary.getPlayerAttribute();
			if(playerAttrList != null && playerAttrList.size() == 2){
				PlayerAttribute player1 = playerAttrList.get(0);
				PlayerAttribute player2 = playerAttrList.get(1);
				List<String> attributeList = player1.getAttribute(); 
				attributeList.retainAll(player2.getAttribute());
				ResultBean resultBean = new ResultBean();
				resultBean.setImageId(attrSummary.getImageId());
				resultBean.setMatchedAttribute(attributeList);
				resultBean.setScore(attributeList.size());
				Image image = imageDao.findImagebyId(attrSummary.getImageId());
				resultBean.setImagePath(image.getReadFilePath());
				resultList.add(resultBean);
			}
		}*/
		return resultList;
	}
	
	@Override
	public List<Image> searchImage(String query,FILETYPE fileType){
		return searchDao.searchImage(query,fileType);
	}



	@Override
	public boolean isEvaluate(String gameId) {
		
		return false;
	}


	@Override
	public Game saveNewSinglePlayerGame(Integer score, String userId,List<GameAttributeSummary>gameAttributeSummaryList,FILETYPE fileType) {
		return gameDao.saveNewSinglePlayerGame(score, userId,gameAttributeSummaryList,fileType);
	}


	@Override
	public Game saveNewNonAssistedGame(Integer score, String userId,List<GameAttributeSummary>gameAttributeSummaryList,FILETYPE fileType) {
		return gameDao.saveNewNonAssistedGame(score, userId,gameAttributeSummaryList,fileType);
	}


	@Override
	public PlayerAttribute savePlayerAttribute(PlayerAttribute playeAttribute) {
		// TODO Auto-generated method stub
		return gameDao.savePlayerAttribute(playeAttribute);
	}


	@Override
	public List<Game> findAllGames() {
		// TODO Auto-generated method stub
		return gameDao.findAllGames();
	}


	@Override
	public List<GameAttributeSummary> getGameAttributeSummaryList(String gameId, String imageId) {
		// TODO Auto-generated method stub
		return gameDao.getGameAttributeSummaryList(gameId, imageId);
	}

}
