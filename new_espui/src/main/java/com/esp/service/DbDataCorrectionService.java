package com.esp.service;

import com.esp.model.user.User;

public interface DbDataCorrectionService {
	public void correctData(String persistedAttrid,String attrId,String imageId,User answeredBy);
}
