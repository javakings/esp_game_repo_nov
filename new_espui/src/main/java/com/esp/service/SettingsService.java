/**
 * 
 */
package com.esp.service;

import java.util.Date;
import java.util.List;

import com.esp.model.gamesettings.Settings;

/**
 * @author Jay
 *
 */
public interface SettingsService {
	 public List<Settings>findSettings();
	 public void addSettings(Date defaultPlayTime,Integer scorePerAttributeMatched);
}
