package com.esp.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.esp.model.auth.Authentication;
import com.esp.model.user.User;

public interface UserService {
	  void saveUser(User user, String loginId, String password);
	     
	  List<User> findAllUsers();
	     
	  User deleteUserById(String id);
	  
	  User getUserById(String id);
	  
	  User updateUser(String fname,String lname,String city,String email,String id,HttpServletRequest request);
	  
	  public void uploadImage(String filePath,String readFilePath,String id,HttpServletRequest request);
	  
	  User getUserByLoginId(String loginId);
	  
	  Authentication authenticate(String loginId, String password);
}
