/**
 * 
 */
package com.esp.service;

import java.util.List;

import com.esp.model.image.AttributeCategory;

/**
 * @author Jay
 *
 */
public interface AttributeCategoryService {
	 public List<AttributeCategory>findAllCategory();
	  public AttributeCategory findCategoryById(String id);
	  public void addCategory(String name);
	  public void updateCategory(String id,String name);
	  public void deleteCategory(String id,String name);
}
