/**
 * 
 */
package com.esp.service;

import java.util.List;

import com.esp.model.image.Attribute;
import com.esp.model.type.ATTRIBUTEFORTYPE;

/**
 * @author Jay
 *
 */
public interface AttributeService {
	  public List<Attribute> findAllAttributes();
	  public Attribute getAttributeById(String attrId);
	  public List<Attribute> getAttributeBySql(String sql);
	  public List<Attribute>getAttributeByCriteria(List<Attribute>attrList,ATTRIBUTEFORTYPE attributeForType);
	  public List<Attribute> findAllAttributesByImageId(String imageId) ;
}
