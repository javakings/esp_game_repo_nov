/**
 * 
 */
package com.esp.service;

import java.util.List;

import javax.servlet.ServletContext;

import com.esp.bean.ResultBean;
import com.esp.model.game.Game;
import com.esp.model.game.GameAttributeSummary;
import com.esp.model.game.PlayerAttribute;
import com.esp.model.image.Image;
import com.esp.model.type.FILETYPE;

/**
 * @author Amit
 *
 */
public interface GameService {
	public void submitAttribute(String gameId, String userId, String attribute, String imageId);
	
	public List<String> compareAttribute(String gameId, String imageId);	
	
	Game saveNewNonAssistedGame(Integer score,String userId,List<GameAttributeSummary>gameAttributeSummaryList,FILETYPE fileType);
	
	public boolean isCompare(String gameId, String imageId);
	
	PlayerAttribute savePlayerAttribute(PlayerAttribute playeAttribute);
	
	public Game choosePlayer(String playerId, ServletContext sc);
	
	public void addPlayer(String playerId, ServletContext sc);
	
	void setImage(String gameId, ServletContext sc);
	Image getImage(String gameId, ServletContext sc, int counter);
	List<ResultBean> evaluateGame(String gameId);
	
	boolean isEvaluate(String gameId);
	public List<Image> getImageList(String gameId, ServletContext sc);
	
	Game saveNewSinglePlayerGame(Integer score,String userId,List<GameAttributeSummary>gameAttributeSummaryList,FILETYPE fileType);

	List<Image> searchImage(String query,FILETYPE fileType);
	List<GameAttributeSummary> getGameAttributeSummaryList(String gameId, String imageId);
	List<Game> findAllGames();

}
