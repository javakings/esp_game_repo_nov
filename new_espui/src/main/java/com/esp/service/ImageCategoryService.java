/**
 * 
 */
package com.esp.service;

import java.util.List;

import com.esp.model.image.ImageCategory;

/**
 * @author Bikash
 *
 */
public interface ImageCategoryService {
	  public List<ImageCategory>findAllCategory();
	  public ImageCategory findCategoryById(String id);
	  public void addCategory(String name);
	  public void updateCategory(String id,String name);
	  public void deleteCategory(String id,String name);
}
