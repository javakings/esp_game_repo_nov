/**
 * 
 */
package com.esp.utility;

import java.util.Random;

/**
 * @author Amit
 *
 */
public class CommonUtility {
	
	public static int getRandomNo(int limit){
		Random randomGenerator = new Random();
		return randomGenerator.nextInt(limit);
	}
	
}
