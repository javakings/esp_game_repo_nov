/**
 * 
 */
package com.esp.utility;

import org.springframework.security.crypto.codec.Base64;


/**
 * @author Amit
 *
 */
public class AuthenticationUtility {


	public String encrypt(String password)
	{
		return new String(Base64.encode(password.getBytes()));
	}
	
	public String decrypt(String password)
	{
		return new String(Base64.decode(password.getBytes()));
	}
	
	public static void main(String[] args) {
		System.out.println(new AuthenticationUtility().decrypt("Y2hvY29sYXRlMTIz"));
	}
}
