/**
 * 
 */
package com.esp.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;

import com.esp.constant.EspConstant;

/**
 * @author Jay
 *
 */
public class ExcelUtility {
	public static String writeToExcel(Map<Integer, List<String>> data, HttpServletRequest request) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Reporting sheet");
		for (int i=0; i<7; i++){
			   sheet.setColumnWidth(i,7000);
			}
		String readFilePath = "";
		TreeMap<Integer, List<String>> dataTreeMap = new TreeMap<Integer, List<String>>(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return o1.compareTo(o2);
			}
		});
		dataTreeMap.putAll(data);
		Set<Integer> keyset = dataTreeMap.keySet();

		int rownum = 0;
		/*
		 * HSSFRow firstrow = sheet.createRow(rownum++); HSSFCellStyle style =
		 * workbook.createCellStyle(); HSSFFont font = workbook.createFont();
		 * font.setFontName(HSSFFont.FONT_ARIAL);
		 * font.setFontHeightInPoints((short)10); font.setBold(true);
		 * style.setFont(font); List<String> firstRowList = data.get("1"); int
		 * cellnumRow1 = 0; for (Object obj : firstRowList) { Cell cell =
		 * firstrow.createCell(cellnumRow1++); cell.setCellValue((String)obj); }
		 * 
		 * Row secondrow = sheet.createRow(rownum++); List<String> secondRowList
		 * = data.get("2"); int cellnumRow2 = 0; for (Object obj :
		 * secondRowList) { Cell cell = secondrow.createCell(cellnumRow2++);
		 * cell.setCellValue((String)obj); }
		 */
		for (Integer key : keyset) {

			Row row = sheet.createRow(rownum++);
			List<String> objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				cell.setCellValue((String) obj);
				/*
				 * if(obj instanceof Date) cell.setCellValue((Date)obj); else
				 * if(obj instanceof Boolean) cell.setCellValue((Boolean)obj);
				 * else if(obj instanceof String)
				 * cell.setCellValue((String)obj); else if(obj instanceof
				 * Double) cell.setCellValue((Double)obj);
				 */

			}
		}
		
         makeRowBold(workbook, sheet.getRow(0));

		try {
			String fileName = "Reporting_" + new Date().getTime() + "file.xls";
			String filePath = request.getServletContext().getRealPath(EspConstant.DEFAULT_PATH) + File.separator;
			FileOutputStream out = new FileOutputStream(new File(filePath + fileName));
			readFilePath = "/" + EspConstant.DEFAULT_PATH + fileName;
			workbook.write(out);
			/*
			 * response.setContentType("application/ms-excel");
			 * response.setContentLength(outArray.length);
			 * response.setHeader("Expires:", "0"); // eliminates browser
			 * caching response.setHeader("Content-Disposition",
			 * "attachment; filename=Reporting.xls"); OutputStream outStream =
			 * response.getOutputStream(); out.close();
			 */
			System.out.println("Excel written successfully..");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return readFilePath;
	}

	public static String writeToExcelAttributeReporting(Map<Integer, List<String>> data, HttpServletRequest request) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Reporting sheet");
		String readFilePath = "";
		TreeMap<Integer, List<String>> dataTreeMap = new TreeMap<Integer, List<String>>(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return o1.compareTo(o2);
			}
		});
		dataTreeMap.putAll(data);
		Set<Integer> keyset = dataTreeMap.keySet();
		for (int i=0; i<12; i++){
			   sheet.setColumnWidth(i,7000);
			}
		int rownum = 0;

		for (Integer key : keyset) {
			Row row = sheet.createRow(rownum++);
			List<String> objArr = data.get(key);
			int cellnum = 0;
			if (rownum > 1) {

				int count = 0;
				for (String obj : objArr) {
					if (count == 0) {
						Cell cell = row.createCell(cellnum++);
						cell.setCellValue(obj);
					} else {
						String[] dataArr = obj.split("@");
						int subCellnum = 0;
						Row subrow = sheet.createRow(rownum++);
						Cell cell1 = subrow.createCell(subCellnum++);
						cell1.setCellValue("   ");
						for (String rowData : dataArr) {
							Cell cell = subrow.createCell(subCellnum++);
							cell.setCellValue(rowData);

						}

					}
					count++;
					/*
					 * if(obj instanceof Date) cell.setCellValue((Date)obj);
					 * else if(obj instanceof Boolean)
					 * cell.setCellValue((Boolean)obj); else if(obj instanceof
					 * String) cell.setCellValue((String)obj); else if(obj
					 * instanceof Double) cell.setCellValue((Double)obj);
					 */

				}
			} else {
				for (String obj : objArr) {
					Cell cell = row.createCell(cellnum++);
					cell.setCellValue(obj);

				}
			}
		}
		
		 makeRowBold(workbook, sheet.getRow(0));

		try {
			String fileName = "Attribute_Reporting_" + new Date().getTime() + "file.xls";
			String filePath = request.getServletContext().getRealPath(EspConstant.DEFAULT_PATH) + File.separator;
			FileOutputStream out = new FileOutputStream(new File(filePath + fileName));
			readFilePath = "/" + EspConstant.DEFAULT_PATH + fileName;
			workbook.write(out);
			/*
			 * response.setContentType("application/ms-excel");
			 * response.setContentLength(outArray.length);
			 * response.setHeader("Expires:", "0"); // eliminates browser
			 * caching response.setHeader("Content-Disposition",
			 * "attachment; filename=Reporting.xls"); OutputStream outStream =
			 * response.getOutputStream(); out.close();
			 */
			System.out.println("Excel written successfully..");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return readFilePath;
	}
	
	private static  void makeRowBold(Workbook wb, Row row){
	    CellStyle style = wb.createCellStyle();//Create style
	    Font font = wb.createFont();//Create font
	    font.setBoldweight(Font.BOLDWEIGHT_BOLD);//Make font bold
	    style.setFont(font);//set it to bold
        style.setWrapText(true);
	    for(int i = 0; i < row.getLastCellNum(); i++){//For each cell in the row 
	        row.getCell(i).setCellStyle(style);//Set the sty;e
	    }
	}
}
