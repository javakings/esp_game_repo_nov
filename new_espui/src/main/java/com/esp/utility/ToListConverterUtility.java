/**
 * 
 */
package com.esp.utility;

import java.util.ArrayList;
import java.util.List;

import com.esp.model.image.Attribute;

/**
 * @author gyan
 *
 */
public class ToListConverterUtility {

	public List<Attribute>convertToList(String data){
		String [] arr=data.split(",");
		List<Attribute>attrList=new ArrayList<Attribute>();
		for(String attr:arr){
			if(attr!=null && attr.trim().length()>0){
			attr=attr.trim();
			attr=attr.toLowerCase();
			Attribute attribute=new Attribute();
			attribute.setAttrName(attr);
			attribute.setAttrValue(attr);
			attrList.add(attribute);
			}
		}

		return attrList;

	}

	public static List<String> convertToList(String data, String delimeter){
		List<String> list = null;
		if(data != null){
		    list = new ArrayList<String>();
			String[] arr = data.split(delimeter);
			for(String str : arr)
			{
				list.add(str);
			}
		}
		return list;
	}
}
