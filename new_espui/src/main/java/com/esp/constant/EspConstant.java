package com.esp.constant;

public class EspConstant {
	public static final String DEFAULT_PATH = "resources/htmlresource/images/temp/";
	
	
	public static final String USER = "user";
	public static final String SINGLE_PLAYER_IMAGES="SINGLE_PLAYER_IMAGES";
	public static final String MULTI_PLAYER_IMAGES="MULTI_PLAYER_IMAGES";

	public static final Integer NO_OF_ANSWERS=5;
	public static final String IMAGE_ANSWERS="IMAGE_ANSWERS";
	public static final String IMAGE_ANSWERS_OPTIONS="IMAGE_ANSWERS_OPTIONS";
	public static final String NON_ASSISTED_IMAGES_ANSWERS="NON_ASSISTED_IMAGES_ANSWERS";
	public static final String IMAGE_TIME_NON_ASSISTED="IMAGE_TIME_NON_ASSISTED";
	public static final String IMAGE_TIME_ASSISTED="IMAGE_TIME_ASSISTED";
	public static final String TUNE="TUNE";

}
